#include <stdint.h>

uint16_t tmtc_ccsds_sequence_count_cb(uint16_t apid)
{
	// TODO: Make this counter unique for certain APID's.
	static uint16_t counter = 0;

	(void) apid; // TODO: check the value of this argument

	return counter++;
}

uint8_t tmtc_ax25_master_frame_count_cb(void)
{
	static uint8_t counter = 0;

	return counter++;
}

uint8_t tmtc_ax25_virtual_frame_count_cb(uint8_t virtual_channel)
{
	static uint8_t counters[8];

	if (virtual_channel > 7)
		return 0;

	return counters[virtual_channel]++;
}