/**
 * @file   fram.c
 * @author John Wikman
 *
 * Program for accessing the FRAM.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hal/Storage/FRAM.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>

#define FRAM_MODE_NONE        (-1)
#define FRAM_MODE_READ_CHAR   (0x10)
#define FRAM_MODE_READ_SHORT  (0x11)
#define FRAM_MODE_READ_INT    (0x12)
#define FRAM_MODE_READ_BYTES  (0x13)
#define FRAM_MODE_WRITE_CHAR  (0x20)
#define FRAM_MODE_WRITE_SHORT (0x21)
#define FRAM_MODE_WRITE_INT   (0x22)
#define FRAM_MODE_WRITE_BYTES (0x23)
#define FRAM_MODE_INFO        (0x30)
#define FRAM_MODE_SET_PROT    (0x40)

#define FRAM_FMT_DECIMAL     (1)
#define FRAM_FMT_HEXADECIMAL (2)
#define FRAM_FMT_HEXDUMP     (3)

#define FRAM_WRITE_BYTES_MAXLEN (256)

struct hexdump_info {
	unsigned int bytes_per_row;
	unsigned int startaddr;
	unsigned int addr;
};

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int fram_read_char(const char *fname, int fmt, const char *addrarg);
static int fram_read_short(const char *fname, int fmt, const char *addrarg);
static int fram_read_int(const char *fname, int fmt, const char *addrarg);
static int fram_read_bytes(const char *fname, int fmt, const char *addrarg, const char *lenarg);
static void output_hexdump_byte(unsigned char val, struct hexdump_info *info);
static int fram_write_char(const char *fname, const char *addrarg, const char *valarg);
static int fram_write_short(const char *fname, const char *addrarg, const char *valarg);
static int fram_write_int(const char *fname, const char *addrarg, const char *valarg);
static int fram_write_bytes(const char *fname, const char *addrarg, unsigned char *bytes, int len);
static int fram_info(const char *fname);
static int fram_setprot(const char *fname, const char *valarg);


int fram_main(int argc, char **argv)
{
	int ret;
	int val;
	int choice;
	int mode = FRAM_MODE_NONE;
	int fmt = FRAM_FMT_DECIMAL;
	const char *addrarg = NULL;
	const char *lenarg = NULL;
	const char *valarg = NULL;
	int writebyteslen = 0;
	unsigned char writebytesarg[FRAM_WRITE_BYTES_MAXLEN];

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan and set the mode variable
	if (strcmp(argv[1], "read") == 0) { /* READ MODE */
		if (argc < 3) {
			fprintf(stderr, "%s read: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "char") == 0) {
			mode = FRAM_MODE_READ_CHAR;
			optind = 3;
		} else if (strcmp(argv[2], "short") == 0) {
			mode = FRAM_MODE_READ_SHORT;
			optind = 3;
		} else if (strcmp(argv[2], "int") == 0) {
			mode = FRAM_MODE_READ_INT;
			optind = 3;
		} else if (strcmp(argv[2], "bytes") == 0) {
			mode = FRAM_MODE_READ_BYTES;
			optind = 3;
		} else {
			fprintf(stderr, "%s read: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "rc") == 0) { /* READ ALIASES */
		mode = FRAM_MODE_READ_CHAR;
		optind = 2;
	} else if (strcmp(argv[1], "rs") == 0) {
		mode = FRAM_MODE_READ_SHORT;
		optind = 2;
	} else if (strcmp(argv[1], "ri") == 0) {
		mode = FRAM_MODE_READ_INT;
		optind = 2;
	} else if (strcmp(argv[1], "rb") == 0) {
		mode = FRAM_MODE_READ_BYTES;
		optind = 2;
	} else if (strcmp(argv[1], "write") == 0) { /* WRITE MODE */
		if (argc < 3) {
			fprintf(stderr, "%s write: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "char") == 0) {
			mode = FRAM_MODE_WRITE_CHAR;
			optind = 3;
		} else if (strcmp(argv[2], "short") == 0) {
			mode = FRAM_MODE_WRITE_SHORT;
			optind = 3;
		} else if (strcmp(argv[2], "int") == 0) {
			mode = FRAM_MODE_WRITE_INT;
			optind = 3;
		} else if (strcmp(argv[2], "bytes") == 0) {
			mode = FRAM_MODE_WRITE_BYTES;
			optind = 3;
		} else {
			fprintf(stderr, "%s write: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "wc") == 0) { /* WRITE ALIASES */
		mode = FRAM_MODE_WRITE_CHAR;
		optind = 2;
	} else if (strcmp(argv[1], "ws") == 0) {
		mode = FRAM_MODE_WRITE_SHORT;
		optind = 2;
	} else if (strcmp(argv[1], "wi") == 0) {
		mode = FRAM_MODE_WRITE_INT;
		optind = 2;
	} else if (strcmp(argv[1], "wb") == 0) {
		mode = FRAM_MODE_WRITE_BYTES;
		optind = 2;
	} else if (strcmp(argv[1], "info") == 0) { /* STATUS/INFO MODE */
		mode = FRAM_MODE_INFO;
		optind = 2;
	} else if (strcmp(argv[1], "set") == 0) { /* SET MODE */
		if (argc < 3) {
			fprintf(stderr, "%s set: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "prot") == 0) {
			mode = FRAM_MODE_SET_PROT;
			optind = 3;
		} else {
			fprintf(stderr, "%s set: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "sp") == 0) { /* SET ALIASES */
		mode = FRAM_MODE_SET_PROT;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan choices
	while ((choice = getopt(argc, argv, "-hxdX")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			switch (mode) {
			case FRAM_MODE_READ_CHAR:
			case FRAM_MODE_READ_SHORT:
			case FRAM_MODE_READ_INT:
				if (addrarg == NULL) {
					addrarg = optarg;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case FRAM_MODE_READ_BYTES:
				if (addrarg == NULL) {
					addrarg = optarg;
				} else if (lenarg == NULL) {
					lenarg = optarg;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case FRAM_MODE_WRITE_CHAR:
			case FRAM_MODE_WRITE_SHORT:
			case FRAM_MODE_WRITE_INT:
				if (addrarg == NULL) {
					addrarg = optarg;
				} else if (valarg == NULL) {
					valarg = optarg;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case FRAM_MODE_WRITE_BYTES:
				if (addrarg == NULL) {
					addrarg = optarg;
				} else if (writebyteslen < FRAM_WRITE_BYTES_MAXLEN) {
					// scan the bytes to write to FRAM
					ret = arghandler_to_int(optarg, &val);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					}
					if ((val < 0) || (val > 255)) {
						fprintf(stderr, "%s: the value \'%s\' can not be contained within a byte\n\r", argv[0], optarg);
						return 1;
					}
					writebytesarg[writebyteslen++] = (unsigned char) val;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case FRAM_MODE_SET_PROT:
				if (valarg == NULL) {
					valarg = optarg;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			default:
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'x':
			fmt = FRAM_FMT_HEXADECIMAL;
			break;
		case 'd':
			fmt = FRAM_FMT_DECIMAL;
			break;
		case 'X':
			if (mode != FRAM_MODE_READ_BYTES) {
				fprintf(stderr, "%s: option \'-X\' is only valid for fram read bytes\n\r", argv[0]);
				return 1;
			}
			fmt = FRAM_FMT_HEXDUMP;
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check that required positionals are set
	switch (mode) {
	case FRAM_MODE_READ_CHAR:
	case FRAM_MODE_READ_SHORT:
	case FRAM_MODE_READ_INT:
		if (addrarg == NULL) {
			fprintf(stderr, "%s: must specify address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case FRAM_MODE_READ_BYTES:
		if (addrarg == NULL) {
			fprintf(stderr, "%s: must specify address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (lenarg == NULL) {
			fprintf(stderr, "%s: must specify length\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case FRAM_MODE_WRITE_CHAR:
	case FRAM_MODE_WRITE_SHORT:
	case FRAM_MODE_WRITE_INT:
		if (addrarg == NULL) {
			fprintf(stderr, "%s: must specify address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (valarg == NULL) {
			fprintf(stderr, "%s: must specify the value to write\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case FRAM_MODE_WRITE_BYTES:
		if (addrarg == NULL) {
			fprintf(stderr, "%s: must specify address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (writebyteslen < 1) {
			fprintf(stderr, "%s: must specify the values to write\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case FRAM_MODE_SET_PROT:
		if (valarg == NULL) {
			fprintf(stderr, "%s: must specify protection mode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
	default:
		break;
	}

	// Invoke corresponding functions
	ret = 1;
	switch (mode) {
	case FRAM_MODE_READ_CHAR:
		ret = fram_read_char(argv[0], fmt, addrarg);
		break;
	case FRAM_MODE_READ_SHORT:
		ret = fram_read_short(argv[0], fmt, addrarg);
		break;
	case FRAM_MODE_READ_INT:
		ret = fram_read_int(argv[0], fmt, addrarg);
		break;
	case FRAM_MODE_READ_BYTES:
		ret = fram_read_bytes(argv[0], fmt, addrarg, lenarg);
		break;
	case FRAM_MODE_WRITE_CHAR:
		ret = fram_write_char(argv[0], addrarg, valarg);
		break;
	case FRAM_MODE_WRITE_SHORT:
		ret = fram_write_short(argv[0], addrarg, valarg);
		break;
	case FRAM_MODE_WRITE_INT:
		ret = fram_write_int(argv[0], addrarg, valarg);
		break;
	case FRAM_MODE_WRITE_BYTES:
		ret = fram_write_bytes(argv[0], addrarg, writebytesarg, writebyteslen);
		break;
	case FRAM_MODE_INFO:
		ret = fram_info(argv[0]);
		break;
	case FRAM_MODE_SET_PROT:
		ret = fram_setprot(argv[0], valarg);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Access functionality for FRAM.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s read (char|short|int) [-hxd] <addr>\n\r", fname);
	fprintf(stderr, "       %s read bytes [-hxdX] <addr> <len>\n\r", fname);
	fprintf(stderr, "       %s write (char|short|int) [-h] <addr> <value>\n\r", fname);
	fprintf(stderr, "       %s write bytes [-h] <addr> <values...>\n\r", fname);
	fprintf(stderr, "       %s info [-h]\n\r", fname);
	fprintf(stderr, "       %s set prot [-h]\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -x: Output numbers in hexadecimal.\n\r");
	fprintf(stderr, "    -d: Output numbers in decimal. (default)\n\r");
	fprintf(stderr, "    -X: Output bytes in the style of a hexdump.\n\r");
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Mode aliases:\n\r");
	fprintf(stderr, "    r(c|s|i|b): read (char|short|int|bytes)\n\r");
	fprintf(stderr, "    w(c|s|i|b): write (char|short|int|bytes)\n\r");
	fprintf(stderr, "    sp:         set prot\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Reads the character at the specified address.
 */
static int fram_read_char(const char *fname, int fmt, const char *addrarg)
{
	int ret;
	int addrconv;
	unsigned int addr;
	unsigned char buf[16];

	ret = arghandler_to_int(addrarg, &addrconv);
	if ((ret != 0) || (addrconv < 0)) {
		fprintf(stderr, "%s read char: \'%s\' is not a valid FRAM address\n\r", fname, addrarg);
		return 1;
	}

	addr = (unsigned int) addrconv;

	ret = FRAM_read(buf, addr, sizeof(char));
	if (ret == -2) {
		fprintf(stderr, "%s read char: address \'%s\' is out-of-bounds\n\r", fname, addrarg);
		return 1;
	} else if (ret != 0) {
		fprintf(stderr, "%s read char: FRAM_read returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "char@0x%X: ", addr);

	if (fmt == FRAM_FMT_HEXADECIMAL)
		fprintf(stderr, "0x%02X", buf[0]);
	else
		fprintf(stderr, "%u", buf[0]);

	fprintf(stderr, "\n\r");
	return 0;
}

/**
 * Reads a short from the specified address.
 */
static int fram_read_short(const char *fname, int fmt, const char *addrarg)
{
	int ret;
	int addrconv;
	unsigned int addr;
	unsigned char buf[16];
	unsigned short val;

	ret = arghandler_to_int(addrarg, &addrconv);
	if ((ret != 0) || (addrconv < 0)) {
		fprintf(stderr, "%s read short: \'%s\' is not a valid FRAM address\n\r", fname, addrarg);
		return 1;
	}

	addr = (unsigned int) addrconv;

	ret = FRAM_read(buf, addr, sizeof(unsigned short));
	if (ret == -2) {
		fprintf(stderr, "%s read short: address \'%s\' is out-of-bounds\n\r", fname, addrarg);
		return 1;
	} else if (ret != 0) {
		fprintf(stderr, "%s read short: FRAM_read returned %d\n\r", fname, ret);
		return 1;
	}

	memcpy(&val, buf, sizeof(val));

	fprintf(stderr, "short@0x%X: ", addr);

	if (fmt == FRAM_FMT_HEXADECIMAL)
		fprintf(stderr, "0x%04X", val);
	else
		fprintf(stderr, "%hu", val);

	fprintf(stderr, "\n\r");
	return 0;
}

/**
 * Reads an int from the specified address.
 */
static int fram_read_int(const char *fname, int fmt, const char *addrarg)
{
	int ret;
	int addrconv;
	unsigned int addr;
	unsigned char buf[16];
	unsigned int val;

	ret = arghandler_to_int(addrarg, &addrconv);
	if ((ret != 0) || (addrconv < 0)) {
		fprintf(stderr, "%s read int: \'%s\' is not a valid FRAM address\n\r", fname, addrarg);
		return 1;
	}

	addr = (unsigned int) addrconv;

	ret = FRAM_read(buf, addr, sizeof(unsigned int));
	if (ret == -2) {
		fprintf(stderr, "%s read int: address \'%s\' is out-of-bounds\n\r", fname, addrarg);
		return 1;
	} else if (ret != 0) {
		fprintf(stderr, "%s read int: FRAM_read returned %d\n\r", fname, ret);
		return 1;
	}

	memcpy(&val, buf, sizeof(val));

	fprintf(stderr, "int@0x%X: ", addr);

	if (fmt == FRAM_FMT_HEXADECIMAL)
		fprintf(stderr, "0x%08X", val);
	else
		fprintf(stderr, "%u", val);

	fprintf(stderr, "\n\r");
	return 0;
}

/**
 * Reads bytes from the specified address.
 */
static int fram_read_bytes(const char *fname, int fmt, const char *addrarg, const char *lenarg)
{
	int ret;
	int conv;
	unsigned int addr;
	unsigned char buf[128];
	unsigned int readlen;
	unsigned int len;
	struct hexdump_info fmtinfo;

	ret = arghandler_to_int(addrarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s read bytes: \'%s\' is not a valid FRAM address\n\r", fname, addrarg);
		return 1;
	}

	addr = (unsigned int) conv;

	ret = arghandler_to_int(lenarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s read bytes: \'%s\' is not a valid length\n\r", fname, lenarg);
		return 1;
	}

	len = (unsigned int) conv;
	if ((addr + len) > FRAM_getMaxAddress()) {
		fprintf(stderr, "%s read bytes: address and length is out-of-bounds\n\r", fname);
		return 1;
	}

	fmtinfo.bytes_per_row = 16;
	fmtinfo.startaddr = addr;
	fmtinfo.addr = addr;

	if (fmt != FRAM_FMT_HEXDUMP) {
		fprintf(stderr, "bytes@0x%X:", addr);
	}

	readlen = 0;
	while (readlen < len) {
		unsigned int i;
		unsigned int bytes_to_read = len - readlen;
		if (bytes_to_read > sizeof(buf))
			bytes_to_read = sizeof(buf);

		ret = FRAM_read(buf, addr + readlen, bytes_to_read);
		if (ret != 0) {
			fprintf(stderr, "%s read bytes: FRAM_read returned %d\n\r", fname, ret);
			return 1;
		}

		for (i = 0; i < bytes_to_read; i++) {
			if (fmt == FRAM_FMT_HEXDUMP)
				output_hexdump_byte(buf[i], &fmtinfo);
			else if (fmt == FRAM_FMT_HEXADECIMAL)
				fprintf(stderr, " 0x%02X", buf[i]);
			else
				fprintf(stderr, " %u", buf[i]);
		}

		readlen += bytes_to_read;
	}

	fprintf(stderr, "\n\r");
	return 0;
}

/**
 * Outputs a byte according to hexadecimal format.
 */
static void output_hexdump_byte(unsigned char val, struct hexdump_info *info)
{
	// Make sure that bytes per row is not 0
	if (info->bytes_per_row == 0)
		info->bytes_per_row = 16;

	int rowpos = (info->addr - info->startaddr) % info->bytes_per_row;


	if (rowpos == 0) {
		if (info->addr != info->startaddr)
			fprintf(stderr, "\n\r");
		fprintf(stderr, "%07x", info->addr);
	}

	if ((rowpos & 1) == 0)
		fprintf(stderr, " ");

	fprintf(stderr, "%02x", val);

	(info->addr)++;
}

/**
 * Writes a char to the specified address in FRAM.
 */
static int fram_write_char(const char *fname, const char *addrarg, const char *valarg)
{
	int ret;
	int conv;
	unsigned int addr;
	unsigned char val;

	ret = arghandler_to_int(addrarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s write char: \'%s\' is not a valid FRAM address\n\r", fname, addrarg);
		return 1;
	}

	addr = (unsigned int) conv;

	ret = arghandler_to_int(valarg, &conv);
	if ((ret != 0)) {
		fprintf(stderr, "%s write char: \'%s\' is not a recognized numerical value\n\r", fname, valarg);
		return 1;
	}
	if ((conv < 0) || (conv > 0xFF)) {
		fprintf(stderr, "%s write char: \'%s\' is too large to fit in a char\n\r", fname, valarg);
		return 1;
	}

	val = (unsigned char) conv;

	ret = FRAM_write(&val, addr, sizeof(val));
	if (ret == -2) {
		fprintf(stderr, "%s write char: address \'%s\' is out-of-bounds\n\r", fname, addrarg);
		return 1;
	} else if (ret != 0) {
		fprintf(stderr, "%s write char: FRAM_write returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Writes a short to the specified address in FRAM.
 */
static int fram_write_short(const char *fname, const char *addrarg, const char *valarg)
{
	int ret;
	int conv;
	unsigned int addr;
	unsigned short val;

	ret = arghandler_to_int(addrarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s write short: \'%s\' is not a valid FRAM address\n\r", fname, addrarg);
		return 1;
	}

	addr = (unsigned int) conv;

	ret = arghandler_to_int(valarg, &conv);
	if ((ret != 0)) {
		fprintf(stderr, "%s write short: \'%s\' is not a recognized numerical value\n\r", fname, valarg);
		return 1;
	}
	if ((conv < 0) || (conv > 0xFFFF)) {
		fprintf(stderr, "%s write short: \'%s\' is too large to fit in a short\n\r", fname, valarg);
		return 1;
	}

	val = (unsigned short) conv;

	ret = FRAM_write((unsigned char *) &val, addr, sizeof(val));
	if (ret == -2) {
		fprintf(stderr, "%s write short: address \'%s\' is out-of-bounds\n\r", fname, addrarg);
		return 1;
	} else if (ret != 0) {
		fprintf(stderr, "%s write short: FRAM_write returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Writes an int to the specified address in FRAM.
 */
static int fram_write_int(const char *fname, const char *addrarg, const char *valarg)
{
	int ret;
	int conv;
	unsigned int addr;
	unsigned int val;

	ret = arghandler_to_int(addrarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s write int: \'%s\' is not a valid FRAM address\n\r", fname, addrarg);
		return 1;
	}

	addr = (unsigned int) conv;

	ret = arghandler_to_int(valarg, &conv);
	if ((ret != 0)) {
		fprintf(stderr, "%s write int: \'%s\' is not a recognized numerical value\n\r", fname, valarg);
		return 1;
	}

	val = (unsigned int) conv;

	ret = FRAM_write((unsigned char *) &val, addr, sizeof(val));
	if (ret == -2) {
		fprintf(stderr, "%s write int: address \'%s\' is out-of-bounds\n\r", fname, addrarg);
		return 1;
	} else if (ret != 0) {
		fprintf(stderr, "%s write int: FRAM_write returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Writes bytes to the specified address in FRAM.
 */
static int fram_write_bytes(const char *fname, const char *addrarg, unsigned char *bytes, int len)
{
	int ret;
	int conv;
	unsigned int addr;

	ret = arghandler_to_int(addrarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s write bytes: \'%s\' is not a valid FRAM address\n\r", fname, addrarg);
		return 1;
	}

	addr = (unsigned int) conv;

	ret = FRAM_write(bytes, addr, (unsigned int) len);
	if (ret == -2) {
		fprintf(stderr, "%s write bytes: address \'%s\' and length %d is out-of-bounds\n\r", fname, addrarg, len);
		return 1;
	} else if (ret != 0) {
		fprintf(stderr, "%s write bytes: FRAM_write returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Display information about FRAM.
 */
static int fram_info(const char *fname)
{
	int ret;
	int i;
	FRAMblockProtect blocks;
	unsigned char devid[9];
	unsigned int maxaddr;

	ret = FRAM_getProtectedBlocks(&blocks);
	if (ret != 0) {
		fprintf(stderr, "%s info: FRAM_getProtectedBlocks returned %d\n\r", fname, ret);
		return 1;
	}

	ret = FRAM_getDeviceID(devid);
	if (ret != 0) {
		fprintf(stderr, "%s info: FRAM_getDeviceID returned %d\n\r", fname, ret);
		return 1;
	}

	maxaddr = FRAM_getMaxAddress();

	fprintf(stderr, "FRAM Information\n\r");
	fprintf(stderr, "  - write-protected blocks: ");
	switch (blocks.fields.blockProtect) {
	case 0:
		fprintf(stderr, "none");
		break;
	case 1:
		fprintf(stderr, "upper 1/4");
		break;
	case 2:
		fprintf(stderr, "upper 1/2");
		break;
	case 3:
		fprintf(stderr, "all");
		break;
	default:
		break;
	}
	fprintf(stderr, "\n\r");
	fprintf(stderr, "  - device ID: %02X", devid[0]);
	for (i = 1; i < 9; i++)
		fprintf(stderr, "-%02X", devid[i]);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "  - max address: 0x%X\n\r", maxaddr);

	return 0;
}

/**
 * Sets the block protection mode
 */
static int fram_setprot(const char *fname, const char *valarg)
{
	int ret;
	int mode;
	FRAMblockProtect blocks;

	ret = arghandler_to_int(valarg, &mode);
	if (ret != 0) {
		fprintf(stderr, "%s set prot: \'%s\' is not a recognized numerical value\n\r", fname, valarg);
		return 1;
	}

	if ((mode < 0) || (mode > 3)) {
		fprintf(stderr, "%s set prot: block protection mode must be a value between 0 and 3.\n\r", fname);
		return 1;
	}

	blocks.fields.blockProtect = mode;

	ret = FRAM_protectBlocks(blocks);
	if (ret != 0) {
		fprintf(stderr, "%s set prot: FRAM_protectBlocks returned %d\n", fname, ret);
		return 1;
	}

	return 0;
}
