/**
 * @file   mkdir.c
 * @author John Wikman
 *
 * Create directory.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>

#include <mist-tools/arghandler.h>

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

int mkdir_main(int argc, char **argv)
{
	int ret;
	int choice;
	const char *dirname = NULL;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	while ((choice = getopt(argc, argv, "-h")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			if (dirname == NULL) {
				dirname = optarg;
			} else {
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	if (dirname == NULL) {
		fprintf(stderr, "%s: must specify a directory name\n\r", argv[0]);
		return 1;
	}

	ret = f_mkdir(dirname);
	if (ret != F_NO_ERROR) {
		fprintf(stderr, "%s: f_mkdir returned %d\n\r", argv[0], ret);
		return 1;
	}

	return 0;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Create a new directory.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s [-h] <dirname>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "\n\r");
}
