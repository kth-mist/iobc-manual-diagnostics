/**
 * @file   tmtc.c
 * @author John Wikman
 *
 * Access the functionality of the TMTC API.
 */

#include <getopt.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hal/Timing/Time.h>

#include <tmtc-api/tmtc.h>
#include <tmtc-api/config/config.h>
#include <tmtc-api/application/frgmnt.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>

#define TMTC_MODE_NONE    (-1)
#define TMTC_MODE_SETUP   (0x10)
#define TMTC_MODE_GET_TC  (0x21)
#define TMTC_MODE_SEND_TM (0x31)

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int tmtc_prog_setup(const char *fname);
static int tmtc_prog_get(const char *fname);
static int tmtc_prog_send(const char *fname, struct tmtc_tm_params *params);

int tmtc_prog_main(int argc, char **argv)
{
	int i;
	int ret;
	int value;
	int choice;
	int mode = TMTC_MODE_NONE;
	static uint8_t data[54016];
	static uint16_t datalen;
	static fragmenter_t fragmenter;
	static uint8_t buf[214];
	static uint8_t fragment_size;
	struct tmtc_tm_params sendtmparams;
	sendtmparams.virtual_channel_id = 0;
	sendtmparams.apid = 77;
	sendtmparams.service_type = 150;
	sendtmparams.service_subtype = 51;
	sendtmparams.time = 0;
	sendtmparams.payload = buf;
	sendtmparams.payloadlen = 0;
	datalen = 0;
	fragment_size = 0;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	if (strcmp(argv[1], "setup") == 0) { /* SETUP MODE */
		mode = TMTC_MODE_SETUP;
		optind = 2;
	} else if (strcmp(argv[1], "get") == 0) { /* GET MODE */
		if (argc < 3) {
			fprintf(stderr, "%s get: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "tc") == 0) {
			mode = TMTC_MODE_GET_TC;
			optind = 3;
		} else {
			fprintf(stderr, "%s get: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "send") == 0) { /* SEND MODE */
		if (argc < 3) {
			fprintf(stderr, "%s send: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "tm") == 0) {
			mode = TMTC_MODE_SEND_TM;
			optind = 3;
		} else {
			fprintf(stderr, "%s send: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	while ((choice = getopt(argc, argv, "-hV:A:T:S:F:G:")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			switch (mode) {
			case TMTC_MODE_SEND_TM:
				if (datalen < sizeof(data)) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
						return 1;
					}
					if ((value < 0) || (value > 0xff)) {
						fprintf(stderr, "%s: Specified value \'%s\' does not fit into a single byte.\n\r", argv[0], optarg);
						return 1;
					}
					data[datalen++] = (uint8_t) value;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			default:
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'V':
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
				return 1;
			}
			if ((value < 0) || (value > 7)) {
				fprintf(stderr, "%s: Virtual channel ID must be a number between 0 and 7.\n\r", argv[0]);
				return 1;
			}
			sendtmparams.virtual_channel_id = (uint8_t) value;
			break;
		case 'A':
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
				return 1;
			}
			if ((value < 0) || (value > 0x07ff)) {
				fprintf(stderr, "%s: APID must be a value between 0 and 0x7FFF.\n\r", argv[0]);
				return 1;
			}
			sendtmparams.apid = (uint16_t) value;
			break;
		case 'T':
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
				return 1;
			}
			if ((value < 0) || (value > 0xff)) {
				fprintf(stderr, "%s: Service type must be a value between 0 and 255.\n\r", argv[0]);
				return 1;
			}
			sendtmparams.service_type = (uint8_t) value;
			break;
		case 'S':
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
				return 1;
			}
			if ((value < 0) || (value > 0xff)) {
				fprintf(stderr, "%s: Service subtype must be a value between 0 and 255.\n\r", argv[0]);
				return 1;
			}
			sendtmparams.service_subtype = (uint8_t) value;
			break;
		case 'F':
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
				return 1;
			}
			if ((value != 0) && ((value < 3) || (value > 214))) {
				fprintf(stderr, "%s: Fragment size must be a value between 3 and 214, or 0 (no fragmentation).\n\r", argv[0]);
				return 1;
			}
			fragment_size = (uint8_t) value;
			break;
		case 'G':
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
				return 1;
			}
			if (value > (int)(sizeof(data) - datalen))
			{
				fprintf(stderr, "%s: Too large data to send.\n\r", argv[0]);
				return 1;
			}

			for (i = 0; i < value; i++)
			{
				data[datalen++] = (uint8_t) i % 256;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check for required positionals
	switch (mode) {
	case TMTC_MODE_SEND_TM:
		if (fragment_size == 0 && datalen > 214)
		{
			fprintf(stderr, "%s: Unfragmented data cannot be larger than 214 B.\n\r", argv[0]);
			return 1;
		} else if (fragment_size != 0 && datalen > (fragment_size - 3) * 256)
		{
			fprintf(stderr, "%s: Fragmented data cannot be larger than %d B for a fragment size of %d B.\n\r", argv[0],
			(fragment_size - 3) * 256, fragment_size);
			return 1;
		}
		break;
	default:
		break;
	}

	// Invoke corresponding function
	ret = 1;
	switch (mode) {
	case TMTC_MODE_SETUP:
		ret = tmtc_prog_setup(argv[0]);
		break;
	case TMTC_MODE_GET_TC:
		ret = tmtc_prog_get(argv[0]);
		break;
	case TMTC_MODE_SEND_TM:
		if (fragment_size == 0)
		{
			sendtmparams.payloadlen = datalen;
			sendtmparams.payload = data;
			ret = tmtc_prog_send(argv[0], &sendtmparams);
		}
		else
		{
			int fragments;
			ret = frgmnt_init_fragmentation(&fragmenter, data, datalen, fragment_size, &fragments);
			if (ret != 0)
			{
				fprintf(stderr, "%s: frgmnt_init_fragmentation returned %d.\n\r", argv[0], ret);
				return 1;
			}

			for (i = 0; i < fragments; i++)
			{
				ret = frgmnt_get_next_fragment(&fragmenter, buf, (uint8_t *) &sendtmparams.payloadlen);
				if (ret != 0)
				{
					fprintf(stderr, "%s: frgmnt_get_next_fragment returned %d.\n\r", argv[0], ret);
					return 1;
				}

				sendtmparams.payload = buf;
				ret = tmtc_prog_send(argv[0], &sendtmparams);
			}
		}
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Provides access to the TMTC API.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	size_t align = strlen(fname) + 1; // +1 to replace the fmt character

	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s setup [-h]\n\r", fname);
	fprintf(stderr, "       %s get tc [-h]\n\r", fname);
	fprintf(stderr, "       %s send tm [-h] [-V virtual_channel_id] [-A apid]\n\r", fname);
	fprintf(stderr, "       %*s        [-T service_type] [-S service_subtype]\n\r", align, "");
	fprintf(stderr, "       %*s        <bytes ...>\n\r", align, "");
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -V: Specify AX.25 virtual channel ID (default: 0).\n\r");
	fprintf(stderr, "    -A: Specify CCSDS PUS APID (default: 77).\n\r");
	fprintf(stderr, "    -T: Specify CCSDS PUS service type (default: 150).\n\r");
	fprintf(stderr, "    -S: Specify CCSDS PUS service subtype (default: 51).\n\r");
	fprintf(stderr, "    -F: Specify fragment size (default: 0 (no fragmentation)).\n\r");
	fprintf(stderr, "    -G: Generate given number of bytes of data (0x00, 0x01, 0x02...).\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Sets up the TMTC API.
 */
static int tmtc_prog_setup(const char *fname)
{
	int ret;

	// TODO Allow specifying the I2C address to the simulator
	ret = tmtc_setup(NULL);
	if (ret != 0) {
		fprintf(stderr, "%s setup: tmtc_setup returned 0x%03x\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Retrieves a telecommand from the TMTC API.
 */
static int tmtc_prog_get(const char *fname)
{
	static struct tmtc_telecommand tc;

	int ret;
	size_t datalen;
	size_t i;

	ret = tmtc_get_tc(&tc);
	if (ret != 0) {
		fprintf(stderr, "%s get: tmtc_get_tc returned 0x%03x\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "AX.25 sequence flag:   %u\n\r", tc.ax25.sequence_flag);
	fprintf(stderr, "CCSDS APID:            %hu\n\r", tc.ccsds.apid);
	fprintf(stderr, "CCSDS sequence count:  %hu\n\r", tc.ccsds.sequence_count);
	fprintf(stderr, "CCSDS packet length:   %hu\n\r", tc.ccsds.packet_length);
	fprintf(stderr, "CCSDS ack flag:        0x%02x\n\r", tc.ccsds.ack);
	fprintf(stderr, "CCSDS service type:    %u\n\r", tc.ccsds.service_type);
	fprintf(stderr, "CCSDS service subtype: %u\n\r", tc.ccsds.service_subtype);

	datalen = CCSDS_TC_APPLICATION_DATA_LENGTH(tc.ccsds.packet_length);
	fprintf(stderr, "Contained data: (%u bytes)", datalen);
	for (i = 0; i < datalen; i++) {
		if ((i % 10) == 0)
				fprintf(stderr, "\n\r");
		else
			fprintf(stderr, " ");
		fprintf(stderr, "0x%02x", TC_CCSDS_APPLICATION_DATA(tc.rawdata)[i]);
	}
	fprintf(stderr, "\n\r");

	return 0;
}

/**
 * Sends a telemetry packet from the TMTC API.
 */
static int tmtc_prog_send(const char *fname, struct tmtc_tm_params *params)
{
	static struct tmtc_telemetry tm;

	int ret;
	unsigned int epoch;

	if (params->time == 0)
	{
		ret = Time_getUnixEpoch(&epoch);
		if (ret != 0) {
			fprintf(stderr, "%s send: Time_getUnixEpoch returned %d\n\r", fname, ret);
			return 1;
		}

		params->time = (uint32_t) epoch;
	}

	ret = tmtc_generate_tm(&tm, params);
	if (ret != 0) {
		fprintf(stderr, "%s send: tmtc_generate_tm returned 0x%03x\n\r", fname, ret);
		return 1;
	}

	ret = tmtc_send_tm(&tm);
	if (ret != 0) {
		fprintf(stderr, "%s send: tmtc_send_tm returned 0x%03x\n\r", fname, ret);
		return 1;
	}

	return 0;
}
