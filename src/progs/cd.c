/**
 * @file   cd.c
 * @author John Wikman
 *
 * Change directory.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>
#include <hcc/api_hcc_mem.h>
#include <hcc/api_mdriver_atmel_mcipdc.h>

#include <mist-tools/arghandler.h>

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static const char *default_dir = "/";

int cd_main(int argc, char **argv)
{
	int ret;
	int choice;
	const char *cdarg = NULL;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	while ((choice = getopt(argc, argv, "-h")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			if (cdarg == NULL) {
				cdarg = optarg;
			} else {
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	if (cdarg == NULL) {
		cdarg = default_dir;
	}

	ret = f_chdir(cdarg);
	if (ret == F_ERR_NOTFOUND) {
		fprintf(stderr, "%s: directory \'%s\' not found\n\r", argv[0], cdarg);
		return 1;
	} else if (ret != F_NO_ERROR) {
		fprintf(stderr, "error: f_chdir returned %d\n\r", ret);
		return 1;
	}

	return 0;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Change current working directory.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s [-h] <dir>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "\n\r");
}
