/**
 * @file   eps.c
 * @author John Wikman
 *
 * Access all the functionality of the GOMspace EPS subsystem.
 */

#include <ctype.h>
#include <getopt.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hal/errors.h>
#include <satellite-subsystems/GomEPS.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>
#include <mist-tools/serial_in.h>

// This is the default address on main campus at the time of testing
#define EPS_ADDR_DEFAULT (0x02)

#define EPS_DELAY_DEFAULT (0)

#define EPS_MODE_NONE                 (-1)
#define EPS_MODE_INITIALIZE           (0x10)
#define EPS_MODE_PING                 (0x20)
#define EPS_MODE_RESET_SOFT           (0x31)
#define EPS_MODE_RESET_HARD           (0x32)
#define EPS_MODE_RESET_COUNTERS       (0x33)
#define EPS_MODE_RESET_WDT            (0x34)
#define EPS_MODE_HOUSEKEEPING_PARAM   (0x41)
#define EPS_MODE_HOUSEKEEPING_GENERAL (0x42)
#define EPS_MODE_HOUSEKEEPING_VI      (0x43)
#define EPS_MODE_HOUSEKEEPING_OUT     (0x44)
#define EPS_MODE_HOUSEKEEPING_WDT     (0x45)
#define EPS_MODE_HOUSEKEEPING_BASIC   (0x46)
#define EPS_MODE_SET_OUTPUT_ALL       (0x51)
#define EPS_MODE_SET_OUTPUT_SINGLE    (0x52)
#define EPS_MODE_SET_PHOTOVOLTAIC     (0x53)
#define EPS_MODE_SET_POWERPOINT       (0x54)
#define EPS_MODE_SET_HEATER           (0x55)
#define EPS_MODE_CONFIG_CMD           (0x61)
#define EPS_MODE_CONFIG_GET           (0x62)
#define EPS_MODE_CONFIG_SET           (0x63)
#define EPS_MODE_CONFIG2_CMD          (0x71)
#define EPS_MODE_CONFIG2_GET          (0x72)
#define EPS_MODE_CONFIG2_SET          (0x73)

struct eps_setsingle_config {
	int id_isset;
	gom_eps_channelid_t id;
	int status_isset;
	gom_eps_output_status_t status;
	unsigned short delay;
};

struct eps_setpv_config {
	int numset;
	unsigned short voltages[3];
};

struct eps_setppt_config {
	int isset;
	gom_eps_power_point_mode_t mode;
};

struct eps_setheater_config {
	int isset;
	gom_eps_heater_cmdparams_t params;
};

static void print_help(const char *fname);
static void print_usage(const char *fname);

static int eps_initialize(const char *fname, unsigned char i2caddr);
static int eps_ping(const char *fname, unsigned char pingbyte);
static int eps_reset_soft(const char *fname, int ignore_confirmation);
static int eps_reset_hard(const char *fname, int ignore_confirmation);
static int eps_reset_counters(const char *fname, int ignore_confirmation);
static int eps_reset_wdt(const char *fname, int ignore_confirmation);
static int eps_hk_param(const char *fname, int hkfmt_raw);
static int eps_hk_general(const char *fname, int hkfmt_raw);
static int eps_hk_vi(const char *fname, int hkfmt_raw);
static int eps_hk_out(const char *fname, int hkfmt_raw);
static int eps_hk_wdt(const char *fname, int hkfmt_raw);
static int eps_hk_basic(const char *fname, int hkfmt_raw);
static int eps_set_output_all(const char *fname);
static int eps_set_output_single(const char *fname, struct eps_setsingle_config *conf);
static int eps_set_photovoltaic(const char *fname, struct eps_setpv_config *conf);
static int eps_set_powerpoint(const char *fname, struct eps_setppt_config *conf);
static int eps_set_heater(const char *fname, struct eps_setheater_config *conf);
static int eps_conf_cmd(const char *fname, unsigned char cmd);
static int eps_conf_get(const char *fname, int fmt_raw);
static int eps_conf_set(const char *fname);
static int eps_conf2_cmd(const char *fname, unsigned char cmd);
static int eps_conf2_get(const char *fname, int fmt_raw);
static int eps_conf2_set(const char *fname);

static void eps_printfmthk_vi(const uint16_t *vboost,
                              uint16_t vbatt,
                              const uint16_t *curin,
                              uint16_t cursun,
                              uint16_t cursys);
static void eps_printfmthk_out(const uint16_t *curout,
                               const uint8_t *output,
                               const uint16_t *output_on_delta,
                               const uint16_t *output_off_delta,
                               const uint16_t *latchup);
static void eps_printfmthk_wdt(uint32_t wdt_i2c_time_left,
                               uint32_t wdt_gnd_time_left,
                               const uint8_t *wdt_csp_pings_left,
                               uint32_t counter_wdt_i2c,
                               uint32_t counter_wdt_gnd,
                               const uint32_t *counter_wdt_csp);
static void eps_printfmthk_basic(uint32_t counter_boot,
                                 const int16_t *temp,
                                 uint8_t bootcause,
                                 uint8_t battmode,
                                 uint8_t pptmode);

int eps_main(int argc, char **argv)
{
	int ret;
	int choice;
	int value;
	int mode = EPS_MODE_NONE;
	unsigned char i2caddr = EPS_ADDR_DEFAULT;
	unsigned char pingbyte = 0x00;
	int pingbyte_isset = 0;
	int ignore_confirmation = 0;
	int fmt_raw = 0;
	unsigned char confcmd = 0x00;
	int confcmd_isset = 0;
	struct eps_setsingle_config setsingleconf;
	struct eps_setpv_config setpvconf;
	struct eps_setppt_config setpptconf;
	struct eps_setheater_config setheaterconf;
	setsingleconf.id_isset = 0;
	setsingleconf.status_isset = 0;
	setsingleconf.delay = EPS_DELAY_DEFAULT;
	setpvconf.numset = 0;
	setpptconf.isset = 0;
	setheaterconf.isset = 0;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	if (strcmp(argv[1], "initialize") == 0) { /* INITIALIZE MODE */
		mode = EPS_MODE_INITIALIZE;
		optind = 2;
	} else if (strcmp(argv[1], "init") == 0) { /* INITIALIZE ALIASES */
		mode = EPS_MODE_INITIALIZE;
		optind = 2;
	} else if (strcmp(argv[1], "ping") == 0) {  /* PING MODE */
		mode = EPS_MODE_PING;
		optind = 2;
	} else if (strcmp(argv[1], "reset") == 0) {  /* RESET MODE */
		if (argc < 3) {
			fprintf(stderr, "%s reset: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "soft") == 0) {
			mode = EPS_MODE_RESET_SOFT;
			optind = 3;
		} else if (strcmp(argv[2], "hard") == 0) {
			mode = EPS_MODE_RESET_HARD;
			optind = 3;
		} else if (strcmp(argv[2], "counters") == 0) {
			mode = EPS_MODE_RESET_COUNTERS;
			optind = 3;
		} else if (strcmp(argv[2], "wdt") == 0) {
			mode = EPS_MODE_RESET_WDT;
			optind = 3;
		} else {
			fprintf(stderr, "%s reset: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "rs") == 0) { /* RESET ALIASES */
		mode = EPS_MODE_RESET_SOFT;
		optind = 2;
	} else if (strcmp(argv[1], "rh") == 0) {
		mode = EPS_MODE_RESET_HARD;
		optind = 2;
	} else if (strcmp(argv[1], "rc") == 0) {
		mode = EPS_MODE_RESET_COUNTERS;
		optind = 2;
	} else if (strcmp(argv[1], "rw") == 0) {
		mode = EPS_MODE_RESET_WDT;
		optind = 2;
	} else if (strcmp(argv[1], "housekeeping") == 0) {  /* HOUSEKEEPING MODE */
		if (argc < 3) {
			fprintf(stderr, "%s housekeeping: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "param") == 0) {
			mode = EPS_MODE_HOUSEKEEPING_PARAM;
			optind = 3;
		} else if (strcmp(argv[2], "general") == 0) {
			mode = EPS_MODE_HOUSEKEEPING_GENERAL;
			optind = 3;
		} else if (strcmp(argv[2], "vi") == 0) {
			mode = EPS_MODE_HOUSEKEEPING_VI;
			optind = 3;
		} else if (strcmp(argv[2], "out") == 0) {
			mode = EPS_MODE_HOUSEKEEPING_OUT;
			optind = 3;
		} else if (strcmp(argv[2], "wdt") == 0) {
			mode = EPS_MODE_HOUSEKEEPING_WDT;
			optind = 3;
		} else if (strcmp(argv[2], "basic") == 0) {
			mode = EPS_MODE_HOUSEKEEPING_BASIC;
			optind = 3;
		} else {
			fprintf(stderr, "%s housekeeping: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "hkp") == 0) { /* HOUSEKEEPING ALIASES */
		mode = EPS_MODE_HOUSEKEEPING_PARAM;
		optind = 2;
	} else if (strcmp(argv[1], "hkg") == 0) {
		mode = EPS_MODE_HOUSEKEEPING_GENERAL;
		optind = 2;
	} else if (strcmp(argv[1], "hkvi") == 0) {
		mode = EPS_MODE_HOUSEKEEPING_VI;
		optind = 2;
	} else if (strcmp(argv[1], "hko") == 0) {
		mode = EPS_MODE_HOUSEKEEPING_OUT;
		optind = 2;
	} else if (strcmp(argv[1], "hkw") == 0) {
		mode = EPS_MODE_HOUSEKEEPING_WDT;
		optind = 2;
	} else if (strcmp(argv[1], "hkb") == 0) {
		mode = EPS_MODE_HOUSEKEEPING_BASIC;
		optind = 2;
	} else if (strcmp(argv[1], "set") == 0) {  /* SET MODE */
		if (argc < 3) {
			fprintf(stderr, "%s set: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "output") == 0) {
			if ((argc > 3) && (strcmp(argv[3], "all") == 0)) {
				mode = EPS_MODE_SET_OUTPUT_ALL;
				optind = 4;
			} else {
				mode = EPS_MODE_SET_OUTPUT_SINGLE;
				optind = 3;
			}
		} else if (strcmp(argv[2], "photovoltaic") == 0) {
			mode = EPS_MODE_SET_PHOTOVOLTAIC;
			optind = 3;
		} else if (strcmp(argv[2], "powerpoint") == 0) {
			mode = EPS_MODE_SET_POWERPOINT;
			optind = 3;
		} else if (strcmp(argv[2], "heater") == 0) {
			mode = EPS_MODE_SET_HEATER;
			optind = 3;
		} else {
			fprintf(stderr, "%s set: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "so") == 0) { /* SET ALIASES */
		mode = EPS_MODE_SET_OUTPUT_SINGLE;
		optind = 2;
	} else if (strcmp(argv[1], "soa") == 0) {
		mode = EPS_MODE_SET_OUTPUT_ALL;
		optind = 2;
	} else if (strcmp(argv[1], "spv") == 0) {
		mode = EPS_MODE_SET_PHOTOVOLTAIC;
		optind = 2;
	} else if (strcmp(argv[1], "sppt") == 0) {
		mode = EPS_MODE_SET_POWERPOINT;
		optind = 2;
	} else if (strcmp(argv[1], "sh") == 0) {
		mode = EPS_MODE_SET_HEATER;
		optind = 2;
	} else if ((strcmp(argv[1], "config") == 0) || (strcmp(argv[1], "conf") == 0)) {  /* CONFIG MODE */
		if (argc < 3) {
			fprintf(stderr, "%s config: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "cmd") == 0) {
			mode = EPS_MODE_CONFIG_CMD;
			optind = 3;
		} else if (strcmp(argv[2], "get") == 0) {
			mode = EPS_MODE_CONFIG_GET;
			optind = 3;
		} else if (strcmp(argv[2], "set") == 0) {
			mode = EPS_MODE_CONFIG_SET;
			optind = 3;
		} else {
			fprintf(stderr, "%s config: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "cc") == 0) { /* CONFIG ALIASES */
		mode = EPS_MODE_CONFIG_CMD;
		optind = 2;
	} else if (strcmp(argv[1], "cg") == 0) {
		mode = EPS_MODE_CONFIG_GET;
		optind = 2;
	} else if (strcmp(argv[1], "cs") == 0) {
		mode = EPS_MODE_CONFIG_SET;
		optind = 2;
	} else if ((strcmp(argv[1], "config2") == 0) || (strcmp(argv[1], "conf2") == 0)) {  /* CONFIG2 MODE */
		if (argc < 3) {
			fprintf(stderr, "%s config2: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "cmd") == 0) {
			mode = EPS_MODE_CONFIG2_CMD;
			optind = 3;
		} else if (strcmp(argv[2], "get") == 0) {
			mode = EPS_MODE_CONFIG2_GET;
			optind = 3;
		} else if (strcmp(argv[2], "set") == 0) {
			mode = EPS_MODE_CONFIG2_SET;
			optind = 3;
		} else {
			fprintf(stderr, "%s config2: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "c2c") == 0) { /* CONFIG2 ALIASES */
		mode = EPS_MODE_CONFIG2_CMD;
		optind = 2;
	} else if (strcmp(argv[1], "c2g") == 0) {
		mode = EPS_MODE_CONFIG2_GET;
		optind = 2;
	} else if (strcmp(argv[1], "c2s") == 0) {
		mode = EPS_MODE_CONFIG2_SET;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	while ((choice = getopt(argc, argv, "-ha:fr")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			switch (mode) {
			case EPS_MODE_PING:
				if (!pingbyte_isset) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					} else if ((value < 0) || (value > 0xFF)) {
						fprintf(stderr, "%s: \'%s\' does not fit into a single byte\n\r", argv[0], optarg);
						return 1;
					}
					pingbyte = (unsigned char) value;
					pingbyte_isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case EPS_MODE_SET_OUTPUT_SINGLE:
				if (!setsingleconf.id_isset) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					} else if ((value < 0) || (value > 7)) {
						fprintf(stderr, "%s: id \'%s\' must be a value between 0 and 7\n\r", argv[0], optarg);
						return 1;
					}
					setsingleconf.id = (gom_eps_channelid_t) value;
					setsingleconf.id_isset = 1;
				} else if (!setsingleconf.status_isset) {
					if (strcmp(optarg, "on") == 0) {
						setsingleconf.status = gomeps_channel_on;
					} else if (strcmp(optarg, "off") == 0) {
						setsingleconf.status = gomeps_channel_off;
					} else {
						fprintf(stderr, "%s: status \'%s\' must be either on or off\n\r", argv[0], optarg);
						return 1;
					}
					setsingleconf.status_isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case EPS_MODE_SET_PHOTOVOLTAIC:
				if (setpvconf.numset < 3) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					} else if ((value < 0) || (value > 0xFFFF)) {
						fprintf(stderr, "%s: \'%s\' does not fit into a 16-bit unsigned integer\n\r", argv[0], optarg);
						return 1;
					}
					setpvconf.voltages[setpvconf.numset++] = (unsigned short) value;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case EPS_MODE_SET_POWERPOINT:
				if (!setpptconf.isset) {
					if ((strcmp(optarg, "hardware") == 0) || (strcmp(optarg, "h") == 0)) {
						setpptconf.mode = gomeps_ppt_hardware;
					} else if ((strcmp(optarg, "maximum") == 0) || (strcmp(optarg, "m") == 0)) {
						setpptconf.mode = gomeps_ppt_maximum;
					} else if ((strcmp(optarg, "software") == 0) || (strcmp(optarg, "s") == 0)) {
						setpptconf.mode = gomeps_ppt_software;
					} else {
						fprintf(stderr, "%s: unknown power point mode \'%s\'\n\r", argv[0], optarg);
						return 1;
					}
					setpptconf.isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case EPS_MODE_SET_HEATER:
				if (!setheaterconf.isset) {
					// FIXME Parse the heater to set here
					setheaterconf.params.fields.heater = heater_bp4;
					if (strcmp(optarg, "on") == 0) {
						setheaterconf.params.fields.mode = mode_on;
					} else if (strcmp(optarg, "off") == 0) {
						setheaterconf.params.fields.mode = mode_off;
					} else {
						fprintf(stderr, "%s: auto heater mode \'%s\' must be either on or off\n\r", argv[0], optarg);
						return 1;
					}
					setheaterconf.isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case EPS_MODE_CONFIG_CMD:
			case EPS_MODE_CONFIG2_CMD:
				if (!confcmd_isset) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					} else if ((value < 0) || (value > 0xFF)) {
						fprintf(stderr, "%s: \'%s\' does not fit into a single byte\n\r", argv[0], optarg);
						return 1;
					}
					confcmd = (unsigned char) value;
					confcmd_isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			default:
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'a': /* I2C Address */
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
				return 1;
			} else if ((value < 0) || (value > 0x7F)) {
				fprintf(stderr, "%s: \'%s\' is not a valid I2C address\n\r", argv[0], optarg);
				return 1;
			}
			i2caddr = (unsigned char) value;
			break;
		case 'f': /* Ignore Confirmation Prompt */
			ignore_confirmation = 1;
			break;
		case 'r': /* Format Housekeeping as Raw Values */
			fmt_raw = 1;
			break;
		case 'd': /* Delay (in seconds) */
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
				return 1;
			} else if ((value < 0) || (value > 0xFFFF)) {
				fprintf(stderr, "%s: \'%s\' does not fit into a 16-bit unsigned integer\n\r", argv[0], optarg);
				return 1;
			}
			setsingleconf.delay = (unsigned short) value;
			break;
		case 'h': /* Help */
			print_help(argv[0]);
			return 0;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check if required positional are set
	switch (mode) {
	case EPS_MODE_PING:
		if (!pingbyte_isset) {
			fprintf(stderr, "%s: missing ping byte\n\r", argv[0]);
			return 1;
		}
		break;
	case EPS_MODE_SET_OUTPUT_SINGLE:
		if (!setsingleconf.id_isset) {
			fprintf(stderr, "%s: missing id\n\r", argv[0]);
			return 1;
		}
		if (!setsingleconf.status_isset) {
			fprintf(stderr, "%s: missing status\n\r", argv[0]);
			return 1;
		}
		break;
	case EPS_MODE_SET_PHOTOVOLTAIC:
		if (setpvconf.numset != 3) {
			fprintf(stderr, "%s: missing photovoltaic inputs\n\r", argv[0]);
			return 1;
		}
		break;
	case EPS_MODE_SET_POWERPOINT:
		if (!setpptconf.isset) {
			fprintf(stderr, "%s: missing power point mode\n\r", argv[0]);
			return 1;
		}
		break;
	case EPS_MODE_SET_HEATER:
		if (!setheaterconf.isset) {
			fprintf(stderr, "%s: missing heater auto mode\n\r", argv[0]);
			return 1;
		}
		break;
	case EPS_MODE_CONFIG_CMD:
	case EPS_MODE_CONFIG2_CMD:
		if (!confcmd_isset) {
			fprintf(stderr, "%s: missing configuration command\n\r", argv[0]);
			return 1;
		}
		break;
	default:
		break;
	}

	ret = 1;
	switch (mode) {
	case EPS_MODE_INITIALIZE:
		ret = eps_initialize(argv[0], i2caddr);
		break;
	case EPS_MODE_PING:
		ret = eps_ping(argv[0], pingbyte);
		break;
	case EPS_MODE_RESET_SOFT:
		ret = eps_reset_soft(argv[0], ignore_confirmation);
		break;
	case EPS_MODE_RESET_HARD:
		ret = eps_reset_hard(argv[0], ignore_confirmation);
		break;
	case EPS_MODE_RESET_COUNTERS:
		ret = eps_reset_counters(argv[0], ignore_confirmation);
		break;
	case EPS_MODE_RESET_WDT:
		ret = eps_reset_wdt(argv[0], ignore_confirmation);
		break;
	case EPS_MODE_HOUSEKEEPING_PARAM:
		ret = eps_hk_param(argv[0], fmt_raw);
		break;
	case EPS_MODE_HOUSEKEEPING_GENERAL:
		ret = eps_hk_general(argv[0], fmt_raw);
		break;
	case EPS_MODE_HOUSEKEEPING_VI:
		ret = eps_hk_vi(argv[0], fmt_raw);
		break;
	case EPS_MODE_HOUSEKEEPING_OUT:
		ret = eps_hk_out(argv[0], fmt_raw);
		break;
	case EPS_MODE_HOUSEKEEPING_WDT:
		ret = eps_hk_wdt(argv[0], fmt_raw);
		break;
	case EPS_MODE_HOUSEKEEPING_BASIC:
		ret = eps_hk_basic(argv[0], fmt_raw);
		break;
	case EPS_MODE_SET_OUTPUT_ALL:
		ret = eps_set_output_all(argv[0]);
		break;
	case EPS_MODE_SET_OUTPUT_SINGLE:
		ret = eps_set_output_single(argv[0], &setsingleconf);
		break;
	case EPS_MODE_SET_PHOTOVOLTAIC:
		ret = eps_set_photovoltaic(argv[0], &setpvconf);
		break;
	case EPS_MODE_SET_POWERPOINT:
		ret = eps_set_powerpoint(argv[0], &setpptconf);
		break;
	case EPS_MODE_SET_HEATER:
		ret = eps_set_heater(argv[0], &setheaterconf);
		break;
	case EPS_MODE_CONFIG_CMD:
		ret = eps_conf_cmd(argv[0], confcmd);
		break;
	case EPS_MODE_CONFIG_GET:
		ret = eps_conf_get(argv[0], fmt_raw);
		break;
	case EPS_MODE_CONFIG_SET:
		ret = eps_conf_set(argv[0]);
		break;
	case EPS_MODE_CONFIG2_CMD:
		ret = eps_conf2_cmd(argv[0], confcmd);
		break;
	case EPS_MODE_CONFIG2_GET:
		ret = eps_conf2_get(argv[0], fmt_raw);
		break;
	case EPS_MODE_CONFIG2_SET:
		ret = eps_conf2_set(argv[0]);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Accessability functions for the GOMspace EPS.\n\r\n\r", fname);
	fprintf(stderr, "    Make sure that \'board i2cslave\' variable is set to 1. If not,\n\r");
	fprintf(stderr, "    use GOMspace Shell (GOSH) to set it to 1 and reboot the EPS.\n\r");
	fprintf(stderr, "    You can also check the current value of the \'board i2cslave\' in GOSH.\n\r");
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s initialize [-h] [-a i2caddr]\n\r", fname);
	fprintf(stderr, "       %s ping [-h] <byte>\n\r", fname);
	fprintf(stderr, "       %s reset (hard|soft|counters|wdt) [-hf]\n\r", fname);
	fprintf(stderr, "       %s housekeeping (param|general|vi|out|wdt|basic) [-hr]\n\r", fname);
	fprintf(stderr, "       %s set output all [-h]\n\r", fname);
	fprintf(stderr, "       %s set output [-h] [-d delay] <id> <on|off>\n\r", fname);
	fprintf(stderr, "       %s set photovoltaic [-h] <volt1> <volt2> <volt3>\n\r", fname);
	fprintf(stderr, "       %s set powerpoint [-h] <hardware|maximum|software>\n\r", fname);
	fprintf(stderr, "       %s set heater [-h] <on|off>\n\r", fname);
	fprintf(stderr, "       %s config cmd [-h] <value>\n\r", fname);
	fprintf(stderr, "       %s config get [-hr]\n\r", fname);
	fprintf(stderr, "       %s config set [-h]\n\r", fname);
	fprintf(stderr, "       %s config2 cmd [-h] <value>\n\r", fname);
	fprintf(stderr, "       %s config2 get [-hr]\n\r", fname);
	fprintf(stderr, "       %s config2 set [-h]\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -a: Specifies the I2C address of the EPS (default: 0x%02x).\n\r", EPS_ADDR_DEFAULT);
	fprintf(stderr, "    -f: Ignore confirmation prompt.\n\r");
	fprintf(stderr, "    -r: Print housekeeping values as raw integer/byte values.\n\r");
	fprintf(stderr, "    -d: Specify a delay value in seconds (default: %hu).\n\r", EPS_DELAY_DEFAULT);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Mode aliases:\n\r");
	fprintf(stderr, "    init:             initialize\n\r");
	fprintf(stderr, "    r(s|h|c|w):       reset (soft|hard|counters|wdt)\n\r");
	fprintf(stderr, "    hk(p|g|vi|o|w|b): housekeeping (param|general|vi|out|wdt|basic)\n\r");
	fprintf(stderr, "    so:               set output\n\r");
	fprintf(stderr, "    soa:              set output all\n\r");
	fprintf(stderr, "    spv:              set photovoltaic\n\r");
	fprintf(stderr, "    sppt <h|m|s>:     set powerpoint <hardware|maximum|software>\n\r");
	fprintf(stderr, "    sh:               set heater\n\r");
	fprintf(stderr, "    c(c|g|s):         config (cmd|get|set)\n\r");
	fprintf(stderr, "    c2(c|g|s):        config2 (cmd|get|set)\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Initialize the EPS with the specified I2C address.
 */
static int eps_initialize(const char *fname, unsigned char i2caddr)
{
	int ret;

	// Initialize a single EPS
	ret = GomEpsInitialize(&i2caddr, 1);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s initialize: GomEpsInitialize returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Ping the EPS with the entered byte.
 */
static int eps_ping(const char *fname, unsigned char pingbyte)
{
	int ret;
	unsigned char outbyte;

	ret = GomEpsPing(0, pingbyte, &outbyte);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s ping: GomEpsPing returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "EPS Ping Response: %u (0x%02x)\n\r", outbyte, outbyte);

	return 0;
}

/**
 * Perform a soft reset of the EPS.
 */
static int eps_reset_soft(const char *fname, int ignore_confirmation)
{
	int ret;
	int confirmed;

	if (!ignore_confirmation) {
		// Confirm the reset
		confirmed = serial_promptconfirm("Are you sure that you want to soft reset the EPS?", 0);
		if (!confirmed) {
			fprintf(stderr, "%s: EPS was not reset\n\r", fname);
			return 0;
		}
	}

	ret = GomEpsSoftReset(0);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s reset soft: GomEpsSoftReset returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Perform a hard reset of the EPS.
 */
static int eps_reset_hard(const char *fname, int ignore_confirmation)
{
	int ret;
	int confirmed;

	if (!ignore_confirmation) {
		// Confirm the reset
		confirmed = serial_promptconfirm("Are you sure that you want to hard reset the EPS?", 0);
		if (!confirmed) {
			fprintf(stderr, "%s: EPS was not reset\n\r", fname);
			return 0;
		}
	}

	ret = GomEpsHardReset(0);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s reset hard: GomEpsHardReset returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Resets the counters on the EPS.
 */
static int eps_reset_counters(const char *fname, int ignore_confirmation)
{
	int ret;
	int confirmed;

	if (!ignore_confirmation) {
		// Confirm the reset
		confirmed = serial_promptconfirm("Are you sure that you want to reset the EPS counters?", 0);
		if (!confirmed) {
			fprintf(stderr, "%s: EPS counters were not reset\n\r", fname);
			return 0;
		}
	}

	ret = GomEpsResetCounters(0);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s reset counters: GomEpsResetCounters returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Resets the watchdog timer on the EPS.
 */
static int eps_reset_wdt(const char *fname, int ignore_confirmation)
{
	int ret;
	int confirmed;

	if (!ignore_confirmation) {
		// Confirm the reset
		confirmed = serial_promptconfirm("Are you sure that you want to reset the EPS watchdog timer?", 0);
		if (!confirmed) {
			fprintf(stderr, "%s: EPS watchdog timer was not reset\n\r", fname);
			return 0;
		}
	}

	ret = GomEpsResetWDT(0);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s reset wdt: GomEpsResetWDT returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Retrieves the gom_eps_hkparam_t (Legacy) from the EPS and prints it.
 */
static int eps_hk_param(const char *fname, int hkfmt_raw)
{
	int ret;
	gom_eps_hkparam_t hk;

	static const char *bootcauses[] = {"Unknown reset",
	                                   "Dedicated WDT reset",
	                                   "I2C WDT reset",
	                                   "Hard reset",
	                                   "Soft reset",
	                                   "Stack overflow",
	                                   "Timer overflow",
	                                   "Brownout or power-on reset",
	                                   "Internal WDT reset"};

	static const char *pptmodes[] = {"Hardware", "Maximum PPT", "Fixed Software PPT", "Unknown"};

	static const char *statusstr[] = {"Off", "On "};

	ret = GomEpsGetHkData_param(0, &hk);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s housekeeping param: GomEpsGetHkData_param returned %d\n\r", fname, ret);
		return 1;
	}

	if (hkfmt_raw) {
		fprintf(stderr, "Raw GOMspace EPS param housekeeping:\n\r");
		fprintf(stderr, " - commandReply:        %hu\n\r", hk.fields.commandReply);
		fprintf(stderr, " - photoVoltaic3:       %hu\n\r", hk.fields.photoVoltaic3);
		fprintf(stderr, " - photoVoltaic2:       %hu\n\r", hk.fields.photoVoltaic2);
		fprintf(stderr, " - photoVoltaic1:       %hu\n\r", hk.fields.photoVoltaic1);
		fprintf(stderr, " - pc:                  %hu\n\r", hk.fields.pc);
		fprintf(stderr, " - bv:                  %hu\n\r", hk.fields.bv);
		fprintf(stderr, " - sc:                  %hu\n\r", hk.fields.sc);
		fprintf(stderr, " - tempConverter1:      %hd\n\r", hk.fields.tempConverter1);
		fprintf(stderr, " - tempConverter2:      %hd\n\r", hk.fields.tempConverter2);
		fprintf(stderr, " - tempConverter3:      %hd\n\r", hk.fields.tempConverter3);
		fprintf(stderr, " - tempBattery:         %hd\n\r", hk.fields.tempBattery);
		fprintf(stderr, " - batteryBoardTemp[0]: %hd\n\r", hk.fields.batteryBoardTemp[0]);
		fprintf(stderr, " - batteryBoardTemp[1]: %hd\n\r", hk.fields.batteryBoardTemp[1]);
		fprintf(stderr, " - latchUp3v3Channel3:  %hu\n\r", hk.fields.latchUp3v3Channel3);
		fprintf(stderr, " - latchUp3v3Channel2:  %hu\n\r", hk.fields.latchUp3v3Channel2);
		fprintf(stderr, " - latchUp3v3Channel1:  %hu\n\r", hk.fields.latchUp3v3Channel1);
		fprintf(stderr, " - latchUp5vChannel3:   %hu\n\r", hk.fields.latchUp5vChannel3);
		fprintf(stderr, " - latchUp5vChannel2:   %hu\n\r", hk.fields.latchUp5vChannel2);
		fprintf(stderr, " - latchUp5vChannel1:   %hu\n\r", hk.fields.latchUp5vChannel1);
		fprintf(stderr, " - reset:               %u\n\r", hk.fields.reset);
		fprintf(stderr, " - bootCount:           %hu\n\r", hk.fields.bootCount);
		fprintf(stderr, " - swErrors:            %hu\n\r", hk.fields.swErrors);
		fprintf(stderr, " - pptMode:             %u\n\r", hk.fields.pptMode);
		fprintf(stderr, " - channelStatus:       0x%02X\n\r", hk.fields.channelStatus.raw);
	} else {
		if (hk.fields.reset > 8)
			hk.fields.reset = 0;

		if (hk.fields.pptMode > 3)
			hk.fields.pptMode = 3;

		fprintf(stderr, "[EPS Backwards Compatible Housekeeping]__________    ________________________________\n\r");
		fprintf(stderr, "|                                    |          |    |                              |\n\r");
		fprintf(stderr, "| Photo-voltaic 1 input voltage      | %5hu mV |    |         TEMPERATURES         |\n\r", hk.fields.photoVoltaic1);
		fprintf(stderr, "| Photo-voltaic 2 input voltage      | %5hu mV |    |______________________________|\n\r", hk.fields.photoVoltaic1);
		fprintf(stderr, "| Photo-voltaic 3 input voltage      | %5hu mV |    |                |             |\n\r", hk.fields.photoVoltaic1);
		fprintf(stderr, "| Total photo-voltaic input current  | %5hu mA |    | Boost Conv. 1  | %6hd degC |\n\r", hk.fields.pc, hk.fields.tempConverter1);
		fprintf(stderr, "| Battery voltage                    | %5hu mV |    | Boost Conv. 2  | %6hd degC |\n\r", hk.fields.bv, hk.fields.tempConverter2);
		fprintf(stderr, "| Total system current               | %5hu mA |    | Boost Conv. 3  | %6hd degC |\n\r", hk.fields.sc, hk.fields.tempConverter3);
		fprintf(stderr, "|____________________________________|__________|    | Battery        | %6hd degC |\n\r", hk.fields.tempBattery);
		fprintf(stderr, "                                                     | Batt. Board 1  | %6hd degC |\n\r", hk.fields.batteryBoardTemp[0]);
		fprintf(stderr, "_________________________________________________    | Batt. Board 2  | %6hd degC |\n\r", hk.fields.batteryBoardTemp[1]);
		fprintf(stderr, "|                  |                            |    |________________|_____________|\n\r");
		fprintf(stderr, "| Last reset cause | %26s |\n\r", bootcauses[hk.fields.reset]);
		fprintf(stderr, "| Boot counter     | %26hd |    ___________________________________\n\r", hk.fields.bootCount);
		fprintf(stderr, "| Software errors  | %26hd |    |             |        |          |\n\r", hk.fields.swErrors);
		fprintf(stderr, "| Power point mode | %26s |    |   CHANNEL   | STATUS | LATCHUPS |\n\r", pptmodes[hk.fields.pptMode]);
		fprintf(stderr, "|__________________|____________________________|    |_____________|________|__________|\n\r");
		fprintf(stderr, "                                                     |             |        |          |\n\r");
		fprintf(stderr, "                                                     | 5V1         | %s    | %8hd |\n\r", statusstr[hk.fields.channelStatus.fields.channel5V_1], hk.fields.latchUp5vChannel1);
		fprintf(stderr, "                                                     | 5V2         | %s    | %8hd |\n\r", statusstr[hk.fields.channelStatus.fields.channel5V_2], hk.fields.latchUp5vChannel2);
		fprintf(stderr, "                                                     | 5V3         | %s    | %8hd |\n\r", statusstr[hk.fields.channelStatus.fields.channel5V_3], hk.fields.latchUp5vChannel3);
		fprintf(stderr, "                                                     | 3.3V1       | %s    | %8hd |\n\r", statusstr[hk.fields.channelStatus.fields.channel3V3_1], hk.fields.latchUp3v3Channel1);
		fprintf(stderr, "                                                     | 3.3V2       | %s    | %8hd |\n\r", statusstr[hk.fields.channelStatus.fields.channel3V3_2], hk.fields.latchUp3v3Channel2);
		fprintf(stderr, "                                                     | 3.3V3       | %s    | %8hd |\n\r", statusstr[hk.fields.channelStatus.fields.channel3V3_3], hk.fields.latchUp3v3Channel3);
		fprintf(stderr, "                                                     | BP4 Switch  | %s    |          |\n\r", statusstr[hk.fields.channelStatus.fields.quadbatSwitch]);
		fprintf(stderr, "                                                     | BP4 Heater  | %s    |          |\n\r", statusstr[hk.fields.channelStatus.fields.quadbatHeater]);
		fprintf(stderr, "                                                     |_____________|________|__________|\n\r");
	}

	return 0;
}

/**
 * Retrieves the gom_eps_hk_t (All) from the EPS and prints it.
 */
static int eps_hk_general(const char *fname, int hkfmt_raw)
{
	int ret;
	int i;
	gom_eps_hk_t hk;

	ret = GomEpsGetHkData_general(0, &hk);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s housekeeping general: GomEpsGetHkData_general returned %d\n\r", fname, ret);
		return 1;
	}

	if (hkfmt_raw) {
		fprintf(stderr, "Raw GOMspace EPS general housekeeping:\n\r");
		fprintf(stderr, " - commandReply:          %hu\n\r", hk.fields.commandReply);
		for (i = 0; i < 3; i++) {
			fprintf(stderr, " - vboost[%d]:             %hu\n\r", i, hk.fields.vboost[i]);
		}
		fprintf(stderr, " - vbatt:                 %hu\n\r", hk.fields.vbatt);
		for (i = 0; i < 3; i++) {
			fprintf(stderr, " - curin[%d]:              %hu\n\r", i, hk.fields.curin[i]);
		}
		fprintf(stderr, " - cursun:                %hu\n\r", hk.fields.cursun);
		fprintf(stderr, " - cursys:                %hu\n\r", hk.fields.cursys);
		fprintf(stderr, " - reserved1:             %hu\n\r", hk.fields.reserved1);
		for (i = 0; i < 6; i++) {
			fprintf(stderr, " - curout[%d]:             %hu\n\r", i, hk.fields.curout[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output[%d]:             %u\n\r", i, hk.fields.output[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output_on_delta[%d]:    %hu\n\r", i, hk.fields.output_on_delta[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output_off_delta[%d]:   %hu\n\r", i, hk.fields.output_off_delta[i]);
		}
		for (i = 0; i < 6; i++) {
			fprintf(stderr, " - latchup[%d]:            %hu\n\r", i, hk.fields.latchup[i]);
		}
		fprintf(stderr, " - wdt_i2c_time_left:     %u\n\r", hk.fields.wdt_i2c_time_left);
		fprintf(stderr, " - wdt_gnd_time_left:     %u\n\r", hk.fields.wdt_gnd_time_left);
		for (i = 0; i < 2; i++) {
			fprintf(stderr, " - wdt_csp_pings_left[%d]: %u\n\r", i, hk.fields.wdt_csp_pings_left[i]);
		}
		fprintf(stderr, " - counter_wdt_i2c:       %u\n\r", hk.fields.counter_wdt_i2c);
		fprintf(stderr, " - counter_wdt_gnd:       %u\n\r", hk.fields.counter_wdt_gnd);
		for (i = 0; i < 2; i++) {
			fprintf(stderr, " - counter_wdt_csp[%d]:    %u\n\r", i, hk.fields.counter_wdt_csp[i]);
		}
		fprintf(stderr, " - counter_boot:          %u\n\r", hk.fields.counter_boot);
		for (i = 0; i < 6; i++) {
			fprintf(stderr, " - temp[%d]:               %hd\n\r", i, hk.fields.temp[i]);
		}
		fprintf(stderr, " - bootcause:             %u\n\r", hk.fields.bootcause);
		fprintf(stderr, " - battmode:              %u\n\r", hk.fields.battmode);
		fprintf(stderr, " - pptmode:               %u\n\r", hk.fields.pptmode);
		fprintf(stderr, " - reserved2:             %hu\n\r", hk.fields.reserved2);
	} else {
		eps_printfmthk_vi(hk.fields.vboost,
		                  hk.fields.vbatt,
		                  hk.fields.curin,
		                  hk.fields.cursun,
		                  hk.fields.cursys);
		fprintf(stderr, "\n\r");
		eps_printfmthk_out(hk.fields.curout,
		                   hk.fields.output,
		                   hk.fields.output_on_delta,
		                   hk.fields.output_off_delta,
		                   hk.fields.latchup);
		fprintf(stderr, "\n\r");
		eps_printfmthk_wdt(hk.fields.wdt_i2c_time_left,
		                   hk.fields.wdt_gnd_time_left,
		                   hk.fields.wdt_csp_pings_left,
		                   hk.fields.counter_wdt_i2c,
		                   hk.fields.counter_wdt_gnd,
		                   (const uint32_t *) hk.fields.counter_wdt_csp);
		fprintf(stderr, "\n\r");
		eps_printfmthk_basic(hk.fields.counter_boot,
		                     hk.fields.temp,
		                     hk.fields.bootcause,
		                     hk.fields.battmode,
		                     hk.fields.pptmode);
	}

	return 0;
}

/**
 * Retrieves the gom_eps_hk_vi_t (Voltage and Current) from the EPS and prints it.
 */
static int eps_hk_vi(const char *fname, int hkfmt_raw)
{
	int ret;
	int i;
	gom_eps_hk_vi_t hk;

	ret = GomEpsGetHkData_vi(0, &hk);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s housekeeping vi: GomEpsGetHkData_vi returned %d\n\r", fname, ret);
		return 1;
	}

	if (hkfmt_raw) {
		fprintf(stderr, "Raw GOMspace EPS VI (Voltage & Current) housekeeping:\n\r");
		fprintf(stderr, " - commandReply: %hu\n\r", hk.fields.commandReply);
		for (i = 0; i < 3; i++) {
			fprintf(stderr, " - vboost[%d]:    %hu\n\r", i, hk.fields.vboost[i]);
		}
		fprintf(stderr, " - vbatt:        %hu\n\r", hk.fields.vbatt);
		for (i = 0; i < 3; i++) {
			fprintf(stderr, " - curin[%d]:     %hu\n\r", i, hk.fields.curin[i]);
		}
		fprintf(stderr, " - cursun:       %hu\n\r", hk.fields.cursun);
		fprintf(stderr, " - cursys:       %hu\n\r", hk.fields.cursys);
		fprintf(stderr, " - reserved1:    %hu\n\r", hk.fields.reserved1);
	} else {
		eps_printfmthk_vi(hk.fields.vboost,
		                  hk.fields.vbatt,
		                  hk.fields.curin,
		                  hk.fields.cursun,
		                  hk.fields.cursys);
	}

	return 0;
}

/**
 * Retrieves the gom_eps_hk_out_t (Outputs) from the EPS and prints it.
 */
static int eps_hk_out(const char *fname, int hkfmt_raw)
{
	int ret;
	int i;
	gom_eps_hk_out_t hk;

	ret = GomEpsGetHkData_out(0, &hk);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s housekeeping out: GomEpsGetHkData_out returned %d\n\r", fname, ret);
		return 1;
	}

	if (hkfmt_raw) {
		fprintf(stderr, "Raw GOMspace EPS Output housekeeping:\n\r");
		fprintf(stderr, " - commandReply:        %hu\n\r", hk.fields.commandReply);
		for (i = 0; i < 6; i++) {
			fprintf(stderr, " - curout[%d]:           %hu\n\r", i, hk.fields.curout[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output[%d]:           %u\n\r", i, hk.fields.output[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output_on_delta[%d]:  %hu\n\r", i, hk.fields.output_on_delta[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output_off_delta[%d]: %hu\n\r", i, hk.fields.output_off_delta[i]);
		}
		for (i = 0; i < 6; i++) {
			fprintf(stderr, " - latchup[%d]:          %hu\n\r", i, hk.fields.latchup[i]);
		}
	} else {
		eps_printfmthk_out(hk.fields.curout,
		                   hk.fields.output,
		                   hk.fields.output_on_delta,
		                   hk.fields.output_off_delta,
		                   hk.fields.latchup);
	}

	return 0;
}

/**
 * Retrieves the gom_eps_hk_wdt_t (Watchdog Info) from the EPS and prints it.
 */
static int eps_hk_wdt(const char *fname, int hkfmt_raw)
{
	int ret;
	int i;
	gom_eps_hk_wdt_t hk;

	ret = GomEpsGetHkData_wdt(0, &hk);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s housekeeping wdt: GomEpsGetHkData_wdt returned %d\n\r", fname, ret);
		return 1;
	}

	if (hkfmt_raw) {
		fprintf(stderr, "Raw GOMspace EPS Watchdog housekeeping:\n\r");
		fprintf(stderr, " - commandReply:          %hu\n\r", hk.fields.commandReply);
		fprintf(stderr, " - wdt_i2c_time_left:     %u\n\r", hk.fields.wdt_i2c_time_left);
		fprintf(stderr, " - wdt_gnd_time_left:     %u\n\r", hk.fields.wdt_gnd_time_left);
		for (i = 0; i < 2; i++) {
			fprintf(stderr, " - wdt_csp_pings_left[%d]: %u\n\r", i, hk.fields.wdt_csp_pings_left[i]);
		}
		fprintf(stderr, " - counter_wdt_i2c:       %u\n\r", hk.fields.counter_wdt_i2c);
		fprintf(stderr, " - counter_wdt_gnd:       %u\n\r", hk.fields.counter_wdt_gnd);
		for (i = 0; i < 2; i++) {
			fprintf(stderr, " - counter_wdt_csp[%d]:    %u\n\r", i, hk.fields.counter_wdt_csp[i]);
		}
	} else {
		eps_printfmthk_wdt(hk.fields.wdt_i2c_time_left,
		                   hk.fields.wdt_gnd_time_left,
		                   hk.fields.wdt_csp_pings_left,
		                   hk.fields.counter_wdt_i2c,
		                   hk.fields.counter_wdt_gnd,
		                   (const uint32_t *) hk.fields.counter_wdt_csp);
	}

	return 0;
}

/**
 * Retrieves the gom_eps_hk_basic_t (Basic) from the EPS and prints it.
 */
static int eps_hk_basic(const char *fname, int hkfmt_raw)
{
	int ret;
	int i;
	gom_eps_hk_basic_t hk;

	ret = GomEpsGetHkData_basic(0, &hk);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s housekeeping basic: GomEpsGetHkData_basic returned %d\n\r", fname, ret);
		return 1;
	}

	if (hkfmt_raw) {
		fprintf(stderr, "Raw GOMspace EPS basic housekeeping:\n\r");
		fprintf(stderr, " - commandReply: %hu\n\r", hk.fields.commandReply);
		fprintf(stderr, " - counter_boot: %u\n\r", hk.fields.counter_boot);
		for (i = 0; i < 6; i++) {
			fprintf(stderr, " - temp[%d]:      %hd\n\r", i, hk.fields.temp[i]);
		}
		fprintf(stderr, " - bootcause:    %u\n\r", hk.fields.bootcause);
		fprintf(stderr, " - battmode:     %u\n\r", hk.fields.battmode);
		fprintf(stderr, " - pptmode:      %u\n\r", hk.fields.pptmode);
		fprintf(stderr, " - reserved2:    %hu\n\r", hk.fields.reserved2);
	} else {
		eps_printfmthk_basic(hk.fields.counter_boot,
		                     hk.fields.temp,
		                     hk.fields.bootcause,
		                     hk.fields.battmode,
		                     hk.fields.pptmode);
	}

	return 0;
}

/* Helper functions to avoid code duplication in eps_set_output_all */
static unsigned char eps_set_output_all_onoff(const char *fname, const char *channel_name)
{
	int len;
	char buf[4];
	fprintf(stderr, "Channel status for %s: [on/off] ", channel_name);
	len = serial_readline(buf, sizeof(buf));
	if (len < 1) {
		fprintf(stderr, "%s set output all: Error reading status\n\r", fname);
		return 0xff;
	}
	if (strcmp(buf, "on") == 0) {
		return 1;
	} else if (strcmp(buf, "off") == 0) {
		return 0;
	} else {
		fprintf(stderr, "%s set output all: Unknown status \'%s\'\n\r", fname, buf);
		return 0xff;
	}
}
static const char *eps_set_output_all_onoff_to_string(unsigned char status)
{
	const char *str_on = "on";
	const char *str_off = "off";

	if (status)
		return str_on;
	else
		return str_off;
}
/**
 * Set the output values on all channels.
 */
static int eps_set_output_all(const char *fname)
{
	int ret;
	int confirmed;
	unsigned char chret;
	gom_eps_channelstates_t out;

	chret = eps_set_output_all_onoff(fname, "channel5V_1");
	if (chret == 0xff)
		return 1;
	out.fields.channel5V_1 = chret;

	chret = eps_set_output_all_onoff(fname, "channel5V_2");
	if (chret == 0xff)
		return 1;
	out.fields.channel5V_2 = chret;

	chret = eps_set_output_all_onoff(fname, "channel5V_3");
	if (chret == 0xff)
		return 1;
	out.fields.channel5V_3 = chret;

	chret = eps_set_output_all_onoff(fname, "channel3V3_1");
	if (chret == 0xff)
		return 1;
	out.fields.channel3V3_1 = chret;

	chret = eps_set_output_all_onoff(fname, "channel3V3_2");
	if (chret == 0xff)
		return 1;
	out.fields.channel3V3_2 = chret;

	chret = eps_set_output_all_onoff(fname, "channel3V3_3");
	if (chret == 0xff)
		return 1;
	out.fields.channel3V3_3 = chret;

	chret = eps_set_output_all_onoff(fname, "quadbatSwitch");
	if (chret == 0xff)
		return 1;
	out.fields.quadbatSwitch = chret;

	chret = eps_set_output_all_onoff(fname, "quadbatHeater");
	if (chret == 0xff)
		return 1;
	out.fields.quadbatHeater = chret;

	fprintf(stderr, "Selected channel output states:\n\r");
	fprintf(stderr, " - channel5V_1: %s\n\r", eps_set_output_all_onoff_to_string(out.fields.channel5V_1));
	fprintf(stderr, " - channel5V_2: %s\n\r", eps_set_output_all_onoff_to_string(out.fields.channel5V_2));
	fprintf(stderr, " - channel5V_3: %s\n\r", eps_set_output_all_onoff_to_string(out.fields.channel5V_3));
	fprintf(stderr, " - channel3V3_1: %s\n\r", eps_set_output_all_onoff_to_string(out.fields.channel3V3_1));
	fprintf(stderr, " - channel3V3_2: %s\n\r", eps_set_output_all_onoff_to_string(out.fields.channel3V3_2));
	fprintf(stderr, " - channel3V3_3: %s\n\r", eps_set_output_all_onoff_to_string(out.fields.channel3V3_3));
	fprintf(stderr, " - quadbatSwitch: %s\n\r", eps_set_output_all_onoff_to_string(out.fields.quadbatSwitch));
	fprintf(stderr, " - quadbatHeater: %s\n\r", eps_set_output_all_onoff_to_string(out.fields.quadbatHeater));
	confirmed = serial_promptconfirm("Set EPS output to the following?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s set output all: EPS output was not modified.\n\r", fname);
		return 0;
	}

	ret = GomEpsSetOutput(0, out);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s set output all: GomEpsSetOutput returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Set the output on a single channel of the EPS.
 */
static int eps_set_output_single(const char *fname, struct eps_setsingle_config *conf)
{
	int ret;
	int confirmed;

	fprintf(stderr, "Set channel %d to ", conf->id);
	if (conf->status == gomeps_channel_on)
		fprintf(stderr, "on");
	else
		fprintf(stderr, "off");
	confirmed = serial_promptconfirm("?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s set output: EPS output was not modified.\n\r", fname);
		return 0;
	}

	ret = GomEpsSetSingleOutput(0, conf->id, conf->status, conf->delay);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s set output: GomEpsSetSingleOutput returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Sets the photovoltaic inputs of the EPS.
 */
static int eps_set_photovoltaic(const char *fname, struct eps_setpv_config *conf)
{
	int ret;
	int confirmed;

	fprintf(stderr, "Specified values:\n\r");
	fprintf(stderr, " - photovoltaic1: %hu mV\n\r", conf->voltages[0]);
	fprintf(stderr, " - photovoltaic2: %hu mV\n\r", conf->voltages[1]);
	fprintf(stderr, " - photovoltaic3: %hu mV\n\r", conf->voltages[2]);
	fprintf(stderr, "\n\r");
	confirmed = serial_promptconfirm("Set photovoltaic inputs to the specified values above?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s set photovoltaic: Photovoltaic inputs were not set.\n\r", fname);
		return 0;
	}

	ret = GomEpsSetPhotovoltaicInputs(0, conf->voltages[0], conf->voltages[1], conf->voltages[2]);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s set photovoltaic: GomEpsSetPhotovoltaicInputs returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Sets the power point mode of the EPS.
 */
static int eps_set_powerpoint(const char *fname, struct eps_setppt_config *conf)
{
	int ret;
	int confirmed;

	fprintf(stderr, "Set power point mode to \"");
	if (conf->mode == gomeps_ppt_hardware)
		fprintf(stderr, "hardware");
	else if (conf->mode == gomeps_ppt_maximum)
		fprintf(stderr, "maximum");
	else if (conf->mode == gomeps_ppt_software)
		fprintf(stderr, "software");
	confirmed = serial_promptconfirm("\"?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s set powerpoint: Power point mode was not set.\n\r", fname);
		return 0;
	}

	ret = GomEpsSetPptMode(0, conf->mode);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s set powerpoint: GomEpsSetPptMode returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Sets the heater auto mode of the EPS.
 */
static int eps_set_heater(const char *fname, struct eps_setheater_config *conf)
{
	int ret;
	int confirmed;
	gom_eps_heater_status_t stat;

	fprintf(stderr, "Set ");
	if (conf->params.fields.heater == heater_bp4)
		fprintf(stderr, "heater_bp4");
	else if (conf->params.fields.heater == heater_onboard)
		fprintf(stderr, "heater_onboard");
	else if (conf->params.fields.heater == heater_both)
		fprintf(stderr, "heater_both");
	fprintf(stderr, " mode to \"");
	if (conf->params.fields.mode == mode_on)
		fprintf(stderr, "on");
	else if (conf->params.fields.mode == mode_off)
		fprintf(stderr, "off");
	confirmed = serial_promptconfirm("\"?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s set heater: Heater mode was not set.\n\r", fname);
		return 0;
	}

	ret = GomEpsSetHeaterMode(0, conf->params, &stat);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s set heater: GomEpsSetHeaterAutoMode returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "(current mode of heater_bp4 is \"");
	if (stat.fields.bp4_heatermode == mode_on)
		fprintf(stderr, "on");
	else if (stat.fields.bp4_heatermode == mode_off)
		fprintf(stderr, "off");
	fprintf(stderr, "\" and heater_onboard is \"");
	if (stat.fields.onboard_heatermode == mode_on)
		fprintf(stderr, "on");
	else if (stat.fields.onboard_heatermode == mode_off)
		fprintf(stderr, "off");
	fprintf(stderr, "\")\n\r");

	return 0;
}

/**
 * Send a configuration command to the EPS
 */
static int eps_conf_cmd(const char *fname, unsigned char cmd)
{
	int ret;

	ret = GomEpsConfigCMD(0, cmd);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s config cmd: GomEpsConfigCMD returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Retrieve current configuration for the EPS.
 */
static int eps_conf_get(const char *fname, int fmt_raw)
{
	int i;
	int ret;
	eps_config_t conf;

	static const char *pptmodes[] = {"Unknown", "Auto", "Fixed"};
	static const char *bhmodes[] = {"Manual", "Auto", "Unknown"};
	static const char *statstr[] = {"Off", "On "};

	ret = GomEpsConfigGet(0, &conf);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s config get: GomEpsConfigGet returned %d\n\r", fname, ret);
		return 1;
	}

	if (fmt_raw) {
		fprintf(stderr, "Current GOMspace EPS Configuration (raw format):\n\r");
		fprintf(stderr, " - commandReply:                %hu\n\r", conf.fields.commandReply);
		fprintf(stderr, " - ppt_mode:                    %u\n\r", conf.fields.ppt_mode);
		fprintf(stderr, " - battheater_mode:             %u\n\r", conf.fields.battheater_mode);
		fprintf(stderr, " - battheater_low:              %d\n\r", conf.fields.battheater_low);
		fprintf(stderr, " - battheater_high:             %d\n\r", conf.fields.battheater_high);
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output_normal_value[%d]:      %u\n\r", i, conf.fields.output_normal_value[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output_safe_value[%d]:        %u\n\r", i, conf.fields.output_safe_value[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output_initial_on_delay[%d]:  %hu\n\r", i, conf.fields.output_initial_on_delay[i]);
		}
		for (i = 0; i < 8; i++) {
			fprintf(stderr, " - output_initial_off_delay[%d]: %hu\n\r", i, conf.fields.output_initial_off_delay[i]);
		}
		for (i = 0; i < 3; i++) {
			fprintf(stderr, " - vboost[%d]:                   %hu\n\r", i, conf.fields.vboost[i]);
		}
	} else {
		if (conf.fields.ppt_mode > 2)
			conf.fields.ppt_mode = 0;

		if (conf.fields.battheater_mode > 2)
			conf.fields.battheater_mode = 2;

		for (i = 0; i < 8; i++) {
			if (conf.fields.output_normal_value[i] > 2)
				conf.fields.output_normal_value[i] = 1;

			if (conf.fields.output_safe_value[i] > 2)
				conf.fields.output_safe_value[i] = 1;
		}

		fprintf(stderr, "[EPS Config]_______________________    ________________________________________________________\n\r");
		fprintf(stderr, "|                                 |    |             |          |        |         |          |\n\r");
		fprintf(stderr, "|      GENERAL CONFIGURATION      |    |             |  OUTPUT  | OUTPUT | INITIAL | INITIAL  |\n\r");
		fprintf(stderr, "|_________________________________|    |   CHANNEL   |  VALUE   | VALUE  | TURN ON | TURN OFF |\n\r");
		fprintf(stderr, "|                     |           |    |             | (NORMAL) | (SAFE) |  DELAY  |  DELAY   |\n\r");
		fprintf(stderr, "| Power point mode    | %9s |    |_____________|__________|________|_________|__________|\n\r", pptmodes[conf.fields.ppt_mode]);
		fprintf(stderr, "| Battery heater mode | %9s |    |             |          |        |         |          |\n\r", bhmodes[conf.fields.battheater_mode]);
		fprintf(stderr, "| Turn heater off at  | %4d degC |    | 5V1         | %s      | %s    | %5hu s |  %5hu s |\n\r", conf.fields.battheater_low, statstr[conf.fields.output_normal_value[0]], statstr[conf.fields.output_safe_value[0]], conf.fields.output_initial_on_delay[0], conf.fields.output_initial_off_delay[0]);
		fprintf(stderr, "| Turn heater on at   | %4d degC |    | 5V2         | %s      | %s    | %5hu s |  %5hu s |\n\r", conf.fields.battheater_high, statstr[conf.fields.output_normal_value[1]], statstr[conf.fields.output_safe_value[1]], conf.fields.output_initial_on_delay[1], conf.fields.output_initial_off_delay[1]);
		fprintf(stderr, "|_____________________|___________|    | 5V3         | %s      | %s    | %5hu s |  %5hu s |\n\r", statstr[conf.fields.output_normal_value[2]], statstr[conf.fields.output_safe_value[2]], conf.fields.output_initial_on_delay[2], conf.fields.output_initial_off_delay[2]);
		fprintf(stderr, "                                       | 3.3V1       | %s      | %s    | %5hu s |  %5hu s |\n\r", statstr[conf.fields.output_normal_value[3]], statstr[conf.fields.output_safe_value[3]], conf.fields.output_initial_on_delay[3], conf.fields.output_initial_off_delay[3]);
		fprintf(stderr, "________________________________       | 3.3V2       | %s      | %s    | %5hu s |  %5hu s |\n\r", statstr[conf.fields.output_normal_value[4]], statstr[conf.fields.output_safe_value[4]], conf.fields.output_initial_on_delay[4], conf.fields.output_initial_off_delay[4]);
		fprintf(stderr, "|                              |       | 3.3V3       | %s      | %s    | %5hu s |  %5hu s |\n\r", statstr[conf.fields.output_normal_value[5]], statstr[conf.fields.output_safe_value[5]], conf.fields.output_initial_on_delay[5], conf.fields.output_initial_off_delay[5]);
		fprintf(stderr, "|       FIXED PPT POINTS       |       | BP4 Heater  | %s      | %s    | %5hu s |  %5hu s |\n\r", statstr[conf.fields.output_normal_value[6]], statstr[conf.fields.output_safe_value[6]], conf.fields.output_initial_on_delay[6], conf.fields.output_initial_off_delay[6]);
		fprintf(stderr, "|______________________________|       | BP4 Switch  | %s      | %s    | %5hu s |  %5hu s |\n\r", statstr[conf.fields.output_normal_value[7]], statstr[conf.fields.output_safe_value[7]], conf.fields.output_initial_on_delay[7], conf.fields.output_initial_off_delay[7]);
		fprintf(stderr, "|                   |          |       |_____________|__________|________|_________|__________|\n\r");
		fprintf(stderr, "| Boost Converter 1 | %5hu mV |\n\r", conf.fields.vboost[0]);
		fprintf(stderr, "| Boost Converter 2 | %5hu mV |\n\r", conf.fields.vboost[1]);
		fprintf(stderr, "| Boost Converter 3 | %5hu mV |\n\r", conf.fields.vboost[2]);
		fprintf(stderr, "|___________________|__________|\n\r");
	}

	return 0;
}

/**
 * Sets the configuration of the EPS.
 */
static int eps_conf_set(const char *fname)
{
	int i;
	int ret;
	int len;
	int value;
	int confirmed;
	char buf[10];
	eps_config_t conf;

	fprintf(stderr, "Power point mode [auto/fixed]: ");
	len = serial_readline(buf, sizeof(buf));
	if (len < 1) {
		fprintf(stderr, "%s config set: Error reading input.\n\r", fname);
		return 1;
	}
	if (strcasecmp(buf, "auto") == 0) {
		conf.fields.ppt_mode = 1;
	} else if (strcasecmp(buf, "fixed") == 0) {
		conf.fields.ppt_mode = 2;
	} else {
		fprintf(stderr, "%s config set: Unknown power point mode \'%s\'.\n\r", fname, buf);
		return 1;
	}

	fprintf(stderr, "Battery heater mode [manual/auto]: ");
	len = serial_readline(buf, sizeof(buf));
	if (len < 1) {
		fprintf(stderr, "%s config set: Error reading input.\n\r", fname);
		return 1;
	}
	if (strcasecmp(buf, "manual") == 0) {
		conf.fields.ppt_mode = 0;
	} else if (strcasecmp(buf, "auto") == 0) {
		conf.fields.ppt_mode = 1;
	} else {
		fprintf(stderr, "%s config set: Unknown power point mode \'%s\'.\n\r", fname, buf);
		return 1;
	}

	fprintf(stderr, "Heater lower threshold (turn on at degC): ");
	ret = serial_readint(&value);
	if (ret != 0) {
		fprintf(stderr, "%s config set: Error reading integer.\n\r", fname);
		return 1;
	}
	if ((value < (-128)) || (value > 127)) {
		fprintf(stderr, "%s config set: Heater threshold \'%d\' cannot fit into a single byte.\n\r", fname, value);
		return 1;
	}
	conf.fields.battheater_low = (char) value;

	fprintf(stderr, "Heater upper threshold (turn off at degC): ");
	ret = serial_readint(&value);
	if (ret != 0) {
		fprintf(stderr, "%s config set: Error reading integer.\n\r", fname);
		return 1;
	}
	if ((value < (-128)) || (value > 127)) {
		fprintf(stderr, "%s config set: Heater threshold \'%d\' cannot fit into a single byte.\n\r", fname, value);
		return 1;
	}
	conf.fields.battheater_high = (char) value;

	for (i = 0; i < 8; i++) {
		fprintf(stderr, "Normal/nominal mode value of output %d: ", i + 1);
		ret = serial_readint(&value);
		if (ret != 0) {
			fprintf(stderr, "%s config set: Error reading integer.\n\r", fname);
			return 1;
		}
		if ((value < 0) || (value > 0xFF)) {
			fprintf(stderr, "%s config set: Value \'%d\' cannot fit into a single byte.\n\r", fname, value);
			return 1;
		}
		conf.fields.output_normal_value[i] = (unsigned char) value;
	}

	for (i = 0; i < 8; i++) {
		fprintf(stderr, "Safe mode value of output %d: ", i + 1);
		ret = serial_readint(&value);
		if (ret != 0) {
			fprintf(stderr, "%s config set: Error reading integer.\n\r", fname);
			return 1;
		}
		if ((value < 0) || (value > 0xFF)) {
			fprintf(stderr, "%s config set: Value \'%d\' cannot fit into a single byte.\n\r", fname, value);
			return 1;
		}
		conf.fields.output_safe_value[i] = (unsigned char) value;
	}

	for (i = 0; i < 8; i++) {
		fprintf(stderr, "Turn-on delay of output %d: ", i + 1);
		ret = serial_readint(&value);
		if (ret != 0) {
			fprintf(stderr, "%s config set: Error reading integer.\n\r", fname);
			return 1;
		}
		if ((value < 0) || (value > 0xFFFF)) {
			fprintf(stderr, "%s config set: \'%d\' cannot fit into a 16-bit unsigned integer.\n\r", fname, value);
			return 1;
		}
		conf.fields.output_initial_on_delay[i] = (unsigned short) value;
	}

	for (i = 0; i < 8; i++) {
		fprintf(stderr, "Turn-off delay of output %d: ", i + 1);
		ret = serial_readint(&value);
		if (ret != 0) {
			fprintf(stderr, "%s config set: Error reading integer.\n\r", fname);
			return 1;
		}
		if ((value < 0) || (value > 0xFFFF)) {
			fprintf(stderr, "%s config set: \'%d\' cannot fit into a 16-bit unsigned integer.\n\r", fname, value);
			return 1;
		}
		conf.fields.output_initial_off_delay[i] = (unsigned short) value;
	}

	for (i = 0; i < 3; i++) {
		fprintf(stderr, "Fixed PPT point of boost converter %d: ", i + 1);
		ret = serial_readint(&value);
		if (ret != 0) {
			fprintf(stderr, "%s config set: Error reading integer.\n\r", fname);
			return 1;
		}
		if ((value < 0) || (value > 0xFFFF)) {
			fprintf(stderr, "%s config set: \'%d\' cannot fit into a 16-bit unsigned integer.\n\r", fname, value);
			return 1;
		}
		conf.fields.vboost[i] = (unsigned short) value;
	}

	confirmed = serial_promptconfirm("Are these settings OK?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s config set: Configuration was not set.\n\r", fname);
		return 0;
	}

	ret = GomEpsConfigSet(0, &conf);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s config get: GomEpsConfigSet returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Send a configuration2 command to the EPS
 */
static int eps_conf2_cmd(const char *fname, unsigned char cmd)
{
	int ret;

	ret = GomEpsConfig2CMD(0, cmd);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s config2 cmd: GomEpsConfig2CMD returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Retrieve and print current configuration2 for the EPS.
 */
static int eps_conf2_get(const char *fname, int fmt_raw)
{
	int i;
	int ret;
	eps_config2_t conf;

	ret = GomEpsConfig2Get(0, &conf);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s config2 get: GomEpsConfig2Get returned %d\n\r", fname, ret);
		return 1;
	}

	if (fmt_raw) {
		fprintf(stderr, "Current GOMspace EPS Configuration2 (raw format):\n\r");
		fprintf(stderr, " - commandReply:         %hu\n\r", conf.fields.commandReply);
		fprintf(stderr, " - batt_maxvoltage:      %hu\n\r", conf.fields.batt_maxvoltage);
		fprintf(stderr, " - batt_safevoltage:     %hu\n\r", conf.fields.batt_safevoltage);
		fprintf(stderr, " - batt_criticalvoltage: %hu\n\r", conf.fields.batt_criticalvoltage);
		fprintf(stderr, " - batt_normalvoltage:   %hu\n\r", conf.fields.batt_normalvoltage);
		for (i = 0; i < 2; i++) {
			fprintf(stderr, " - reserved1[%d]:         %u\n\r", i, conf.fields.reserved1[i]);
		}
		for (i = 0; i < 4; i++) {
			fprintf(stderr, " - reserved2[%d]:         %u\n\r", i, conf.fields.reserved2[i]);
		}
	} else {
		fprintf(stderr, "[EPS Config2]___________\n\r");
		fprintf(stderr, "|          |           |\n\r");
		fprintf(stderr, "|   MODE   |  VOLTAGE  |\n\r");
		fprintf(stderr, "|__________|___________|\n\r");
		fprintf(stderr, "|          |           |\n\r");
		fprintf(stderr, "| Normal   | %6hu mV |\n\r", conf.fields.batt_normalvoltage);
		fprintf(stderr, "| Safe     | %6hu mV |\n\r", conf.fields.batt_safevoltage);
		fprintf(stderr, "| Critical | %6hu mV |\n\r", conf.fields.batt_criticalvoltage);
		fprintf(stderr, "|__________|___________|\n\r");
		fprintf(stderr, "|          |           |\n\r");
		fprintf(stderr, "| Maximum  | %6hu mV |\n\r", conf.fields.batt_maxvoltage);
		fprintf(stderr, "|__________|___________|\n\r");
	}

	return 0;
}

/**
 * Sets the configuration2 of the EPS.
 */
static int eps_conf2_set(const char *fname)
{
	int ret;
	int value;
	int confirmed;
	eps_config2_t conf;

	fprintf(stderr, "Maximum battery voltage [mV]: ");
	ret = serial_readint(&value);
	if (ret != 0) {
		fprintf(stderr, "%s config2 set: Error reading integer.\n\r", fname);
		return 1;
	}
	if ((value < 0) || (value > 0xFFFF)) {
		fprintf(stderr, "%s config2 set: \'%d\' cannot fit into a 16-bit unsigned integer.\n\r", fname, value);
		return 1;
	}
	conf.fields.batt_maxvoltage = (unsigned short) value;

	fprintf(stderr, "Battery voltage for safe mode [mV]: ");
	ret = serial_readint(&value);
	if (ret != 0) {
		fprintf(stderr, "%s config2 set: Error reading integer.\n\r", fname);
		return 1;
	}
	if ((value < 0) || (value > 0xFFFF)) {
		fprintf(stderr, "%s config2 set: \'%d\' cannot fit into a 16-bit unsigned integer.\n\r", fname, value);
		return 1;
	}
	conf.fields.batt_safevoltage = (unsigned short) value;

	fprintf(stderr, "Battery voltage for critical mode [mV]: ");
	ret = serial_readint(&value);
	if (ret != 0) {
		fprintf(stderr, "%s config2 set: Error reading integer.\n\r", fname);
		return 1;
	}
	if ((value < 0) || (value > 0xFFFF)) {
		fprintf(stderr, "%s config2 set: \'%d\' cannot fit into a 16-bit unsigned integer.\n\r", fname, value);
		return 1;
	}
	conf.fields.batt_criticalvoltage = (unsigned short) value;

	fprintf(stderr, "Battery voltage for normal mode [mV]: ");
	ret = serial_readint(&value);
	if (ret != 0) {
		fprintf(stderr, "%s config2 set: Error reading integer.\n\r", fname);
		return 1;
	}
	if ((value < 0) || (value > 0xFFFF)) {
		fprintf(stderr, "%s config2 set: \'%d\' cannot fit into a 16-bit unsigned integer.\n\r", fname, value);
		return 1;
	}
	conf.fields.batt_normalvoltage = (unsigned short) value;

	confirmed = serial_promptconfirm("Are these settings OK?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s config2 set: Configuration was not set.\n\r", fname);
		return 0;
	}

	ret = GomEpsConfig2Set(0, &conf);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s config2 get: GomEpsConfig2Set returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

static void eps_printfmthk_vi(const uint16_t *vboost,
                              uint16_t vbatt,
                              const uint16_t *curin,
                              uint16_t cursun,
                              uint16_t cursys)
{
	int i;

	fprintf(stderr, "[EPS Voltage & Current Housekeeping]___    _______________________\n\r");
	fprintf(stderr, "|       |         |         |         |    |                     |\n\r");
	fprintf(stderr, "| INPUT | VOLTAGE | CURRENT |  POWER  |    |       BATTERY       |\n\r");
	fprintf(stderr, "|_______|_________|_________|_________|    |_____________________|\n\r");
	fprintf(stderr, "|       |         |         |         |    |         |           |\n\r");
  //fprintf(stderr, "| PV1   |  375 mV |    0 mA |    0 mW |    | Voltage |   7796 mV |\n\r");
  //fprintf(stderr, "| PV2   | 3662 mV |  357 mA | 1307 mW |    | Input   |    173 mA |\n\r");
  //fprintf(stderr, "| PV2   | 3662 mV |   41 mA |  150 mW |    | Output  |    772 mA |\n\r");
  //fprintf(stderr, "|_______|_________|_________|_________|    |_________|___________|\n\r");

	for (i = 0; i < 3; i++) {
		uint32_t volt = vboost[i];
		uint32_t cur = curin[i];
		uint32_t power = (volt * cur) / 1000;
		fprintf(stderr, "| PV%d   |%5lu mV |%5lu mA |%5lu mW |", i, volt, cur, power);

		switch (i) {
		case 0:
			fprintf(stderr, "    | Voltage |%7hu mV |\n\r", vbatt);
			break;
		case 1:
			fprintf(stderr, "    | Input   |%7hu mA |\n\r", cursun);
			break;
		case 2:
			fprintf(stderr, "    | Output  |%7hu mA |\n\r", cursys);
			break;
		default:
			break;
		}
	}

	fprintf(stderr, "|_______|_________|_________|_________|    |_________|___________|\n\r");
}

static void eps_printfmthk_out(const uint16_t *curout,
                               const uint8_t *output,
                               const uint16_t *output_on_delta,
                               const uint16_t *output_off_delta,
                               const uint16_t *latchup)
{
	static const char *names[] = {"5V1", "5V2", "5V3", "3.3V1", "3.3V2", "3.3V3", "BP4 Heater", "BP4 Switch"};

	int i;

	fprintf(stderr, "[EPS Output Housekeeping]____________________________________________________\n\r");
	fprintf(stderr, "|            |        |         |          |               |                |\n\r");
	fprintf(stderr, "|   OUTPUT   | STATUS | CURRENT | LATCHUPS | TIME UNTIL ON | TIME UNTIL OFF |\n\r");
	fprintf(stderr, "|____________|________|_________|__________|_______________|________________|\n\r");
	fprintf(stderr, "|            |        |         |          |               |                |\n\r");
  //fprintf(stderr, "| 5V1        | On     |   38 mA |        1 |           0 s |            0 s |\n\r");
  //fprintf(stderr, "| 5V2        | Off    |    0 mA |        0 |           0 s |            0 s |\n\r");
  //fprintf(stderr, "| 5V3        | Off    |    0 mA |        0 |           0 s |            0 s |\n\r");
  //fprintf(stderr, "| 3.3V1      | Off    |    0 mA |        0 |           0 s |            0 s |\n\r");
  //fprintf(stderr, "| 3.3V2      | Off    |    0 mA |        0 |           0 s |            0 s |\n\r");
  //fprintf(stderr, "| 3.3V3      | Off    |    0 mA |        0 |           0 s |            0 s |\n\r");
  //fprintf(stderr, "| BP4 Heater | Off    |         |          |           0 s |            0 s |\n\r");
  //fprintf(stderr, "| BP4 Switch | Off    |         |          |           0 s |            0 s |\n\r");
  //fprintf(stderr, "|____________|________|_________|__________|_______________|________________|\n\r");

	for (i = 0; i < 8; i++) {
		int namelen = (int) strlen(names[i]);
		fprintf(stderr, "| %s%*s |", names[i], 10 - namelen, "");

		if (output[i])
			fprintf(stderr, " On     |");
		else
			fprintf(stderr, " Off    |");

		if (i < 6) {
			fprintf(stderr, "%5hu mA |", curout[i]);
			fprintf(stderr, "%9hu |", latchup[i]);
		} else {
			fprintf(stderr, "         |");
			fprintf(stderr, "          |");
		}

		fprintf(stderr, "%12hu s |%13hu s |\n\r", output_on_delta[i], output_off_delta[i]);
	}

	fprintf(stderr, "|____________|________|_________|__________|_______________|________________|\n\r");
}

static void eps_printfmthk_wdt(uint32_t wdt_i2c_time_left,
                               uint32_t wdt_gnd_time_left,
                               const uint8_t *wdt_csp_pings_left,
                               uint32_t counter_wdt_i2c,
                               uint32_t counter_wdt_gnd,
                               const uint32_t *counter_wdt_csp)
{
	fprintf(stderr, "[EPS Watchdog Timer Housekeeping]____\n\r");
	fprintf(stderr, "|       |            |              |\n\r");
	fprintf(stderr, "|  WDT  |    LEFT    | REBOOT COUNT |\n\r");
	fprintf(stderr, "|_______|____________|______________|\n\r");
	fprintf(stderr, "|       |            |              |\n\r");
	fprintf(stderr, "| I2C   |%9lu s |%13lu |\n\r", wdt_i2c_time_left, counter_wdt_i2c);
	fprintf(stderr, "| GND   |%9lu s |%13lu |\n\r", wdt_gnd_time_left, counter_wdt_gnd);
	fprintf(stderr, "| CSP0  |%5u pings |%13lu |\n\r", wdt_csp_pings_left[0], counter_wdt_csp[0]);
	fprintf(stderr, "| CSP1  |%5u pings |%13lu |\n\r", wdt_csp_pings_left[1], counter_wdt_csp[1]);
	fprintf(stderr, "|_______|____________|______________|\n\r");
}


static void eps_printfmthk_basic(uint32_t counter_boot,
                                 const int16_t *temp,
                                 uint8_t bootcause,
                                 uint8_t battmode,
                                 uint8_t pptmode)
{
	static const char *bootcauses[] = {"Unknown reset",
	                                   "Dedicated WDT reset",
	                                   "I2C WDT reset",
	                                   "Hard reset",
	                                   "Soft reset",
	                                   "Stack overflow",
	                                   "Timer overflow",
	                                   "Brownout or power-on reset",
	                                   "Internal WDT reset"};

	static const char *battmodes[] = {"Initial", "Undervoltage", "Safemode", "Nominal", "Full", "Unknown"};

	static const char *pptmodes[] = {"Unknown", "Maximum PPT", "Fixed Software PPT"};

	if (bootcause > 8)
		bootcause = 0;

	if (battmode > 4)
		battmode = 5;

	if (pptmode > 2)
		pptmode = 0;

	int bc_padding = 26 - ((int) strlen(bootcauses[bootcause]));
	int bm_padding = 26 - ((int) strlen(battmodes[battmode]));
	int ppt_padding = 26 - ((int) strlen(pptmodes[pptmode]));

	fprintf(stderr, "[EPS Basic Housekeeping]__    _____________________________________________\n\r");
	fprintf(stderr, "|                        |    |              |                            |\n\r");
	fprintf(stderr, "|      TEMPERATURES      |    | Boot Counter | %26lu |\n\r", counter_boot);
	fprintf(stderr, "|________________________|    | Boot Cause   | %26s |\n\r", bootcauses[bootcause]);
	fprintf(stderr, "|       |                |    | Battery Mode | %26s |\n\r", battmodes[battmode]);
	fprintf(stderr, "| TEMP1 |%10hd degC |    | PPT Mode     | %26s |\n\r", temp[0], pptmodes[pptmode]);
	fprintf(stderr, "| TEMP2 |%10hd degC |    |______________|____________________________|\n\r", temp[1]);
	fprintf(stderr, "| TEMP3 |%10hd degC |\n\r", temp[2]);
	fprintf(stderr, "| TEMP4 |%10hd degC |\n\r", temp[3]);
	fprintf(stderr, "| BATT0 |%10hd degC |\n\r", temp[4]);
	fprintf(stderr, "| BATT1 |%10hd degC |\n\r", temp[5]);
	fprintf(stderr, "|_______|________________|\n\r");
}
