/**
 * @file   hexdump.c
 * @author John Wikman
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>

#define BYTES_PER_ROW (16)

struct display_options {
	int show_grid;
};

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int hexdump_dump(const char *fname, F_FILE *file, int length, int offset, long filelen, struct display_options *dopts);

int hexdump_main(int argc, char **argv)
{
	int ret;
	int ret2;
	int choice;
	long filelen;
	int length = -1;
	int offset = 0;
	const char *filename = NULL;
	F_FILE *file = NULL;
	struct display_options dopts;
	dopts.show_grid = 0;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	while ((choice = getopt(argc, argv, "-hgn:s:")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			if (filename == NULL) {
				filename = optarg;
			} else {
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'g':
			dopts.show_grid = 1;
			break;
		case 'n':
			ret = arghandler_to_int(optarg, &length);
			if ((ret != 0) || (length < 1)) {
				fprintf(stderr, "%s: length must be a positive integer\n\r", argv[0]);
				return 1;
			}
			break;
		case 's':
			ret = arghandler_to_int(optarg, &offset);
			if ((ret != 0) || (offset < 0)) {
				fprintf(stderr, "%s: offset/skip must be a non-negative integer\n\r", argv[0]);
				return 1;
			}
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	if (filename == NULL) {
		fprintf(stderr, "%s: must specify a filename\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	file = f_open(filename, "r");
	if (!file) {
		fprintf(stderr, "%s: could not open \'%s\'\n\r", argv[0], filename);
		return 1;
	}

	filelen = f_filelength(filename);
	if (filelen < 0) {
		fprintf(stderr, "%s: f_filelength returned %ld\n", argv[0], filelen);
		f_close(file);
		return 1;
	}

	ret = 0;
	if (filelen > 0) {
		ret = hexdump_dump(argv[0], file, length, offset, filelen, &dopts);
	}

	ret2 = f_close(file);
	if (ret2 != F_NO_ERROR) {
		DEBUGLN_ARGS("f_close returned %d", ret2);
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Dumps the contents of a file to output.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s [-hg] [-n len] [-s skip] <file>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -g: Display as a grid.\n\r");
	fprintf(stderr, "    -n: How many bytes to dump from file.\n\r");
	fprintf(stderr, "    -s: Start offset/skip in file.\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Dump the opened file to standard output.
 */
static int hexdump_dump(const char *fname, F_FILE *file, int length, int offset, long filelen, struct display_options *dopts)
{
	int i;
	int ret;
	int alignval;
	int pos;
	int endpos;
	int rowidx;

	if ((offset < 0) || (offset >= filelen)) {
		fprintf(stderr, "%s: offset is out-of-bounds from file\n\r", fname);
		return 1;
	}

	// non-positive len indicates that entire file should be read
	if (length < 1)
		length = (int) filelen;

	endpos = offset + length;
	if ((endpos < 0) || (endpos > filelen)) { // < 0 if overflow
		length = filelen - offset;
		endpos = filelen;
	}

	if (offset != 0) {
		ret = f_seek(file, offset, F_SEEK_SET);
		if (ret != F_NO_ERROR) {
			fprintf(stderr, "%s: f_seek returned %d\n\r", fname, ret);
			return 1;
		}	
	}

	pos = offset;
	rowidx = 0;

	if (dopts->show_grid) {
		fprintf(stderr, "        |  0 1 |  2 3 |  4 5 |  6 7 |  8 9 |  a b |  c d |  e f\n\r");
		fprintf(stderr, "--------|------|------|------|------|------|------|------|-----\n\r");
		// Print the first row and any empty spaces in case position is not aligned
		alignval = (pos / BYTES_PER_ROW) * BYTES_PER_ROW;
	} else {
		alignval = pos;
	}
	fprintf(stderr, "%07x", alignval);

	if (pos != alignval) {
		for (i = alignval; i < pos; i++) {
			if ((rowidx & 0x01) == 0) { // print a space before every even position
				if (dopts->show_grid)
					fprintf(stderr, " | ");
				else
					fprintf(stderr, " ");
			}
			fprintf(stderr, "  ");

			rowidx++;
		}
	}
	for (i = pos; (i < (alignval + BYTES_PER_ROW)) && (i < endpos); i++) {
		int ch = f_getc(file);
		if (ch == -1) {
			fprintf(stderr, "%s: error reading from file\n\r", fname);
			return 1;
		}

		if ((rowidx & 0x01) == 0) {
			if (dopts->show_grid)
				fprintf(stderr, " | ");
			else
				fprintf(stderr, " ");
		}
		fprintf(stderr, "%02x", ch);

		rowidx++;
	}
	fprintf(stderr, "\n\r");

	// Now print remaining rows
	alignval += BYTES_PER_ROW;
	while (alignval < endpos) {
		int rowidx = 0;

		fprintf(stderr, "%07x", alignval);
		for (i = alignval; (i < (alignval + BYTES_PER_ROW)) && (i < endpos); i++) {
			int ch = f_getc(file);
			if (ch == -1) {
				fprintf(stderr, "%s: error reading from file\n\r", fname);
				return 1;
			}

			if ((rowidx & 0x01) == 0) {
				if (dopts->show_grid)
					fprintf(stderr, " | ");
				else
					fprintf(stderr, " ");
			}
			fprintf(stderr, "%02x", ch);

			rowidx++;
		}

		// if we are on the last row and printing a grid, make sure to print the grid lines!
		if (dopts->show_grid && (i >= endpos)) {
			for (; i < (alignval + BYTES_PER_ROW); i++) {
				if ((rowidx & 0x01) == 0)
					fprintf(stderr, " | ");

				fprintf(stderr, "  ");
			}

			rowidx++;
		}

		fprintf(stderr, "\n\r");

		alignval += BYTES_PER_ROW;
	}

	return 0;
}
