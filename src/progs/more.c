/**
 * @file   more.c
 * @author John Wikman
 *
 * Output file line by line.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>
#include <mist-tools/serial_in.h>

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int more_display(F_FILE *file);

int more_main(int argc, char **argv)
{
	int ret, ret2;
	int choice;
	F_FILE *file;
	const char *filename = NULL;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	filename = NULL;

	while ((choice = getopt(argc, argv, "-h")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			if (filename == NULL) {
				filename = optarg;
			} else {
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	if (filename == NULL) {
		fprintf(stderr, "%s: must specify a filename\n\r", argv[0]);
		return 1;
	}

	file = f_open(filename, "r");
	if (!file) {
		fprintf(stderr, "%s: could not open \'%s\'\n\r", argv[0], filename);
		return 1;
	}

	ret = more_display(file);

	ret2 = f_close(file);
	if (ret2 != F_NO_ERROR) {
		DEBUGLN_ARGS("f_close returned %d", ret2);
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Print the content of a textfile line by line.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s [-h] <file>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "\n\r");
}

static int more_display(F_FILE *file)
{
	int lines = 0;
	int has_more_data = 1;
	/* First read 20 lines and then wait for an enter press or a Q */
	while (lines < 20 && has_more_data) {
		int ch = f_getc(file);
		if (ch == -1) {
			has_more_data = 0;
			break;
		}

		fprintf(stderr, "%c", ch);
		if (ch == '\n') {
			fprintf(stderr, "\r");
			lines++;
		}
	}

	/* Print out remaining lines on manual interraction */
	int last_char;
	while (has_more_data) {
		// Now we are at a new line, so first we print a CR and then the line number
		fprintf(stderr, "\rLINE 1-%d", lines);

		int valid_decision = 0;
		char decision;
		while (!valid_decision) {
			decision = serial_getkeypress();
			switch (decision) {
			case 'q':
			case '\r':
				valid_decision = 1;
				break;
			default:
				valid_decision = 0;
				break;
			}
		}
		/* Remove the LINE 1-%d info. */
		fprintf(stderr, "\r               \r");

		/* q == quit */
		if (decision == 'q') {
			has_more_data = 0;
			break;
		}

		/* print one more line */
		if (decision == '\r') {
			int found_newline = 0;
			while (!found_newline && has_more_data) {
				int ch = f_getc(file);
				if (ch == -1) {
					has_more_data = 0;
					if (last_char != '\n')
						fprintf(stderr, "\n\r");
					break;
				}

				fprintf(stderr, "%c", ch);
				if (ch == '\n') {
					fprintf(stderr, "\r");
					found_newline = 1;
					lines++;
				}
				last_char = ch;
			}
		}
	}

	return 0;
}