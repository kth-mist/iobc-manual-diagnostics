/**
 * @file   tmq.c
 * @author John Wikman
 */

#include <ctype.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hal/Timing/Time.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>

#include <obs-api/tmqueue.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>
#include <mist-tools/serial_in.h>

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static const char *tmq_strerror(int err);

static int tmq_clear(const char *fname, int queue_id, int ignore_confirmation);
static int tmq_check_status(const char *fname, int queue_id);
static int tmq_print_length(const char *fname, int queue_id);
static int tmq_insert(const char *fname, int queue_id, struct tmqueue_entry *in);
static int tmq_poll(const char *fname, int queue_id, int metafmt, int datafmt);

#define TMQ_MODE_NONE          (-1)
#define TMQ_MODE_CLEAR         (0x00)
#define TMQ_MODE_CHECK         (0x01)
#define TMQ_MODE_LENGTH        (0x02)
#define TMQ_MODE_INSERT_STRING (0x10)
#define TMQ_MODE_INSERT_BYTES  (0x11)
#define TMQ_MODE_POLL          (0x20)

#define TMQ_METAFMT_DECIMAL     (0)
#define TMQ_METAFMT_HEXADECIMAL (1)

#define TMQ_DATAFMT_ASCII   (0)
#define TMQ_DATAFMT_BYTES   (1)
#define TMQ_DATAFMT_HEXDUMP (2)

int tmq_main(int argc, char **argv)
{
	int ret;
	int conv;
	size_t len, i;
	int choice;
	int mode = TMQ_MODE_NONE;
	int has_set_queue_id = 0;
	int ignore_confirmation = 0;
	int queue_id = -1;
	int metafmt = TMQ_METAFMT_DECIMAL;
	int datafmt = TMQ_DATAFMT_ASCII;
	unsigned int unixepoch;
	struct tmqueue_entry tmqe;
	const char *textarg = NULL;
	char curdir[F_MAXPATHNAME + 1];

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan and set the mode variable
	if (strcmp(argv[1], "clear") == 0) { /* CLEAR MODE */
		mode = TMQ_MODE_CLEAR;
		optind = 2;
	} else if (strcmp(argv[1], "check") == 0) { /* CHECK MODE */
		mode = TMQ_MODE_CHECK;
		optind = 2;
	} else if (strcmp(argv[1], "length") == 0) { /* LENGTH MODE */
		mode = TMQ_MODE_LENGTH;
		optind = 2;
	} else if (strcmp(argv[1], "clr") == 0) { /* ALIASES */
		mode = TMQ_MODE_CLEAR;
		optind = 2;
	} else if (strcmp(argv[1], "chk") == 0) {
		mode = TMQ_MODE_CHECK;
		optind = 2;
	} else if ((strcmp(argv[1], "l") == 0) || (strcmp(argv[1], "len") == 0)) {
		mode = TMQ_MODE_LENGTH;
		optind = 2;
	} else if (strcmp(argv[1], "insert") == 0) { /* INSERT MODE */
		if (argc < 3) {
			fprintf(stderr, "%s insert: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "string") == 0) {
			mode = TMQ_MODE_INSERT_STRING;
			optind = 3;
		} else if (strcmp(argv[2], "bytes") == 0) {
			mode = TMQ_MODE_INSERT_BYTES;
			optind = 3;
		} else {
			fprintf(stderr, "%s insert: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "is") == 0) { /* INSERT ALIASES */
		mode = TMQ_MODE_INSERT_STRING;
		optind = 2;
	} else if (strcmp(argv[1], "ib") == 0) {
		mode = TMQ_MODE_INSERT_BYTES;
		optind = 2;
	} else if (strcmp(argv[1], "poll") == 0) { /* POLL MODE */
		mode = TMQ_MODE_POLL;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	ret = Time_getUnixEpoch(&unixepoch);
	if (ret != 0) {
		DEBUGLN_ARGS("Time_getUnixEpoch returned %d", ret);
		unixepoch = 0;
	}

	has_set_queue_id = 0;
	ignore_confirmation = 0;
	queue_id = -1;
	tmqe.timestamp = (uint32_t) unixepoch;
	tmqe.apid = 101;
	tmqe.service_type = 37;
	tmqe.service_subtype = 73;
	tmqe.datalen = 0;
	textarg = NULL;

	while ((choice = getopt(argc, argv, "-hft:I:T:S:dxBAX")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			if (!has_set_queue_id) {
				ret = arghandler_to_int(optarg, &queue_id);
				if (ret != 0) {
					fprintf(stderr, "%s: queue_id must be an integer\n\r", argv[0]);
					return 1;
				}
				if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)) {
					fprintf(stderr, "%s: queue_id must be between %d and %d\n\r", argv[0], TMQUEUE_MIN_ID, TMQUEUE_MAX_ID);
					return 1;
				}
				has_set_queue_id = 1;
			} else {
				switch (mode) {
				case TMQ_MODE_INSERT_STRING:
					if (textarg == NULL) {
						textarg = optarg;
					} else {
						fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
						return 1;
					}
					break;
				case TMQ_MODE_INSERT_BYTES:
					if (tmqe.datalen < TMQUEUE_ENTRY_DATA_MAXSIZE) {
						ret = arghandler_to_int(optarg, &conv);
						if (ret != 0) {
							fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
							return 1;
						}
						if ((conv < 0) || (conv > 0xFF)) {
							fprintf(stderr, "%s: \'%s\' is too large to fit in a byte\n\r", argv[0], optarg);
							return 1;
						}
						tmqe.data[tmqe.datalen++] = (uint8_t) conv;
					} else {
						fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
						return 1;
					}
					break;
				default:
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'f':
			ignore_confirmation = 1;
			break;
		case 't':
			ret = arghandler_to_int(optarg, &conv);
			if (ret != 0) {
				fprintf(stderr, "%s: timestamp must be a number\n\r", argv[0]);
				return 1;
			}
			tmqe.timestamp = (uint32_t) conv;
			break;
		case 'I':
			ret = arghandler_to_int(optarg, &conv);
			if (ret != 0 || (conv < 0) || (conv > 2047)) {
				fprintf(stderr, "%s: APID must be a number between 0 and 2047\n\r", argv[0]);
				return 1;
			}
			tmqe.apid = (uint16_t) conv;
			break;
		case 'T':
			ret = arghandler_to_int(optarg, &conv);
			if (ret != 0 || (conv < 0) || (conv > 255)) {
				fprintf(stderr, "%s: service type must be a number between 0 and 255\n\r", argv[0]);
				return 1;
			}
			tmqe.service_type = (uint8_t) conv;
			break;
		case 'S':
			ret = arghandler_to_int(optarg, &conv);
			if (ret != 0 || (conv < 0) || (conv > 255)) {
				fprintf(stderr, "%s: service subtype must be a number between 0 and 255\n\r", argv[0]);
				return 1;
			}
			tmqe.service_subtype = (uint8_t) conv;
			break;
		case 'd':
			metafmt = TMQ_METAFMT_DECIMAL;
			break;
		case 'x':
			metafmt = TMQ_METAFMT_HEXADECIMAL;
			break;
		case 'B':
			datafmt = TMQ_DATAFMT_BYTES;
			break;
		case 'A':
			datafmt = TMQ_DATAFMT_ASCII;
			break;
		case 'X':
			datafmt = TMQ_DATAFMT_HEXDUMP;
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check that necessary values have been set
	if (!has_set_queue_id) {
		fprintf(stderr, "%s: must specify queue_id\n\r", argv[0]);
		return 1;
	}
	switch (mode) {
	case TMQ_MODE_INSERT_STRING:
		if (textarg == NULL) {
			fprintf(stderr, "%s: must specify text value to insert\n\r", argv[0]);
			return 1;
		}
		len = strlen(textarg);
		if (len > TMQUEUE_ENTRY_DATA_MAXSIZE) {
			fprintf(stderr, "%s: inserted data cannot be larger than %d bytes\n\r", argv[0], TMQUEUE_ENTRY_DATA_MAXSIZE);
			return 1;
		}

		for (i = 0; i < len; i++)
			tmqe.data[i] = textarg[i];

		tmqe.datalen = (uint16_t) len;
		/* fall-through */
	default:
		break;
	}

	// Save current directory before invoking any tmqueue functions
	ret = f_getcwd(curdir, sizeof(curdir));
	if (ret != F_NO_ERROR) {
		DEBUGLN_ARGS("f_getcwd returned %d", ret);
		return 1;
	}
	curdir[sizeof(curdir) - 1] = 0;

	// Invoke corresponding functions
	ret = 1;
	switch (mode) {
	case TMQ_MODE_CLEAR:
		ret = tmq_clear(argv[0], queue_id, ignore_confirmation);
		break;
	case TMQ_MODE_CHECK:
		ret = tmq_check_status(argv[0], queue_id);
		break;
	case TMQ_MODE_LENGTH:
		ret = tmq_print_length(argv[0], queue_id);
		break;
	case TMQ_MODE_INSERT_STRING:
	case TMQ_MODE_INSERT_BYTES:
		ret = tmq_insert(argv[0], queue_id, &tmqe);
		break;
	case TMQ_MODE_POLL:
		ret = tmq_poll(argv[0], queue_id, metafmt, datafmt);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	// Go back to the directory we were in
	ret = f_chdir(curdir);
	if (ret != F_NO_ERROR)
		DEBUGLN_ARGS("f_chdir returned %d", ret);

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Telemetry queue functionality.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s clear [-hf] <queue_id>\n\r", fname);
	fprintf(stderr, "       %s check [-h] <queue_id>\n\r", fname);
	fprintf(stderr, "       %s length [-h] <queue_id>\n\r", fname);
	fprintf(stderr, "       %s insert string [-h] [-t time] [-A apid] [-T st] [-S sst] <queue_id> <text>\n\r", fname);
	fprintf(stderr, "       %s insert bytes [-h] [-t time] [-A apid] [-T st] [-S sst] <queue_id> <values...>\n\r", fname);
	fprintf(stderr, "       %s poll [-hdxBAX] <queue_id>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -f: Ignore confirmation prompt.\n\r");
	fprintf(stderr, "    -t: Manually specify timestamp for inserted entry.\n\r");
	fprintf(stderr, "    -I: APID for inserted entry (default: 101).\n\r");
	fprintf(stderr, "    -T: Service type for inserted entry (default: 37).\n\r");
	fprintf(stderr, "    -S: Service subtype for inserted entry (default: 73).\n\r");
	fprintf(stderr, "    -d: Format polled metadata in decimal (default).\n\r");
	fprintf(stderr, "    -x: Format polled metadata in hexadecimal.\n\r");
	fprintf(stderr, "    -B: Output polled data as a byte sequence.\n\r");
	fprintf(stderr, "    -A: Attempt to output polled data in ascii text (default).\n\r");
	fprintf(stderr, "    -X: Output polled data as a hexdump.\n\r");
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Mode aliases:\n\r");
	fprintf(stderr, "    chk:     check\n\r");
	fprintf(stderr, "    clr:     clear\n\r");
	fprintf(stderr, "    (l|len): length\n\r");
	fprintf(stderr, "    i(s|b):  insert (string|bytes)\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Returns an error string based on the entered error.
 */
static const char *tmq_strerror(int err)
{
	static char errbuf[64];
	static const char *errstr_queue_id_oob = "queue_id is out-of-bounds";
	static const char *errstr_null_argument = "NULL argument";
	static const char *errstr_metadata_read = "could not read metadata";
	static const char *errstr_metadata_write = "could not write metadata";
	static const char *errstr_dir_missing = "required directory is missing";
	static const char *errstr_is_empty = "specified queue is empty";
	static const char *errstr_entry_fram_read = "could not read entry from FRAM";
	static const char *errstr_entry_fram_write = "could not write entry from FRAM";
	static const char *errstr_crc_mismatch = "CRC mismatch";
	static const char *errstr_entry_fs_read = "could not read entry from file";
	static const char *errstr_entry_fs_write = "could not write entry from file";
	static const char *errstr_read_invalid_size = "read entry has invalid size";
	static const char *errstr_fs_could_not_del = "could not delete block from filesystem";
	static const char *errstr_dir_create_failed = "could not create necessary tmqueue directory";
	static const char *errstr_dir_clear_failed = "could not clear tmqueue directory";
	static const char *errstr_inconsistent_metadata = "tmqueue metadata is inconsistent";
	static const char *errstr_block_is_missing = "a block is missing from the filesystem";
	static const char *errstr_entry_invalid_size = "size of entry data is invalid";
	static const char *errstr_could_not_generate_crc = "could not generate CRC";

	errbuf[0] = '\0';
	const char *ret = errbuf;

	switch (err) {
	case TMQUEUE_ERR_QUEUE_ID_OOB:
		ret = errstr_queue_id_oob;
		break;
	case TMQUEUE_ERR_NULL_ARGUMENT:
		ret = errstr_null_argument;
		break;
	case TMQUEUE_ERR_METADATA_READ:
		ret = errstr_metadata_read;
		break;
	case TMQUEUE_ERR_METADATA_WRITE:
		ret = errstr_metadata_write;
		break;
	case TMQUEUE_ERR_DIR_MISSING:
		ret = errstr_dir_missing;
		break;
	case TMQUEUE_ERR_IS_EMPTY:
		ret = errstr_is_empty;
		break;
	case TMQUEUE_ERR_ENTRY_FRAM_READ:
		ret = errstr_entry_fram_read;
		break;
	case TMQUEUE_ERR_ENTRY_FRAM_WRITE:
		ret = errstr_entry_fram_write;
		break;
	case TMQUEUE_ERR_CRC_MISMATCH:
		ret = errstr_crc_mismatch;
		break;
	case TMQUEUE_ERR_ENTRY_FS_READ:
		ret = errstr_entry_fs_read;
		break;
	case TMQUEUE_ERR_ENTRY_FS_WRITE:
		ret = errstr_entry_fs_write;
		break;
	case TMQUEUE_ERR_READ_INVALID_SIZE:
		ret = errstr_read_invalid_size;
		break;
	case TMQUEUE_ERR_FS_COULD_NOT_DEL:
		ret = errstr_fs_could_not_del;
		break;
	case TMQUEUE_ERR_DIR_CREATE_FAILED:
		ret = errstr_dir_create_failed;
		break;
	case TMQUEUE_ERR_DIR_CLEAR_FAILED:
		ret = errstr_dir_clear_failed;
		break;
	case TMQUEUE_ERR_INCONSISTENT_METADATA:
		ret = errstr_inconsistent_metadata;
		break;
	case TMQUEUE_ERR_BLOCK_IS_MISSING:
		ret = errstr_block_is_missing;
		break;
	case TMQUEUE_ERR_ENTRY_INVALID_SIZE:
		ret = errstr_entry_invalid_size;
		break;
	case TMQUEUE_ERR_COULD_NOT_GENERATE_CRC:
		ret = errstr_could_not_generate_crc;
		break;
	default:
		snprintf(errbuf, sizeof(errbuf), "error code %d", err);
		ret = errbuf;
		break;
	}

	return ret;
}

/**
 * Clears the queue with the specified id.
 */
static int tmq_clear(const char *fname, int queue_id, int ignore_confirmation)
{
	int ret;
	int len;
	char yes_no_buf[2];

	if (!ignore_confirmation) {
		fprintf(stderr, "Are you sure you want to clear tmqueue %d? [y/N]\n\r", queue_id);

		len = serial_readline(yes_no_buf, sizeof(yes_no_buf));
		if ((len != 1) || (tolower((int) yes_no_buf[0]) != 'y')) {
			/* No explicit "yes" */
			fprintf(stderr, "tmqueue %d was not cleared.\n\r", queue_id);
			return 0;
		}
	}

	ret = tmqueue_clear(queue_id);
	if (ret != 0) {
		fprintf(stderr, "%s clear: error: %s\n\r", fname, tmq_strerror(ret));
		return 1;
	}

	return 0;
}

/**
 * Checks the status/health of the specified tmqueue
 */
static int tmq_check_status(const char *fname, int queue_id)
{
	int ret;

	ret = tmqueue_has_error(queue_id);
	if (ret != 0) {
		fprintf(stderr, "%s check: error: %s\n\r", fname, tmq_strerror(ret));
		return 1;
	} else {
		fprintf(stderr, "tmqueue %d is healthy\n\r", queue_id);
		return 0;
	}
}

/**
 * Prints the number of entries in the specified tmqueue
 */
static int tmq_print_length(const char *fname, int queue_id)
{
	uint32_t len;
	const char *singular = "y";
	const char *plural = "ies";

	(void) fname;

	len = tmqueue_length(queue_id);
	if (len == 0) {
		fprintf(stderr, "tmqueue %d is empty\n\r", queue_id);
	} else {
		const char *abbrev = plural;
		if (len == 1)
			abbrev = singular;

		fprintf(stderr, "tmqueue %d contains %lu entr%s\n\r", queue_id, len, abbrev);
	}

	return 0;
}

/**
 * Inserts an entry into the specified tmqueue.
 */
static int tmq_insert(const char *fname, int queue_id, struct tmqueue_entry *in)
{
	int ret;
	static uint8_t blockbuf[TMQUEUE_BLOCK_SIZE];

	ret = tmqueue_insert(queue_id, in, blockbuf);
	if (ret != 0) {
		fprintf(stderr, "%s insert: error: %s\n\r", fname, tmq_strerror(ret));
		return 1;
	}

	return 0;
}

/**
 * Polls an entry from the specified tmqueue.
 */
static int tmq_poll(const char *fname, int queue_id, int metafmt, int datafmt)
{
	int ret;
	uint16_t i;
	struct tmqueue_entry e;

	ret = tmqueue_poll(queue_id, &e);
	if (ret != 0) {
		fprintf(stderr, "%s poll: error: %s\n\r", fname, tmq_strerror(ret));
		return 1;
	}

	fprintf(stderr, "Metadata:\n\r");
	if (metafmt == TMQ_METAFMT_HEXADECIMAL) {
		fprintf(stderr, "             APID: 0x%X\n\r", e.apid);
		fprintf(stderr, "     service_type: 0x%X\n\r", e.service_type);
		fprintf(stderr, "  service_subtype: 0x%X\n\r", e.service_subtype);
		fprintf(stderr, "        timestamp: 0x%lX\n\r", e.timestamp);
		fprintf(stderr, "          datalen: 0x%X\n\r", e.datalen);
	} else {
		fprintf(stderr, "             APID: %hu\n\r", e.apid);
		fprintf(stderr, "     service_type: %u\n\r", e.service_type);
		fprintf(stderr, "  service_subtype: %u\n\r", e.service_subtype);
		fprintf(stderr, "        timestamp: %lu\n\r", e.timestamp);
		fprintf(stderr, "          datalen: %hu\n\r", e.datalen);
	}

	fprintf(stderr, "\n\r");

	if (datafmt == TMQ_DATAFMT_ASCII) {
		int is_ascii = 1;

		for (i = 0; i < e.datalen; i++) {
			if ((e.data[i] < 0x20) || (e.data[i] > 0x7E)) {
				is_ascii = 0;
				break;
			}
		}

		if (is_ascii) {
			fprintf(stderr, "Data (ASCII):\n\r");
			for (i = 0; i < e.datalen; i++)
				fprintf(stderr, "%c", (char) e.data[i]);
			fprintf(stderr, "\n\r");
		} else {
			// output as a byte sequence if ascii fails
			datafmt = TMQ_DATAFMT_BYTES;
		}
	}

	if (datafmt == TMQ_DATAFMT_BYTES) {
		fprintf(stderr, "Data:\n\r");

		for (i = 0; i < e.datalen; i++) {
			if (i > 0)
				fprintf(stderr, " ");
			fprintf(stderr, "0x%02X", (char) e.data[i]);
		}
		fprintf(stderr, "\n\r");
	} else if (datafmt == TMQ_DATAFMT_HEXDUMP) {
		uint16_t rowidx = 0;
		const uint16_t bytes_per_row = 16;

		fprintf(stderr, "Data:\n\r");

		for (i = 0; i < e.datalen; i++) {
			if ((i % bytes_per_row) == 0) {
				if (i != 0)
					fprintf(stderr, "\n\r");

				fprintf(stderr, "%07x", i);
				rowidx = 0;
			}

			if ((rowidx & 0x01) == 0)
				fprintf(stderr, " ");

			fprintf(stderr, "%02x", e.data[i]);
			rowidx++;
		}
		fprintf(stderr, "\n\r");
	}

	return 0;
}
