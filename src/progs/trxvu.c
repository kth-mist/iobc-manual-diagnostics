/**
 * @file   trxvu.c
 * @author John Wikman
 *
 * Access all the functionality of the ISIS TRXVU subsystem.
 */

#include <ctype.h>
#include <getopt.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hal/errors.h>
#include <satellite-subsystems/IsisTRXVU.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>
#include <mist-tools/serial_in.h>

#define TRXVU_MODE_NONE                 (-1)
#define TRXVU_MODE_INITIALIZE           (0x10)
#define TRXVU_MODE_CALLSIGN_TO          (0x21)
#define TRXVU_MODE_CALLSIGN_FROM        (0x22)
#define TRXVU_MODE_RESET                (0x30)
#define TRXVU_MODE_BEACON_SET           (0x41)
#define TRXVU_MODE_BEACON_CLEAR         (0x42)
#define TRXVU_MODE_SEND                 (0x50)
#define TRXVU_MODE_HOUSEKEEPING_RC      (0x61)
#define TRXVU_MODE_HOUSEKEEPING_TC_ALL  (0x62)
#define TRXVU_MODE_HOUSEKEEPING_TC_LAST (0x63)
#define TRXVU_MODE_GET                  (0x70)
#define TRXVU_MODE_UPTIME               (0x80)
#define TRXVU_MODE_STATE                (0x90)
#define TRXVU_MODE_FRAMECOUNT           (0xA0)
#define TRXVU_MODE_SET_IDLESTATE        (0xB1)
#define TRXVU_MODE_SET_BITRATE          (0xB2)

#define TRXVU_BITRATE_TC (trxvu_bitrate_1200) // default transmitter bitrate
#define TRXVU_ADDR_RC (0x60)        // receiver I2C address
#define TRXVU_ADDR_TC (0x61)        // transmitter I2C address
#define TRXVU_MAX_FRAMELEN_RX (200) // receiver maximum frame length
#define TRXVU_MAX_FRAMELEN_TX (235) // transmitter maximum frame length
#define TRXVU_CALLSIGN_FROM ("MIST")   // "from" callsign
#define TRXVU_CALLSIGN_TO   ("SA0SAT") // "to" callsign

struct trxvu_init_params {
	ISIStrxvuBitrate tc_bitrate;
};

struct trxvu_reset_params {
	int soft;
	int hard;
	int reset_receiver;
	int reset_transmitter;
};

struct trxvu_hk_params {
	int raw_format;
	int tc_last;
};

struct trxvu_set_params {
	int isset;
	ISIStrxvuIdleState state;
	ISIStrxvuBitrate tc_bitrate;
};

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int trxvu_initialize(const char *fname, struct trxvu_init_params *params);
static int trxvu_set_to_callsign(const char *fname, const char *callsign);
static int trxvu_set_from_callsign(const char *fname, const char *callsign);
static int trxvu_reset(const char *fname, int ignore_prompt, struct trxvu_reset_params *params);
static int trxvu_send(const char *fname, uint8_t *data, size_t datalen);
static int trxvu_get(const char *fname, struct trxvu_hk_params *params);
static int trxvu_beacon_set(const char *fname, int interval, uint8_t *data, size_t datalen);
static int trxvu_beacon_clear(const char *fname);
static int trxvu_housekeeping_rc(const char *fname, struct trxvu_hk_params *params);
static int trxvu_housekeeping_tc(const char *fname, struct trxvu_hk_params *params);
static int trxvu_uptime(const char *fname);
static int trxvu_state(const char *fname);
static int trxvu_framecount(const char *fname);
static int trxvu_set_idlestate(const char *fname, struct trxvu_set_params *params);
static int trxvu_set_bitrate(const char *fname, struct trxvu_set_params *params);

int trxvu_main(int argc, char **argv)
{
	int ret;
	int value;
	int choice;
	int mode = TRXVU_MODE_NONE;
	const char *argval = NULL;
	uint8_t data[TRXVU_MAX_FRAMELEN_TX];
	size_t datalen = 0;
	int ignore_prompt = 0;
	int interval = -1;
	struct trxvu_init_params initparams;
	struct trxvu_reset_params resetparams;
	struct trxvu_hk_params hkparams;
	struct trxvu_set_params setparams;
	initparams.tc_bitrate = TRXVU_BITRATE_TC;
	resetparams.soft = 0;
	resetparams.hard = 0;
	resetparams.reset_receiver = 0;
	resetparams.reset_transmitter = 0;
	hkparams.raw_format = 0;
	hkparams.tc_last = 0;
	setparams.isset = 0;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	if (strcmp(argv[1], "initialize") == 0) { /* INITIALIZE MODE */
		mode = TRXVU_MODE_INITIALIZE;
		optind = 2;
	} else if (strcmp(argv[1], "init") == 0) { /* INITIALIZE ALIASES */
		mode = TRXVU_MODE_INITIALIZE;
		optind = 2;
	} else if (strcmp(argv[1], "i") == 0) {
		mode = TRXVU_MODE_INITIALIZE;
		optind = 2;
	} else if (strcmp(argv[1], "callsign") == 0) { /* SET CALLSIGN MODE */
		if (argc < 3) {
			fprintf(stderr, "%s callsign: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "to") == 0) {
			mode = TRXVU_MODE_CALLSIGN_TO;
			optind = 3;
		} else if (strcmp(argv[2], "from") == 0) {
			mode = TRXVU_MODE_CALLSIGN_FROM;
			optind = 3;
		} else {
			fprintf(stderr, "%s callsign: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "csto") == 0) { /* SET CALLSIGN ALIASES */
		mode = TRXVU_MODE_CALLSIGN_TO;
		optind = 2;
	} else if (strcmp(argv[1], "csfrom") == 0) {
		mode = TRXVU_MODE_CALLSIGN_FROM;
		optind = 2;
	} else if (strcmp(argv[1], "reset") == 0) { /* RESET MODE */
		mode = TRXVU_MODE_RESET;
		if (argc < 3) {
			fprintf(stderr, "%s reset: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "both") == 0) {
			resetparams.reset_receiver = 1;
			resetparams.reset_transmitter = 1;
			if (argc < 4) {
				fprintf(stderr, "%s reset both: missing subsubmode\n\r", argv[0]);
				print_usage(argv[0]);
				return 1;
			}
			if (strcmp(argv[3], "soft") == 0) {
				resetparams.soft = 1;
				optind = 4;
			} else if (strcmp(argv[3], "hard") == 0) {
				resetparams.hard = 1;
				optind = 4;
			} else {
				fprintf(stderr, "%s reset both: invalid subsubmode \'%s\'\n\r", argv[0], argv[3]);
				print_usage(argv[0]);
				return 1;
			}
		} else if (strcmp(argv[2], "rc") == 0) {
			resetparams.reset_receiver = 1;
			if (argc < 4) {
				fprintf(stderr, "%s reset rc: missing subsubmode\n\r", argv[0]);
				print_usage(argv[0]);
				return 1;
			}
			if (strcmp(argv[3], "soft") == 0) {
				resetparams.soft = 1;
				optind = 4;
			} else if (strcmp(argv[3], "hard") == 0) {
				resetparams.hard = 1;
				optind = 4;
			} else {
				fprintf(stderr, "%s reset rc: invalid subsubmode \'%s\'\n\r", argv[0], argv[3]);
				print_usage(argv[0]);
				return 1;
			}
		} else if (strcmp(argv[2], "tc") == 0) {
			resetparams.reset_transmitter = 1;
			if (argc < 4) {
				fprintf(stderr, "%s reset tc: missing subsubmode\n\r", argv[0]);
				print_usage(argv[0]);
				return 1;
			}
			if (strcmp(argv[3], "soft") == 0) {
				resetparams.soft = 1;
				optind = 4;
			} else if (strcmp(argv[3], "hard") == 0) {
				resetparams.hard = 1;
				optind = 4;
			} else {
				fprintf(stderr, "%s reset tc: invalid subsubmode \'%s\'\n\r", argv[0], argv[3]);
				print_usage(argv[0]);
				return 1;
			}
		} else {
			fprintf(stderr, "%s reset: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "rbs") == 0) { /* RESET ALIASES */
		mode = TRXVU_MODE_RESET;
		resetparams.soft = 1;
		resetparams.reset_receiver = 1;
		resetparams.reset_transmitter = 1;
		optind = 2;
	} else if (strcmp(argv[1], "rbh") == 0) {
		mode = TRXVU_MODE_RESET;
		resetparams.hard = 1;
		resetparams.reset_receiver = 1;
		resetparams.reset_transmitter = 1;
		optind = 2;
	} else if (strcmp(argv[1], "rrs") == 0) {
		mode = TRXVU_MODE_RESET;
		resetparams.soft = 1;
		resetparams.reset_receiver = 1;
		optind = 2;
	} else if (strcmp(argv[1], "rrh") == 0) {
		mode = TRXVU_MODE_RESET;
		resetparams.hard = 1;
		resetparams.reset_receiver = 1;
		optind = 2;
	} else if (strcmp(argv[1], "rts") == 0) {
		mode = TRXVU_MODE_RESET;
		resetparams.soft = 1;
		resetparams.reset_transmitter = 1;
		optind = 2;
	} else if (strcmp(argv[1], "rth") == 0) {
		mode = TRXVU_MODE_RESET;
		resetparams.hard = 1;
		resetparams.reset_transmitter = 1;
		optind = 2;
	} else if (strcmp(argv[1], "send") == 0) { /* SEND MODE */
		mode = TRXVU_MODE_SEND;
		optind = 2;
	} else if (strcmp(argv[1], "get") == 0) { /* GET MODE */
		mode = TRXVU_MODE_GET;
		optind = 2;
	} else if (strcmp(argv[1], "beacon") == 0) { /* BEACON MODE */
		if (argc < 3) {
			fprintf(stderr, "%s beacon: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "set") == 0) {
			mode = TRXVU_MODE_BEACON_SET;
			optind = 3;
		} else if (strcmp(argv[2], "clear") == 0) {
			mode = TRXVU_MODE_BEACON_CLEAR;
			optind = 3;
		} else {
			fprintf(stderr, "%s beacon: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "bs") == 0) { /* BEACON ALIASES */
		mode = TRXVU_MODE_BEACON_SET;
		optind = 2;
	} else if (strcmp(argv[1], "bc") == 0) {
		mode = TRXVU_MODE_BEACON_CLEAR;
		optind = 2;
	} else if (strcmp(argv[1], "housekeeping") == 0) { /* HOUSEKEEPING MODE */
		if (argc < 3) {
			fprintf(stderr, "%s housekeeping: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "rc") == 0) {
			mode = TRXVU_MODE_HOUSEKEEPING_RC;
			optind = 3;
		} else if (strcmp(argv[2], "tc") == 0) {
			if (argc < 4) {
				fprintf(stderr, "%s housekeeping tc: missing subsubmode\n\r", argv[0]);
				print_usage(argv[0]);
				return 1;
			}
			if (strcmp(argv[3], "all") == 0) {
				mode = TRXVU_MODE_HOUSEKEEPING_TC_ALL;
				optind = 4;
			} else if (strcmp(argv[3], "last") == 0) {
				mode = TRXVU_MODE_HOUSEKEEPING_TC_LAST;
				optind = 4;
			} else {
				fprintf(stderr, "%s housekeeping tc: invalid subsubmode \'%s\'\n\r", argv[0], argv[3]);
				print_usage(argv[0]);
				return 1;
			}
		} else {
			fprintf(stderr, "%s housekeeping: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "hkrc") == 0) { /* HOUSEKEEPING ALIASES */
		mode = TRXVU_MODE_HOUSEKEEPING_RC;
		optind = 2;
	} else if (strcmp(argv[1], "hktca") == 0) {
		mode = TRXVU_MODE_HOUSEKEEPING_TC_ALL;
		optind = 2;
	} else if (strcmp(argv[1], "hktcl") == 0) {
		mode = TRXVU_MODE_HOUSEKEEPING_TC_LAST;
		optind = 2;
	} else if (strcmp(argv[1], "uptime") == 0) { /* UPTIME MODE */
		mode = TRXVU_MODE_UPTIME;
		optind = 2;
	} else if (strcmp(argv[1], "up") == 0) { /* UPTIME ALIASES */
		mode = TRXVU_MODE_UPTIME;
		optind = 2;
	} else if (strcmp(argv[1], "state") == 0) { /* STATE MODE */
		mode = TRXVU_MODE_STATE;
		optind = 2;
	} else if (strcmp(argv[1], "st") == 0) { /* STATE ALIASES */
		mode = TRXVU_MODE_STATE;
		optind = 2;
	} else if (strcmp(argv[1], "framecount") == 0) { /* FRAMECOUNT MODE */
		mode = TRXVU_MODE_FRAMECOUNT;
		optind = 2;
	} else if (strcmp(argv[1], "fc") == 0) { /* FRAMECOUNT ALIASES */
		mode = TRXVU_MODE_FRAMECOUNT;
		optind = 2;
	} else if (strcmp(argv[1], "set") == 0) { /* SET MODE */
		if (argc < 3) {
			fprintf(stderr, "%s set: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "idlestate") == 0) {
			mode = TRXVU_MODE_SET_IDLESTATE;
			optind = 3;
		} else if (strcmp(argv[2], "bitrate") == 0) {
			mode = TRXVU_MODE_SET_BITRATE;
			optind = 3;
		} else {
			fprintf(stderr, "%s set: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "sis") == 0) { /* SET ALIASES */
		mode = TRXVU_MODE_SET_IDLESTATE;
		optind = 2;
	} else if (strcmp(argv[1], "sbr") == 0) {
		mode = TRXVU_MODE_SET_BITRATE;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	while ((choice = getopt(argc, argv, "-hfrB:")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			switch (mode) {
			case TRXVU_MODE_CALLSIGN_TO:
			case TRXVU_MODE_CALLSIGN_FROM:
				if (argval == NULL) {
					argval = optarg;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case TRXVU_MODE_SEND:
				if (datalen < TRXVU_MAX_FRAMELEN_TX) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					} else if (value < 0 || value > 255) {
						fprintf(stderr, "%s: \'%s\' is not a value between 0 and 255\n\r", argv[0], optarg);
						return 1;
					}
					data[datalen++] = (uint8_t) value;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case TRXVU_MODE_BEACON_SET:
				if (interval == -1) {
					ret = arghandler_to_int(optarg, &interval);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					} else if (interval < 0 || interval > 0xffff) {
						fprintf(stderr, "%s: specified interval \'%s\' does not fit in an unsigned short.\n\r", argv[0], optarg);
						return 1;
					}
				} else if (datalen < TRXVU_MAX_FRAMELEN_TX) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					} else if (value < 0 || value > 255) {
						fprintf(stderr, "%s: \'%s\' is not a value between 0 and 255\n\r", argv[0], optarg);
						return 1;
					}
					data[datalen++] = (uint8_t) value;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case TRXVU_MODE_SET_IDLESTATE:
				if (!setparams.isset) {
					if (strcmp(optarg, "on") == 0) {
						setparams.state = trxvu_idle_state_on;
						setparams.isset = 1;
					} else if (strcmp(optarg, "off") == 0) {
						setparams.state = trxvu_idle_state_off;
						setparams.isset = 1;
					} else {
						fprintf(stderr, "%s: invalid idlestate \'%s\'\n\r", argv[0], optarg);
						return 1;
					}
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case TRXVU_MODE_SET_BITRATE:
				if (!setparams.isset) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
						return 1;
					}
					switch (value) {
					case 1200:
						setparams.tc_bitrate = trxvu_bitrate_1200;
						break;
					case 2400:
						setparams.tc_bitrate = trxvu_bitrate_2400;
						break;
					case 4800:
						setparams.tc_bitrate = trxvu_bitrate_4800;
						break;
					case 9600:
						setparams.tc_bitrate = trxvu_bitrate_9600;
						break;
					default:
						fprintf(stderr, "%s: \'%s\' is not a valid bitrate. Valid bitrates are 1200,2400,4800,9600.\n\r", argv[0], optarg);
						return 1;
					}
					setparams.isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			default:
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'f':
			ignore_prompt = 1;
			break;
		case 'r':
			hkparams.raw_format = 1;
			break;
		case 'B':
			ret = arghandler_to_int(optarg, &value);
			if (ret != 0) {
				fprintf(stderr, "%s: \'%s\' is not a recognized numerical value.\n\r", argv[0], optarg);
				return 1;
			}
			switch (value) {
			case 1200:
				initparams.tc_bitrate = trxvu_bitrate_1200;
				break;
			case 2400:
				initparams.tc_bitrate = trxvu_bitrate_2400;
				break;
			case 4800:
				initparams.tc_bitrate = trxvu_bitrate_4800;
				break;
			case 9600:
				initparams.tc_bitrate = trxvu_bitrate_9600;
				break;
			default:
				fprintf(stderr, "%s: \'%s\' is not a valid bitrate. Valid bitrates are 1200,2400,4800,9600.\n\r", argv[0], optarg);
				return 1;
			}
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check that argval has been set for the required modes
	switch (mode) {
	case TRXVU_MODE_CALLSIGN_TO:
	case TRXVU_MODE_CALLSIGN_FROM:
		if (argval == NULL) {
			fprintf(stderr, "%s: missing positional argument.\n\r", argv[0]);
			return 1;
		}
		break;
	case TRXVU_MODE_SEND:
		if (datalen < 1) {
			fprintf(stderr, "%s: missing specified bytes.\n\r", argv[0]);
			return 1;
		}
		break;
	case TRXVU_MODE_BEACON_SET:
		if (interval == -1) {
			fprintf(stderr, "%s: missing interval.\n\r", argv[0]);
			return 1;
		} else if (datalen < 1) {
			fprintf(stderr, "%s: missing specified bytes.\n\r", argv[0]);
			return 1;
		}
		break;
	case TRXVU_MODE_SET_IDLESTATE:
		if (!setparams.isset) {
			fprintf(stderr, "%s: missing idlestate.\n\r", argv[0]);
			return 1;
		}
		break;
	case TRXVU_MODE_SET_BITRATE:
		if (!setparams.isset) {
			fprintf(stderr, "%s: missing bitrate.\n\r", argv[0]);
			return 1;
		}
		break;
	default:
		break;
	}

	// Invoke corresponding function
	ret = 1;
	switch (mode) {
	case TRXVU_MODE_INITIALIZE:
		ret = trxvu_initialize(argv[0], &initparams);
		break;
	case TRXVU_MODE_CALLSIGN_TO:
		ret = trxvu_set_to_callsign(argv[0], argval);
		break;
	case TRXVU_MODE_CALLSIGN_FROM:
		ret = trxvu_set_from_callsign(argv[0], argval);
		break;
	case TRXVU_MODE_RESET:
		ret = trxvu_reset(argv[0], ignore_prompt, &resetparams);
		break;
	case TRXVU_MODE_SEND:
		ret = trxvu_send(argv[0], data, datalen);
		break;
	case TRXVU_MODE_GET:
		ret = trxvu_get(argv[0], &hkparams);
		break;
	case TRXVU_MODE_BEACON_SET:
		ret = trxvu_beacon_set(argv[0], interval, data, datalen);
		break;
	case TRXVU_MODE_BEACON_CLEAR:
		ret = trxvu_beacon_clear(argv[0]);
		break;
	case TRXVU_MODE_HOUSEKEEPING_RC:
		ret = trxvu_housekeeping_rc(argv[0], &hkparams);
		break;
	case TRXVU_MODE_HOUSEKEEPING_TC_ALL:
		hkparams.tc_last = 0;
		ret = trxvu_housekeeping_tc(argv[0], &hkparams);
		break;
	case TRXVU_MODE_HOUSEKEEPING_TC_LAST:
		hkparams.tc_last = 1;
		ret = trxvu_housekeeping_tc(argv[0], &hkparams);
		break;
	case TRXVU_MODE_UPTIME:
		ret = trxvu_uptime(argv[0]);
		break;
	case TRXVU_MODE_STATE:
		ret = trxvu_state(argv[0]);
		break;
	case TRXVU_MODE_FRAMECOUNT:
		ret = trxvu_framecount(argv[0]);
		break;
	case TRXVU_MODE_SET_IDLESTATE:
		ret = trxvu_set_idlestate(argv[0], &setparams);
		break;
	case TRXVU_MODE_SET_BITRATE:
		ret = trxvu_set_bitrate(argv[0], &setparams);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Provides access to the TRXVU subsystem.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s initialize [-h] [-B bitrate]\n\r", fname);
	fprintf(stderr, "       %s callsign (to|from) [-h] <value>\n\r", fname);
	fprintf(stderr, "       %s reset (both|rc|tc) (hard|soft) [-hf]\n\r", fname);
	fprintf(stderr, "       %s send [-h] <bytes ...>\n\r", fname);
	fprintf(stderr, "       %s get [-hr]\n\r", fname);
	fprintf(stderr, "       %s beacon set [-h] <interval> <bytes ...>\n\r", fname);
	fprintf(stderr, "       %s beacon clear [-h]\n\r", fname);
	fprintf(stderr, "       %s housekeeping rc [-hr]\n\r", fname);
	fprintf(stderr, "       %s housekeeping tc (all|last) [-hr]\n\r", fname);
	fprintf(stderr, "       %s uptime [-h]\n\r", fname);
	fprintf(stderr, "       %s state [-h]\n\r", fname);
	fprintf(stderr, "       %s framecount [-h]\n\r", fname);
	fprintf(stderr, "       %s set idlestate [-h] <on,off>\n\r", fname);
	fprintf(stderr, "       %s set bitrate [-h] <1200,2400,4800,9600>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -f: Ignore confirmation prompt.\n\r");
	fprintf(stderr, "    -r: Print housekeeping values as raw integer values.\n\r");
	fprintf(stderr, "    -B: Specify the transmitter bitrate [1200,2400,4800,9600] (default: %d).\n\r", TRXVU_BITRATE_TC);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Mode aliases:\n\r");
	fprintf(stderr, "    (i|init):      initialize\n\r");
	fprintf(stderr, "    cs(to|from):   callsign (to|from)\n\r");
	fprintf(stderr, "    r(b|r|t)(s|h): reset (both|rc|tc) (hard|soft)\n\r");
	fprintf(stderr, "    b(s|c):        beacon (set|clear)\n\r");
	fprintf(stderr, "    hkrc:          housekeeping rc\n\r");
	fprintf(stderr, "    hktc(a|l):     housekeeping tc (all|last)\n\r");
	fprintf(stderr, "    up:            uptime\n\r");
	fprintf(stderr, "    st:            state\n\r");
	fprintf(stderr, "    fc:            framecount\n\r");
	fprintf(stderr, "    sis:           set idlestate\n\r");
	fprintf(stderr, "    sbr:           set bitrate\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Initialize the TRXVU with the entered parameters and otherwise default
 */
static int trxvu_initialize(const char *fname, struct trxvu_init_params *params)
{
	int ret;
	ISIStrxvuI2CAddress addr;
	ISIStrxvuFrameLengths len;
	unsigned char from_callsign[7];
	unsigned char to_callsign[7];

	addr.addressVu_rc = TRXVU_ADDR_RC;
	addr.addressVu_tc = TRXVU_ADDR_TC;
	len.maxAX25frameLengthRX = TRXVU_MAX_FRAMELEN_RX;
	len.maxAX25frameLengthTX = TRXVU_MAX_FRAMELEN_TX;
	strcpy((char *) from_callsign, TRXVU_CALLSIGN_FROM);
	strcpy((char *) to_callsign, TRXVU_CALLSIGN_TO);

	// Initialize TRXVU
	ret = IsisTrxvu_initialize(&addr, &len, &params->tc_bitrate, 1);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s initialize: IsisTrxvu_initialize returned %d\n\r", fname, ret);
		return 1;
	}

	// Set default callsigns
	ret = IsisTrxvu_tcSetDefFromClSign(0, from_callsign);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s initialize: IsisTrxvu_tcSetDefFromClSign returned %d\n\r", fname, ret);
		return 1;
	}
	ret = IsisTrxvu_tcSetDefToClSign(0, to_callsign);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s initialize: IsisTrxvu_tcSetDefToClSign returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Sets the "to" callsign of the TRXVU
 */
static int trxvu_set_to_callsign(const char *fname, const char *callsign)
{
	int ret;
	unsigned char csvalue[7];

	if (strlen(callsign) > 6) {
		fprintf(stderr, "%s callsign to: Specified callsign can be at most 6 characters.\n\r", fname);
		return 1;
	}

	strcpy((char *) csvalue, callsign);

	ret = IsisTrxvu_tcSetDefToClSign(0, csvalue);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s callsign to: IsisTrxvu_tcSetDefToClSign returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Sets the "from" callsign of the TRXVU
 */
static int trxvu_set_from_callsign(const char *fname, const char *callsign)
{
	int ret;
	unsigned char csvalue[7];

	if (strlen(callsign) > 6) {
		fprintf(stderr, "%s callsign from: Specified callsign can be at most 6 characters.\n\r", fname);
		return 1;
	}

	strcpy((char *) csvalue, callsign);

	ret = IsisTrxvu_tcSetDefFromClSign(0, csvalue);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s callsign from: IsisTrxvu_tcSetDefFromClSign returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Resets the TRXVU according to the specified params.
 */
static int trxvu_reset(const char *fname, int ignore_prompt, struct trxvu_reset_params *params)
{
	int ret;

	const char *modestr_rbs = "reset both soft";
	const char *modestr_rbh = "reset both hard";
	const char *modestr_rrs = "reset rc soft";
	const char *modestr_rrh = "reset rc hard";
	const char *modestr_rts = "reset tc soft";
	const char *modestr_rth = "reset tc hard";
	const char *typestr_both = "entire TRXVU";
	const char *typestr_rc = "TRXVU receiver";
	const char *typestr_tc = "TRXVU transmitter";

	const char *modestr = NULL;
	const char *typestr = NULL;

	if (params->soft) {
		if (params->reset_receiver && params->reset_transmitter) {
			modestr = modestr_rbs;
			typestr = typestr_both;
		} else if (params->reset_receiver) {
			modestr = modestr_rrs;
			typestr = typestr_rc;
		} else if (params->reset_transmitter) {
			modestr = modestr_rts;
			typestr = typestr_tc;
		}
	} else if (params->hard) {
		if (params->reset_receiver && params->reset_transmitter) {
			modestr = modestr_rbh;
			typestr = typestr_both;
		} else if (params->reset_receiver) {
			modestr = modestr_rrh;
			typestr = typestr_rc;
		} else if (params->reset_transmitter) {
			modestr = modestr_rth;
			typestr = typestr_tc;
		}
	}

	if (modestr == NULL || typestr == NULL) {
		DEBUGLN("either modestr or typestr is NULL");
		return 1;
	}

	if (!ignore_prompt) {
		int len;
		char yes_no_buf[2];

		fprintf(stderr, "Are you sure that you want to soft reset the %s? [y/N]\n\r", typestr);
		len = serial_readline(yes_no_buf, sizeof(yes_no_buf));
		if (len != 1 || tolower((int) yes_no_buf[0]) != 'y') {
			// Did not receive an explicit yes, exiting...
			fprintf(stderr, "%s %s: %s was not reset.\n\r", fname, modestr, typestr);
			return 0;
		}
	}

	if (params->soft) {
		if (params->reset_receiver && params->reset_transmitter) {
			ret = IsisTrxvu_softReset(0);
			if (ret != E_NO_SS_ERR) {
				fprintf(stderr, "%s %s: IsisTrxvu_softReset returned %d\n\r", fname, modestr, ret);
				return 1;
			}
		} else if (params->reset_receiver) {
			ret = IsisTrxvu_componentSoftReset(0, trxvu_rc);
			if (ret != E_NO_SS_ERR) {
				fprintf(stderr, "%s %s: IsisTrxvu_componentSoftReset returned %d\n\r", fname, modestr, ret);
				return 1;
			}
		} else if (params->reset_transmitter) {
			ret = IsisTrxvu_componentSoftReset(0, trxvu_tc);
			if (ret != E_NO_SS_ERR) {
				fprintf(stderr, "%s %s: IsisTrxvu_componentSoftReset returned %d\n\r", fname, modestr, ret);
				return 1;
			}
		}
	}
	if (params->hard) {
		if (params->reset_receiver && params->reset_transmitter) {
			ret = IsisTrxvu_hardReset(0);
			if (ret != E_NO_SS_ERR) {
				fprintf(stderr, "%s %s: IsisTrxvu_hardReset returned %d\n\r", fname, modestr, ret);
				return 1;
			}
		} else if (params->reset_receiver) {
			ret = IsisTrxvu_componentHardReset(0, trxvu_rc);
			if (ret != E_NO_SS_ERR) {
				fprintf(stderr, "%s %s: IsisTrxvu_componentHardReset returned %d\n\r", fname, modestr, ret);
				return 1;
			}
		} else if (params->reset_transmitter) {
			ret = IsisTrxvu_componentHardReset(0, trxvu_tc);
			if (ret != E_NO_SS_ERR) {
				fprintf(stderr, "%s %s: IsisTrxvu_componentHardReset returned %d\n\r", fname, modestr, ret);
				return 1;
			}
		}
	}

	return 0;
}

/**
 * Send the data to the TRXVU transmitter and prints the number of remaining
 * buffer slots.
 */
static int trxvu_send(const char *fname, uint8_t *data, size_t datalen)
{
	int ret;
	unsigned char remaining;

	ret = IsisTrxvu_tcSendAX25DefClSign(0,
	                                    (unsigned char *) data,
	                                    (unsigned char) datalen,
	                                    &remaining);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s send: IsisTrxvu_tcSendAX25DefClSign returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "Sent message to TRXVU transmitter. Remmaining slots: %u\n\r", remaining);

	return 0;
}

/**
 * Fetches a received AX.25 info frame from the TRXVU receiver.
 */
static int trxvu_get(const char *fname, struct trxvu_hk_params *params)
{
	int ret;
	int col;
	unsigned short i;
	ISIStrxvuRxFrame frame;
	unsigned char buf[256];

	frame.rx_framedata = buf;

	ret = IsisTrxvu_rcGetCommandFrame(0, &frame);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s get: IsisTrxvu_rcGetCommandFrame returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "Received %hu bytes:\n\r", frame.rx_length);
	col = 0;
	for (i = 0; i < frame.rx_length; i++) {
		fprintf(stderr, "0x%02x", frame.rx_framedata[i]);
		col++;

		if (col >= 8) {
			fprintf(stderr, "\n\r");
			col = 0;
		} else {
			fprintf(stderr, " ");
		}
	}
	if (col != 0)
		fprintf(stderr, "\n\r");

	fprintf(stderr, "\n\r");
	if (params->raw_format) {
		fprintf(stderr, " - rx_doppler: %hu\n\r", frame.rx_doppler);
		fprintf(stderr, " - rx_rssi: %hu\n\r", frame.rx_rssi);
	} else {
		// Conversion is based on table 7-1 in TRXVU ICD v1.3
		double rx_doppler = (double) frame.rx_doppler;
		double rx_rssi = (double) frame.rx_rssi;

		rx_doppler = (rx_doppler * 13.352) - 22300.0;
		rx_rssi = (rx_rssi * 0.03) - 152.0;

		fprintf(stderr, " - Receiver doppler offset: %f\n\r", rx_doppler);
		fprintf(stderr, " - Receiver signal strength: %f\n\r", rx_rssi);
	}

	return 0;
}

/**
 * Sets the TRXVU beacon with the specified data and interval.
 */
static int trxvu_beacon_set(const char *fname, int interval, uint8_t *data, size_t datalen)
{
	int ret;

	ret = IsisTrxvu_tcSetAx25BeaconDefClSign(0,
	                                         (unsigned char *) data,
	                                         (unsigned char) datalen,
	                                         (unsigned short) interval);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s beacon set: IsisTrxvu_tcSetAx25BeaconDefClSign returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Clears the TRXVU beacon.
 */
static int trxvu_beacon_clear(const char *fname)
{
	int ret;

	ret = IsisTrxvu_tcClearBeacon(0);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s beacon clear: IsisTrxvu_tcClearBeacon returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Fetches and prints TRXVU receiver housekeeping data.
 */
static int trxvu_housekeeping_rc(const char *fname, struct trxvu_hk_params *params)
{
	int ret;
	ISIStrxvuRxTelemetry_revC hk;

	ret = IsisTrxvu_rcGetTelemetryAll_revC(0, &hk);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s housekeeping rc: IsisTrxvu_rcGetTelemetryAll_revC returned %d\n\r", fname, ret);
		return 1;
	}

	if (params->raw_format) {
		fprintf(stderr, "Raw TRXVU receiver housekeeping:\n\r");
		fprintf(stderr, " - rx_doppler:    %hu\n\r", hk.fields.rx_doppler);
		fprintf(stderr, " - total_current: %hu\n\r", hk.fields.total_current);
		fprintf(stderr, " - bus_volt:      %hu\n\r", hk.fields.bus_volt);
		fprintf(stderr, " - locosc_temp:   %hu\n\r", hk.fields.locosc_temp);
		fprintf(stderr, " - pa_temp:       %hu\n\r", hk.fields.pa_temp);
		fprintf(stderr, " - rx_rssi:       %hu\n\r", hk.fields.rx_rssi);
	} else {
		// Conversion is based on table 7-1 in TRXVU ICD v1.3
		double rx_doppler = (double) hk.fields.rx_doppler;
		double total_current = (double) hk.fields.total_current;
		double bus_volt = (double) hk.fields.bus_volt;
		double locosc_temp = (double) hk.fields.locosc_temp;
		double pa_temp = (double) hk.fields.pa_temp;
		double rx_rssi = (double) hk.fields.rx_rssi;

		rx_doppler = (rx_doppler * 13.352) - 22300.0;
		total_current = total_current * 0.16643964;
		bus_volt = bus_volt * 0.00488;
		locosc_temp = (locosc_temp * (-0.07669)) + 195.6037;
		pa_temp = (pa_temp * (-0.07669)) + 195.6037;
		rx_rssi = (rx_rssi * 0.03) - 152.0;

		fprintf(stderr, "Formatted TRXVU receiver housekeeping:\n\r");
		fprintf(stderr, " - Receiver signal doppler offset: %f Hz (em: 1000 Hz)\n\r", rx_doppler);
		fprintf(stderr, " - Total current consumption: %f mA (em: 4 mA)\n\r", total_current);
		fprintf(stderr, " - Bus voltage: %f V (em: 0.055 V)\n\r", bus_volt);
		fprintf(stderr, " - Local oscillator temperature: %f C (em: 1 C)\n\r", locosc_temp);
		fprintf(stderr, " - Power amplifier temperature: %f C (em: 1 C)\n\r", pa_temp);
		fprintf(stderr, " - Receiver signal strength: %f dBm (em: 3 dB)\n\r", rx_rssi);
		fprintf(stderr, "(em = error margin)\n\r");
	}

	return 0;
}

/**
 * Fetches and prints TRXVU transmitter housekeeping data.
 */
static int trxvu_housekeeping_tc(const char *fname, struct trxvu_hk_params *params)
{
	int ret;
	ISIStrxvuTxTelemetry_revC hk;

	if (params->tc_last) {
		ret = IsisTrxvu_tcGetLastTxTelemetry_revC(0, &hk);
		if (ret != E_NO_SS_ERR) {
			fprintf(stderr, "%s housekeeping tc last: IsisTrxvu_tcGetLastTxTelemetry_revC returned %d\n\r", fname, ret);
			return 1;
		}
	} else {
		ret = IsisTrxvu_tcGetTelemetryAll_revC(0, &hk);
		if (ret != E_NO_SS_ERR) {
			fprintf(stderr, "%s housekeeping tc all: IsisTrxvu_tcGetTelemetryAll_revC returned %d\n\r", fname, ret);
			return 1;
		}
	}

	if (params->raw_format) {
		fprintf(stderr, "Raw TRXVU transmitter housekeeping:\n\r");
		fprintf(stderr, " - tx_reflpwr:    %hu\n\r", hk.fields.tx_reflpwr);
		fprintf(stderr, " - tx_fwrdpwr:    %hu\n\r", hk.fields.tx_fwrdpwr);
		fprintf(stderr, " - bus_volt:      %hu\n\r", hk.fields.bus_volt);
		fprintf(stderr, " - total_current: %hu\n\r", hk.fields.total_current);
		fprintf(stderr, " - pa_temp:       %hu\n\r", hk.fields.pa_temp);
		fprintf(stderr, " - locosc_temp:   %hu\n\r", hk.fields.locosc_temp);
	} else {
		// Conversion is based on table 7-2 in TRXVU ICD v1.3
		double tx_reflpwr_dbm = (double) hk.fields.tx_reflpwr;
		double tx_reflpwr_mw = (double) hk.fields.tx_reflpwr;
		double tx_fwrdpwr_dbm = (double) hk.fields.tx_fwrdpwr;
		double tx_fwrdpwr_mw = (double) hk.fields.tx_fwrdpwr;
		double bus_volt = (double) hk.fields.bus_volt;
		double total_current = (double) hk.fields.total_current;
		double pa_temp = (double) hk.fields.pa_temp;
		double locosc_temp = (double) hk.fields.locosc_temp;

		tx_reflpwr_dbm = 20.0 * log10(tx_reflpwr_dbm * 0.00767);
		tx_reflpwr_mw = tx_reflpwr_mw * tx_reflpwr_mw * 5.887e-5;
		tx_fwrdpwr_dbm = 20.0 * log10(tx_fwrdpwr_dbm * 0.00767);
		tx_fwrdpwr_mw = tx_fwrdpwr_mw * tx_fwrdpwr_mw * 5.887e-5;
		bus_volt = bus_volt * 0.00488;
		total_current = total_current * 0.16643964;
		pa_temp = (pa_temp * (-0.07669)) + 195.6037;
		locosc_temp = (locosc_temp * (-0.07669)) + 195.6037;

		fprintf(stderr, "Formatted TRXVU transmitter housekeeping:\n\r");
		fprintf(stderr, " - Reflected power: %f dBm (em: 1.5 dB)\n\r", tx_reflpwr_dbm);
		fprintf(stderr, " -                  %f mW (em: 150 mW)\n\r", tx_reflpwr_mw);
		fprintf(stderr, " - Forward power: %f dBm (em: 1.5 dB)\n\r", tx_fwrdpwr_dbm);
		fprintf(stderr, " -                %f mW (em: 150 mW)\n\r", tx_fwrdpwr_mw);
		fprintf(stderr, " - Bus voltage: %f V (em: 0.055 V)\n\r", bus_volt);
		fprintf(stderr, " - Total current consumption: %f mA (em: 4 mA)\n\r", total_current);
		fprintf(stderr, " - Power amplifier temperature: %f C (em: 1 C)\n\r", pa_temp);
		fprintf(stderr, " - Local oscillator temperature: %f C (em: 1 C)\n\r", locosc_temp);
		fprintf(stderr, "(em = error margin)\n\r");
	}

	return 0;
}

/**
 * Prints the uptime of the TRXVU.
 */
static int trxvu_uptime(const char *fname)
{
	int ret;
	unsigned int rc_uptime;
	unsigned int tc_uptime;

	ret = IsisTrxvu_rcGetUptime(0, &rc_uptime);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s uptime: IsisTrxvu_rcGetUptime_revC returned %d\n\r", fname, ret);
		return 1;
	}

	ret = IsisTrxvu_tcGetUptime(0, &tc_uptime);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s uptime: IsisTrxvu_tcGetUptime_revC returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "TRXVU receiver uptime: %u seconds\n\r", rc_uptime);
	fprintf(stderr, "TRXVU transmitter uptime: %u seconds\n\r", tc_uptime);

	return 0;
}

/**
 * Prints the state of the TRXVU transmitter.
 */
static int trxvu_state(const char *fname)
{
	int ret;
	ISIStrxvuTransmitterState state;

	ret = IsisTrxvu_tcGetState(0, &state);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s state: IsisTrxvu_tcGetState returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "TRXVU transmitter idle state: ");
	if (state.fields.transmitter_idle_state == trxvu_idle_state_on)
		fprintf(stderr, "on\n\r");
	else
		fprintf(stderr, "off\n\r");

	fprintf(stderr, "TRXVU beacon mode status: ");
	if (state.fields.transmitter_beacon == trxvu_beacon_active)
		fprintf(stderr, "active\n\r");
	else
		fprintf(stderr, "none\n\r");

	fprintf(stderr, "TRXVU transmitter bitrate: ");
	switch (state.fields.transmitter_bitrate) {
	case trxvu_bitratestatus_1200:
		fprintf(stderr, "1200 bps\n\r");
		break;
	case trxvu_bitratestatus_2400:
		fprintf(stderr, "2400 bps\n\r");
		break;
	case trxvu_bitratestatus_4800:
		fprintf(stderr, "4800 bps\n\r");
		break;
	case trxvu_bitratestatus_9600:
		fprintf(stderr, "9600 bps\n\r");
		break;
	default:
		DEBUGLN("internal error");
		break;
	}

	return 0;
}

/**
 * Prints the out the number of frames that are in the TRXVU receiver buffer.
 */
static int trxvu_framecount(const char *fname)
{
	int ret;
	unsigned short framecount;

	ret = IsisTrxvu_rcGetFrameCount(0, &framecount);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s framecount: IsisTrxvu_rcGetFrameCount returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "TRXVU receiver frame count: %hu\n\r", framecount);

	return 0;
}

/**
 * Set TRXVU transmitter idle state.
 */
static int trxvu_set_idlestate(const char *fname, struct trxvu_set_params *params)
{
	int ret;

	ret = IsisTrxvu_tcSetIdlestate(0, params->state);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s set idlestate: IsisTrxvu_tcSetIdlestate returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Set TRXVU transmitter bitrate.
 */
static int trxvu_set_bitrate(const char *fname, struct trxvu_set_params *params)
{
	int ret;

	ret = IsisTrxvu_tcSetAx25Bitrate(0, params->tc_bitrate);
	if (ret != E_NO_SS_ERR) {
		fprintf(stderr, "%s set idlestate: IsisTrxvu_tcSetAx25Bitrate returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}
