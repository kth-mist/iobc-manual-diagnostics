/**
 * @file   progs.h
 * @author John Wikman
 */

#ifndef IOBC_PROGS_H
#define IOBC_PROGS_H

int cd_main(int argc, char **argv);
int eps_main(int argc, char **argv);
int fram_main(int argc, char **argv);
int norflash_main(int argc, char **argv);
int fsinfo_main(int argc, char **argv);
int hexdump_main(int argc, char **argv);
int i2c_main(int argc, char **argv);
int led_main(int argc, char **argv);
int ls_main(int argc, char **argv);
int mkdir_main(int argc, char **argv);
int more_main(int argc, char **argv);
int msp_prog_main(int argc, char **argv);
int rm_main(int argc, char **argv);
int rmdir_main(int argc, char **argv);
int time_main(int argc, char **argv);
int tmq_main(int argc, char **argv);
int tmtc_prog_main(int argc, char **argv);
int trxvu_main(int argc, char **argv);

#endif /* IOBC_PROGS_H */
