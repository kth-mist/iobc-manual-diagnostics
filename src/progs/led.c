/**
 * @file   led.c
 * @author John Wikman
 *
 * Access the LED's on the OBC
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hal/Drivers/LED.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>

#define LED_MODE_NONE   (-1)
#define LED_MODE_TOGGLE (0x10)
#define LED_MODE_WAVE   (0x20)

struct wave_opts {
	int reverse;
	int alternate;
};

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int led_toggle_mode(const char *fname, const char *argval);
static int led_wave_mode(const char *fname, const char *argval, struct wave_opts *wopts);

static const char *default_led_wave_count = "1";

int led_main(int argc, char **argv)
{
	int ret;
	int choice;
	int mode = LED_MODE_NONE;
	const char *argval = NULL;
	struct wave_opts wopts;
	wopts.reverse = 0;
	wopts.alternate = 0;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	if (strcmp(argv[1], "toggle") == 0) { /* TOGGLE MODE */
		mode = LED_MODE_TOGGLE;
		optind = 2;
	} else if (strcmp(argv[1], "wave") == 0) { /* WAVE MODE */
		mode = LED_MODE_WAVE;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	while ((choice = getopt(argc, argv, "-hra")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			if (argval == NULL) {
				argval = optarg;
			} else {
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'r':
			wopts.reverse = 1;
			break;
		case 'a':
			wopts.alternate = 1;
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check if required positional are set
	switch (mode) {
	case LED_MODE_WAVE:
		if (argval == NULL)
			argval = default_led_wave_count;
		break;
	case LED_MODE_TOGGLE:
		if (argval == NULL) {
			fprintf(stderr, "%s: missing value\n\r", argv[0]);
			return 1;
		}
		break;
	default:
		break;
	}

	ret = 1;
	switch (mode) {
	case LED_MODE_TOGGLE:
		ret = led_toggle_mode(argv[0], argval);
		break;
	case LED_MODE_WAVE:
		ret = led_wave_mode(argv[0], argval, &wopts);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Toggle the LEDs on the iOBC.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s toggle [-h] <led_id>\n\r", fname);
	fprintf(stderr, "       %s wave [-hra] <amount>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -r: Wave in reverse.\n\r");
	fprintf(stderr, "    -a: Wave alternating back and forth.\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Toggles a LED.
 */
static int led_toggle_mode(const char *fname, const char *argval)
{
	int ret;
	int value;

	ret = arghandler_to_int(argval, &value);
	if (ret != 0) {
		fprintf(stderr, "%s toggle: \'%s\' is not a recognized numerical value\n\r", fname, argval);
		return 1;
	}
	if ((value < 0) || (value > 3)) {
		fprintf(stderr, "%s toggle: led_id has to be a number between 0 and 3\n\r", fname);
		return 1;
	}

	LED_toggle((LED) value);

	return 0;
}

/**
 * Performs one or more LED waves based on the entered options.
 */
static int led_wave_mode(const char *fname, const char *argval, struct wave_opts *wopts)
{
	int ret;
	int times;

	ret = arghandler_to_int(argval, &times);
	if (ret != 0) {
		fprintf(stderr, "%s wave: \'%s\' is not a recognized numerical value\n\r", fname, argval);
		return 1;
	}
	if (times < 1) {
		fprintf(stderr, "%s wave: the number of times to wave must be a positive value\n\r", fname);
		return 1;
	}

	if (wopts->alternate) {
		while (times > 0) {
			if (wopts->reverse) {
				LED_waveReverse(1);
				wopts->reverse = 0;
			} else {
				LED_wave(1);
				wopts->reverse = 1;
			}

			times--;
		}
	} else if (wopts->reverse) {
		LED_waveReverse(times);
	} else {
		LED_wave(times);
	}

	return 0;
}
