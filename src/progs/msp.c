/**
 * @file   msp.c
 * @author John Wikman
 *
 * MSP functionality.
 */

#include <getopt.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <msp/msp_obc.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>
#include <mist-tools/serial_in.h>
#include <mist-tools/progparser.h>

#define MSP_MODE_NONE              (-1)
#define MSP_MODE_REQUEST           (0x11)
#define MSP_MODE_SEND_BYTES        (0x21)
#define MSP_MODE_SEND_STRING       (0x22)
#define MSP_MODE_LINK_LIST         (0x31)
#define MSP_MODE_LINK_ADD          (0x32)
#define MSP_MODE_LINK_REMOVE       (0x33)
#define MSP_MODE_LINK_RESET        (0x34)
#define MSP_MODE_LINK_LOAD_DEFAULT (0x35)

#define NUMLINKS (128)

struct msp_prog_reqopts {
	int opcode_isset;
	unsigned char opcode;

	int use_desyncretry;

	// Buffer, not set by user
	unsigned char *buf;
	unsigned long buflen;
};

struct msp_prog_sendopts {
	int opcode_isset;
	unsigned char opcode;

	int use_desyncretry;

	// Buffer, not set by user
	unsigned char *data;
	unsigned long datalen;
};

struct msp_prog_linkopts {
	int addr_isset;
	int mtu_isset;
	const char *name;

	unsigned long mtu;
	unsigned char addr;
};

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int msp_prog_request(const char *fname, msp_link_t *lnk, struct msp_prog_reqopts *opts);
static int msp_prog_send(const char *fname, msp_link_t *lnk, struct msp_prog_sendopts *opts);
static int msp_prog_link_load_default(const char *fname);
static int msp_prog_link_list(const char *fname);
static int msp_prog_link_add(const char *fname, struct msp_prog_linkopts *opts);
static int msp_prog_link_remove(const char *fname, int linkidx);
static int msp_prog_link_reset(const char *fname, int linkidx);

/**
 * MSP State to keep track of msp links between invocations of msp_main.
 */
static struct {
	int isset;
	char name[32];
	msp_link_t lnk;
} mspstate[NUMLINKS];
static unsigned char msplinkbuf[1024];

/** Returns a link index/address based on an entered argument */
static int chooselink(const char *fname, const char *arg)
{
	int i;
	int ret;
	int chosen_link;
	uint8_t addr;

	// Try to match arg with a link name
	for (i = 0; i < NUMLINKS; i++) {
		if (!mspstate[i].isset)
			continue;

		if (strcasecmp(arg, mspstate[i].name) == 0)
			return i;
	}

	// No name match
	ret = progparser_i2caddr(fname, arg, &addr);
	if (ret != 0)
		return -1;

	chosen_link = addr;
	if (!mspstate[chosen_link].isset) {
		fprintf(stderr, "%s: could not find an MSP link with address \'%s\'\n\r", fname, arg);
		return -1;
	}

	return chosen_link;
}

/** Parse argument as an opcode */
static int parseopcode(const char *fname, const char *arg, unsigned char *dst)
{
	int ret;
	uint8_t u8val;

	if (strcasecmp(arg, "ACTIVE") == 0) {
		*dst = MSP_OP_ACTIVE;
		return 0;
	} else if (strcasecmp(arg, "SLEEP") == 0) {
		*dst = MSP_OP_SLEEP;
		return 0;
	} else if (strcasecmp(arg, "POWER_OFF") == 0) {
		*dst = MSP_OP_POWER_OFF;
		return 0;
	} else if (strcasecmp(arg, "REQ_PAYLOAD") == 0) {
		*dst = MSP_OP_REQ_PAYLOAD;
		return 0;
	} else if (strcasecmp(arg, "REQ_PL") == 0) {
		*dst = MSP_OP_REQ_PAYLOAD;
		return 0;
	} else if (strcasecmp(arg, "REQ_HK") == 0) {
		*dst = MSP_OP_REQ_HK;
		return 0;
	} else if (strcasecmp(arg, "REQ_PUS") == 0) {
		*dst = MSP_OP_REQ_PUS;
		return 0;
	} else if (strcasecmp(arg, "SEND_TIME") == 0) {
		*dst = MSP_OP_SEND_TIME;
		return 0;
	} else if (strcasecmp(arg, "SEND_PUS") == 0) {
		*dst = MSP_OP_SEND_PUS;
		return 0;
	}

	ret = progparser_byte(fname, arg, &u8val);
	if (ret != 0)
		return 1;

	*dst = (unsigned char) u8val;
	return 0;
}

/** Returns a string that describes the MSP error. Will never return NULL. */
static const char *msp_prog_strerror(int error_code)
{
	static char buf[32];
	static const char *invalid_opcode = "invalid opcode";
	static const char *length_is_not_zero = "length is not zero";
	static const char *not_in_a_transaction = "not in a transaction";
	static const char *invalid_length = "invalid length";
	static const char *null_pointer = "null pointer";
	static const char *invalid_action = "invalid action";
	static const char *invalid_state = "invalid state";
	static const char *invalid_frame = "invalid frame";
	static const char *exceeded_error_threshold = "exceeded error threshold";
	static const char *received_null_frame = "received null frame";
	static const char *too_small_request_buffer = "too small request buffer";
	static const char *duplicate_transaction = "duplicate transaction";

	const char *retstr = buf;
	switch (error_code) {
	case MSP_OBC_ERR_INVALID_OPCODE:
		retstr = invalid_opcode;
		break;
	case MSP_OBC_ERR_LENGTH_NOT_ZERO:
		retstr = length_is_not_zero;
		break;
	case MSP_OBC_ERR_NOT_IN_A_TRANSACTION:
		retstr = not_in_a_transaction;
		break;
	case MSP_OBC_ERR_INVALID_LENGTH:
		retstr = invalid_length;
		break;
	case MSP_OBC_ERR_NULL_POINTER:
		retstr = null_pointer;
		break;
	case MSP_OBC_ERR_INVALID_ACTION:
		retstr = invalid_action;
		break;
	case MSP_OBC_ERR_INVALID_STATE:
		retstr = invalid_state;
		break;
	case MSP_OBC_ERR_INVALID_FRAME:
		retstr = invalid_frame;
		break;
	case MSP_OBC_ERR_EXCEEDED_ERROR_THRESHOLD:
		retstr = exceeded_error_threshold;
		break;
	case MSP_OBC_ERR_RECEIVED_NULL_FRAME:
		retstr = received_null_frame;
		break;
	case MSP_OBC_ERR_TOO_SMALL_REQUEST_BUFFER:
		retstr = too_small_request_buffer;
		break;
	case MSP_OBC_ERR_DUPLICATE_TRANSACTION:
		retstr = duplicate_transaction;
		break;
	default:
		snprintf(buf, sizeof(buf), "error code %d", error_code);
		retstr = buf;
		break;
	}

	return retstr;
}

int msp_prog_main(int argc, char **argv)
{
	int ret;
	int value;
	uint8_t u8val;
	int choice;
	int mode = MSP_MODE_NONE;
	int chosen_link = -1;
	static unsigned char buf[1024];
	struct msp_prog_reqopts reqopts;
	struct msp_prog_sendopts sendopts;
	struct msp_prog_linkopts linkopts;
	reqopts.opcode_isset = 0;
	reqopts.use_desyncretry = 0;
	reqopts.buf = buf;
	reqopts.buflen = sizeof(buf);
	sendopts.opcode_isset = 0;
	sendopts.use_desyncretry = 0;
	sendopts.data = buf;
	sendopts.datalen = 0;
	linkopts.addr_isset = 0;
	linkopts.mtu_isset = 0;
	linkopts.name = NULL;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan and set the mode variable
	if (strcmp(argv[1], "request") == 0) { /* REQUEST MODE */
		mode = MSP_MODE_REQUEST;
		optind = 2;
	} else if (strcmp(argv[1], "req") == 0) { /* REQUEST ALIASES */
		mode = MSP_MODE_REQUEST;
		optind = 2;
	} else if (strcmp(argv[1], "r") == 0) {
		mode = MSP_MODE_REQUEST;
		optind = 2;
	} else if (strcmp(argv[1], "send") == 0) { /* SEND MODE */
		if (argc < 3) {
			fprintf(stderr, "%s send: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "bytes") == 0) {
			mode = MSP_MODE_SEND_BYTES;
			optind = 3;
		} else if (strcmp(argv[2], "string") == 0) {
			mode = MSP_MODE_SEND_STRING;
			optind = 3;
		} else {
			fprintf(stderr, "%s send: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "sb") == 0) { /* SEND ALIASES */
		mode = MSP_MODE_REQUEST;
		optind = 2;
	} else if (strcmp(argv[1], "ss") == 0) {
		mode = MSP_MODE_SEND_STRING;
		optind = 2;
	} else if (strcmp(argv[1], "link") == 0) { /* LINK MODE */
		if (argc < 3) {
			fprintf(stderr, "%s link: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "load") == 0) {
			if (argc < 4) {
				fprintf(stderr, "%s link load: missing subsubmode\n\r", argv[0]);
				print_usage(argv[0]);
				return 1;
			}
			if (strcmp(argv[3], "default") == 0) {
				mode = MSP_MODE_LINK_LOAD_DEFAULT;
				optind = 4;
			} else {
				fprintf(stderr, "%s link load: invalid subsubmode \'%s\'\n\r", argv[0], argv[3]);
				print_usage(argv[0]);
				return 1;
			}
		} else if (strcmp(argv[2], "list") == 0) {
			mode = MSP_MODE_LINK_LIST;
			optind = 3;
		} else if (strcmp(argv[2], "add") == 0) {
			mode = MSP_MODE_LINK_ADD;
			optind = 3;
		} else if (strcmp(argv[2], "remove") == 0) {
			mode = MSP_MODE_LINK_REMOVE;
			optind = 3;
		} else if (strcmp(argv[2], "reset") == 0) {
			mode = MSP_MODE_LINK_RESET;
			optind = 3;
		} else {
			fprintf(stderr, "%s link: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "ll") == 0) { /* LINK ALIASES */
		mode = MSP_MODE_LINK_LIST;
		optind = 2;
	} else if (strcmp(argv[1], "lld") == 0) {
		mode = MSP_MODE_LINK_LOAD_DEFAULT;
		optind = 2;
	} else if (strcmp(argv[1], "la") == 0) {
		mode = MSP_MODE_LINK_ADD;
		optind = 2;
	} else if (strcmp(argv[1], "lrm") == 0) {
		mode = MSP_MODE_LINK_REMOVE;
		optind = 2;
	} else if (strcmp(argv[1], "lrs") == 0) {
		mode = MSP_MODE_LINK_RESET;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan choices
	while ((choice = getopt(argc, argv, "-hS")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			switch (mode) {
			case MSP_MODE_REQUEST:
				if (chosen_link == -1) {
					chosen_link = chooselink(argv[0], optarg);
					if (chosen_link == -1)
						return 1;
				} else if (!reqopts.opcode_isset) {
					ret = parseopcode(argv[0], optarg, &reqopts.opcode);
					if (ret != 0)
						return 1;

					reqopts.opcode_isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case MSP_MODE_SEND_BYTES:
				if (chosen_link == -1) {
					chosen_link = chooselink(argv[0], optarg);
					if (chosen_link == -1)
						return 1;
				} else if (!sendopts.opcode_isset) {
					ret = parseopcode(argv[0], optarg, &sendopts.opcode);
					if (ret != 0)
						return 1;

					sendopts.opcode_isset = 1;
				} else if (sendopts.datalen < sizeof(buf)) {
					ret = progparser_byte(argv[0], optarg, &u8val);
					if (ret != 0)
						return 1;

					buf[sendopts.datalen++] = (unsigned char) u8val;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case MSP_MODE_SEND_STRING:
				if (chosen_link == -1) {
					chosen_link = chooselink(argv[0], optarg);
					if (chosen_link == -1)
						return 1;
				} else if (!sendopts.opcode_isset) {
					ret = parseopcode(argv[0], optarg, &sendopts.opcode);
					if (ret != 0)
						return 1;

					sendopts.opcode_isset = 1;
				} else if (sendopts.datalen == 0) {
					sendopts.datalen = strlen(optarg);
					if (sendopts.datalen > sizeof(buf)) {
						fprintf(stderr, "%s: string is too large (%lu bytes), maximum size is %u bytes\n\r", argv[0], sendopts.datalen, sizeof(buf));
						return 1;
					}
					memcpy(buf, optarg, sendopts.datalen);
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case MSP_MODE_LINK_ADD:
				if (linkopts.name == NULL) {
					if (strlen(optarg) > (sizeof(mspstate[0].name) - 1)) {
						fprintf(stderr, "%s: the linkname \'%s\' is too long, maximum allowed length is %u characters.\n\r", argv[0], optarg, sizeof(mspstate[0].name) - 1);
						return 1;
					}
					linkopts.name = optarg;
				} else if (!linkopts.addr_isset) {
					// No name match
					ret = progparser_i2caddr(argv[0], optarg, &u8val);
					if (ret != 0)
						return 1;

					linkopts.addr = (unsigned char) u8val;
					linkopts.addr_isset = 1;
				} else if (!linkopts.mtu_isset) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					}
					if ((value < 4) || (value > (int) sizeof(msplinkbuf))) {
						fprintf(stderr, "%s: MTU must be between 4 and %u bytes\n\r", argv[0], sizeof(msplinkbuf));
						return 1;
					}
					linkopts.mtu = (unsigned long) value;
					linkopts.mtu_isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case MSP_MODE_LINK_REMOVE:
			case MSP_MODE_LINK_RESET:
				if (chosen_link == -1) {
					chosen_link = chooselink(argv[0], optarg);
					if (chosen_link == -1)
						return 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			default:
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'S':
			reqopts.use_desyncretry = 1;
			sendopts.use_desyncretry = 1;
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check that required positionals are set
	switch (mode) {
	case MSP_MODE_REQUEST:
		if (chosen_link == -1) {
			fprintf(stderr, "%s: missing link\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (!reqopts.opcode_isset) {
			fprintf(stderr, "%s: missing opcode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case MSP_MODE_SEND_BYTES:
	case MSP_MODE_SEND_STRING:
		if (chosen_link == -1) {
			fprintf(stderr, "%s: missing link\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (!sendopts.opcode_isset) {
			fprintf(stderr, "%s: missing opcode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case MSP_MODE_LINK_ADD:
		if (linkopts.name == NULL) {
			fprintf(stderr, "%s: missing name\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (!linkopts.addr_isset) {
			fprintf(stderr, "%s: missing address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (!linkopts.mtu_isset) {
			fprintf(stderr, "%s: missing mtu\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case MSP_MODE_LINK_REMOVE:
	case MSP_MODE_LINK_RESET:
		if (chosen_link == -1) {
			fprintf(stderr, "%s: missing link\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	default:
		break;
	}

	// Invoke corresponding functions
	ret = 1;
	switch (mode) {
	case MSP_MODE_REQUEST:
		ret = msp_prog_request(argv[0], &mspstate[chosen_link].lnk, &reqopts);
		break;
	case MSP_MODE_SEND_BYTES:
	case MSP_MODE_SEND_STRING:
		ret = msp_prog_send(argv[0], &mspstate[chosen_link].lnk, &sendopts);
		break;
	case MSP_MODE_LINK_LOAD_DEFAULT:
		ret = msp_prog_link_load_default(argv[0]);
		break;
	case MSP_MODE_LINK_LIST:
		ret = msp_prog_link_list(argv[0]);
		break;
	case MSP_MODE_LINK_ADD:
		ret = msp_prog_link_add(argv[0], &linkopts);
		break;
	case MSP_MODE_LINK_REMOVE:
		ret = msp_prog_link_remove(argv[0], chosen_link);
		break;
	case MSP_MODE_LINK_RESET:
		ret = msp_prog_link_reset(argv[0], chosen_link);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: MSP functionality (OBC side).\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s request [-h] <link> <opcode>\n\r", fname);
	fprintf(stderr, "       %s send bytes [-h] <link> <opcode> [values...]\n\r", fname);
	fprintf(stderr, "       %s send string [-h] <link> <opcode> <text>\n\r", fname);
	fprintf(stderr, "       %s link load default [-h]\n\r", fname);
	fprintf(stderr, "       %s link list [-h]\n\r", fname);
	fprintf(stderr, "       %s link add [-h] <name> <address> <mtu>\n\r", fname);
	fprintf(stderr, "       %s link (remove|reset) [-h] <link> \n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -S: Use desync retry functionality on transactions.\n\r");
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Mode aliases:\n\r");
	fprintf(stderr, "    (r|req):         request\n\r");
	fprintf(stderr, "    s(b|s):          send (bytes|string)\n\r");
	fprintf(stderr, "    l(l|ld|a|rm|rs): link (list|load default|add|remove|reset)\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Performs an MSP request transaction with the specified link and options.
 */
static int msp_prog_request(const char *fname, msp_link_t *lnk, struct msp_prog_reqopts *opts)
{
	unsigned long i;
	struct msp_response r;
	const char *funnames[] = {"msp_obc_recv", "msp_obc_recv_desyncretry"};
	const char *reqf = funnames[0];

	if (opts->use_desyncretry) {
		r = msp_obc_recv_desyncretry(lnk, opts->opcode, opts->buf, opts->buflen);
		reqf = funnames[1];
	} else {
		r = msp_obc_recv(lnk, opts->opcode, opts->buf, opts->buflen);
	}
	switch (r.status) {
	case MSP_RESPONSE_TRANSACTION_SUCCESSFUL:
		// This is expected!
		break;
	case MSP_RESPONSE_TRANSACTION_ABORTED:
		fprintf(stderr, "%s request: %s returned MSP_RESPONSE_TRANSACTION_ABORTED: %s\n\r", fname, reqf, msp_prog_strerror(r.error_code));
		break;
	case MSP_RESPONSE_ERROR:
		fprintf(stderr, "%s request: %s returned MSP_RESPONSE_ERROR: %s\n\r", fname, reqf, msp_prog_strerror(r.error_code));
		break;
	case MSP_RESPONSE_OK:
		fprintf(stderr, "%s request: %s returned MSP_RESPONSE_OK\n\r", fname, reqf);
		break;
	case MSP_RESPONSE_BUSY:
		fprintf(stderr, "%s request: %s returned MSP_RESPONSE_BUSY\n\r", fname, reqf);
		break;
	default:
		fprintf(stderr, "%s request: %s returned with an unknown status\n\r", fname, reqf);
		break;
	}

	if (r.status != MSP_RESPONSE_TRANSACTION_SUCCESSFUL)
		return 1;

	fprintf(stderr, "Received %lu bytes of data with opcode 0x%02X: (transaction ID: %u)", r.len, r.opcode, r.transaction_id);
	for (i = 0; i < r.len; i++) {
		if ((i % 10) == 0)
			fprintf(stderr, "\n\r");
		else
			fprintf(stderr, " ");

		fprintf(stderr, "0x%02X", opts->buf[i]);
	}
	fprintf(stderr, "\n\r");

	return 0;
}

/**
 * Sends a message over the specified link.
 */
static int msp_prog_send(const char *fname, msp_link_t *lnk, struct msp_prog_sendopts *opts)
{
	struct msp_response r;
	const char *funnames[] = {"msp_obc_send", "msp_obc_send_desyncretry"};
	const char *reqf = funnames[0];

	if (opts->use_desyncretry) {
		r = msp_obc_send_desyncretry(lnk, opts->opcode, opts->data, opts->datalen);
		reqf = funnames[1];
	} else {
		r = msp_obc_send(lnk, opts->opcode, opts->data, opts->datalen);
	}
	switch (r.status) {
	case MSP_RESPONSE_TRANSACTION_SUCCESSFUL:
		// This is expected!
		break;
	case MSP_RESPONSE_TRANSACTION_ABORTED:
		fprintf(stderr, "%s send: %s returned MSP_RESPONSE_TRANSACTION_ABORTED: %s\n\r", fname, reqf, msp_prog_strerror(r.error_code));
		break;
	case MSP_RESPONSE_ERROR:
		fprintf(stderr, "%s send: %s returned MSP_RESPONSE_ERROR: %s\n\r", fname, reqf, msp_prog_strerror(r.error_code));
		break;
	case MSP_RESPONSE_OK:
		fprintf(stderr, "%s send: %s returned MSP_RESPONSE_OK\n\r", fname, reqf);
		break;
	case MSP_RESPONSE_BUSY:
		fprintf(stderr, "%s send: %s returned MSP_RESPONSE_BUSY\n\r", fname, reqf);
		break;
	default:
		fprintf(stderr, "%s send: %s returned with an unknown status\n\r", fname, reqf);
		break;
	}

	if (r.status != MSP_RESPONSE_TRANSACTION_SUCCESSFUL)
		return 1;

	fprintf(stderr, "Sent %lu bytes with opcode 0x%02X (transaction ID: %u)\n\r", r.len, r.opcode, r.transaction_id);
	return 0;
}

/**
 * Load default MSP link configurations.
 */
static int msp_prog_link_load_default(const char *fname)
{
	int i, j;
	int confirmed;
	int numdefaults = 1;
	int added_defaults;

	struct {
		char name[32];
		unsigned char addr;
		unsigned long mtu;
	} defaults[] = {{"XTEST", 0x55, 507}};

	fprintf(stderr, "Default link configurations:\n\r");
	for (i = 0; i < numdefaults; i++) {
		fprintf(stderr, " - %s\n\r", defaults[i].name);
		fprintf(stderr, "   - Address: 0x%02X\n\r", defaults[i].addr);
		fprintf(stderr, "   - MTU:     %lu\n\r", defaults[i].mtu);
	}
	confirmed = serial_promptconfirm("Load these configurations?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s load default: No configuration was loaded.\n\r", fname);
		return 0;
	}

	// Check collisions for each default
	added_defaults = 0;
	for (i = 0; i < numdefaults; i++) {
		int name_collision = -1;
		int addr_collision = -1;

		// Check for name and address collision
		// (We know that name is unique to a single link)
		for (j = 0; j < NUMLINKS; j++) {
			if (!mspstate[j].isset)
				continue;

			if (strcasecmp(mspstate[j].name, defaults[i].name) == 0)
				name_collision = j;

			if (j == (int) defaults[i].addr)
				addr_collision = j;
		}

		if (name_collision != -1)
			fprintf(stderr, "The name \'%s\' is already in use by the link with address 0x%02X.\n\r", defaults[i].name, addr_collision);
		if (addr_collision != -1)
			fprintf(stderr, "The address 0x%02X is already in use by the link \'%s\'\n\r", addr_collision, mspstate[addr_collision].name);

		if ((addr_collision != -1) && (name_collision != -1))
			fprintf(stderr, "Remove both of these links");
		else if (addr_collision != -1)
			fprintf(stderr, "Remove the link with this address");
		else if (name_collision != -1)
			fprintf(stderr, "Remove the link with this name");

		confirmed = 1;
		if ((addr_collision != -1) || (name_collision != -1)) {
			fprintf(stderr, " and add the default %s", defaults[i].name);
			confirmed = serial_promptconfirm(" link?", 0);
		}

		if (!confirmed) {
			fprintf(stderr, "%s link load defaults: The default %s link was not added.\n\r", fname, defaults[i].name);
			continue;
		}

		if (addr_collision != -1)
			mspstate[addr_collision].isset = 0;
		if (name_collision != -1)
			mspstate[name_collision].isset = 0;

		mspstate[defaults[i].addr].isset = 1;
		strncpy(mspstate[defaults[i].addr].name, defaults[i].name, sizeof(mspstate[defaults[i].addr].name));
		mspstate[defaults[i].addr].lnk = msp_create_link(defaults[i].addr,
			                                             msp_seqflags_init(),
			                                             msplinkbuf,
			                                             defaults[i].mtu);
		added_defaults += 1;
	}

	if (added_defaults == 0)
		fprintf(stderr, "No default links were added.\n\r");
	else if (added_defaults == 1)
		fprintf(stderr, "1 default link was added.\n\r");
	else
		fprintf(stderr, "%d default links were added.\n\r", added_defaults);

	return 0;
}

/**
 * Print a list of all the active links.
 */
static int msp_prog_link_list(const char *fname)
{
	int i;
	int have_printed = 0;

	(void) fname;

	for (i = 0; i < NUMLINKS; i++) {
		if (!mspstate[i].isset)
			continue;

		if (!have_printed) {
			fprintf(stderr, "The following links are configured for use:\n\r");
			have_printed = 1;
		}

		fprintf(stderr, " - %s\n\r", mspstate[i].name);
		fprintf(stderr, "   - Address: 0x%02X\n\r", i);
		fprintf(stderr, "   - MTU:     %lu\n\r", mspstate[i].lnk.mtu);
	}

	if (!have_printed)
			fprintf(stderr, "No links are available.\n\r");

	return 0;
}

/**
 * Adds an MSP link.
 */
static int msp_prog_link_add(const char *fname, struct msp_prog_linkopts *opts)
{
	int i;
	int name_collision = -1;
	int addr_collision = -1;
	int confirmed;

	// Check for name and address collision
	// (We know that name is unique to a single link)
	for (i = 0; i < NUMLINKS; i++) {
		if (!mspstate[i].isset)
			continue;

		if (strcasecmp(mspstate[i].name, opts->name) == 0)
			name_collision = i;

		if (i == (int) opts->addr)
			addr_collision = i;
	}

	if (name_collision != -1)
		fprintf(stderr, "The name \'%s\' is already in use by the link with address 0x%02X.\n\r", opts->name, addr_collision);
	if (addr_collision != -1)
		fprintf(stderr, "The address 0x%02X is already in use by the link \'%s\'\n\r", addr_collision, mspstate[addr_collision].name);

	if ((addr_collision != -1) && (name_collision != -1))
		fprintf(stderr, "Remove both of these links");
	else if (addr_collision != -1)
		fprintf(stderr, "Remove the link with this address");
	else if (name_collision != -1)
		fprintf(stderr, "Remove the link with this name");

	confirmed = 1;
	if ((addr_collision != -1) || (name_collision != -1))
		confirmed = serial_promptconfirm(" and add the new link?", 0);

	if (!confirmed) {
		fprintf(stderr, "%s link add: The link was not added and no link was removed.\n\r", fname);
		return 0;
	}

	if (addr_collision != -1)
		mspstate[addr_collision].isset = 0;
	if (name_collision != -1)
		mspstate[name_collision].isset = 0;

	mspstate[opts->addr].isset = 1;
	strncpy(mspstate[opts->addr].name, opts->name, sizeof(mspstate[opts->addr].name));
	mspstate[opts->addr].lnk = msp_create_link(opts->addr,
		                                       msp_seqflags_init(),
		                                       msplinkbuf,
		                                       opts->mtu);

	return 0;
}

/**
 * Removes the link at the specified index.
 */
static int msp_prog_link_remove(const char *fname, int linkidx)
{
	int confirmed;

	fprintf(stderr, "Remove the link %s (address: 0x%02X)", mspstate[linkidx].name, linkidx);
	confirmed = serial_promptconfirm("?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s link remove: The link was not removed\n\r", fname);
		return 0;
	}

	mspstate[linkidx].isset = 0;

	return 0;
}

/**
 * Resets the link at the specified index.
 */
static int msp_prog_link_reset(const char *fname, int linkidx)
{
	int confirmed;
	unsigned long mtu;

	fprintf(stderr, "Reset the link %s (address: 0x%02X)", mspstate[linkidx].name, linkidx);
	confirmed = serial_promptconfirm("?", 0);
	if (!confirmed) {
		fprintf(stderr, "%s link reset: The link was not reset\n\r", fname);
		return 0;
	}

	mtu = mspstate[linkidx].lnk.mtu;

	mspstate[linkidx].lnk = msp_create_link((unsigned char) linkidx,
		                                    msp_seqflags_init(),
		                                    msplinkbuf,
		                                    mtu);

	return 0;
}
