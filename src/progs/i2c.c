/**
 * @file   i2c.c
 * @author John Wikman
 *
 * Program for reading and writing on the I2C bus (as master).
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hal/Drivers/I2C.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>

#define I2C_MODE_NONE        (-1)
#define I2C_MODE_WRITE_BYTES (0x11)
#define I2C_MODE_READ        (0x21)
#define I2C_MODE_STATUS      (0x31)

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int i2c_write_bytes(const char *fname, unsigned int addr, unsigned char *data, unsigned int len);
static int i2c_read(const char *fname, unsigned int addr, unsigned int len, unsigned char *buf);
static int i2c_status(const char *fname);

int i2c_main(int argc, char **argv)
{
	int ret;
	int value;
	int choice;
	int mode = I2C_MODE_NONE;
	unsigned int addr;
	int addr_isset = 0;
	unsigned int readlen;
	int readlen_isset = 0;
	static unsigned char buf[512];
	unsigned int buflen = 0;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan and set the mode variable
	if (strcmp(argv[1], "write") == 0) { /* WRITE MODE */
		if (argc < 3) {
			fprintf(stderr, "%s write: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "bytes") == 0) {
			mode = I2C_MODE_WRITE_BYTES;
			optind = 3;
		} else {
			fprintf(stderr, "%s write: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "wb") == 0) { /* WRITE ALIASES */
		mode = I2C_MODE_WRITE_BYTES;
		optind = 2;
	} else if (strcmp(argv[1], "read") == 0) { /* READ MODE */
		mode = I2C_MODE_READ;
		optind = 2;
	} else if (strcmp(argv[1], "r") == 0) { /* READ ALIASES */
		mode = I2C_MODE_READ;
		optind = 2;
	} else if (strcmp(argv[1], "status") == 0) { /* STATUS MODE */
		mode = I2C_MODE_STATUS;
		optind = 2;
	} else if (strcmp(argv[1], "s") == 0) { /* STATUS ALIASES */
		mode = I2C_MODE_STATUS;
		optind = 2;
	} else if (strcmp(argv[1], "st") == 0) {
		mode = I2C_MODE_STATUS;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan choices
	while ((choice = getopt(argc, argv, "-h")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			switch (mode) {
			case I2C_MODE_WRITE_BYTES:
				if (!addr_isset) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					}
					if ((value < 0) || (value > 0x7F)) {
						fprintf(stderr, "%s: the address \'%s\' can not be contained within 7 bits\n\r", argv[0], optarg);
						return 1;
					}
					addr = (unsigned int) value;
					addr_isset = 1;
				} else if (buflen < sizeof(buf)) {
					// scan the bytes to write
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					}
					if ((value < 0) || (value > 0xFF)) {
						fprintf(stderr, "%s: the value \'%s\' can not be contained within a byte\n\r", argv[0], optarg);
						return 1;
					}
					buf[buflen++] = (unsigned char) value;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case I2C_MODE_READ:
				if (!addr_isset) {
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					}
					if ((value < 0) || (value > 0x7F)) {
						fprintf(stderr, "%s: the address \'%s\' can not be contained within 7 bits\n\r", argv[0], optarg);
						return 1;
					}
					addr = (unsigned int) value;
					addr_isset = 1;
				} else if (!readlen_isset) {
					// scan the bytes to write
					ret = arghandler_to_int(optarg, &value);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					}
					if (value < 1) {
						fprintf(stderr, "%s: the read length \'%s\' must be a positive number\n\r", argv[0], optarg);
						return 1;
					}
					readlen = (unsigned int) value;
					readlen_isset = 1;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			default:
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check that required positionals are set
	switch (mode) {
	case I2C_MODE_WRITE_BYTES:
		if (!addr_isset) {
			fprintf(stderr, "%s: must specify an address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		} else if (buflen < 1) {
			fprintf(stderr, "%s: must specify which bytes to write\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case I2C_MODE_READ:
		if (!addr_isset) {
			fprintf(stderr, "%s: must specify an address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		} else if (!readlen_isset) {
			fprintf(stderr, "%s: must specify how many bytes to read\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		} else if (readlen > sizeof(buf)) {
			fprintf(stderr, "%s: length is too large, max length is %u\n\r", argv[0], sizeof(buf));
			print_usage(argv[0]);
			return 1;
		}
		break;
	default:
		break;
	}

	// Invoke corresponding functions
	ret = 1;
	switch (mode) {
	case I2C_MODE_WRITE_BYTES:
		ret = i2c_write_bytes(argv[0], addr, buf, buflen);
		break;
	case I2C_MODE_READ:
		ret = i2c_read(argv[0], addr, readlen, buf);
		break;
	case I2C_MODE_STATUS:
		ret = i2c_status(argv[0]);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: I2C (master) functionality.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s write bytes [-h] <i2caddr> <values...>\n\r", fname);
	fprintf(stderr, "       %s read [-h] <i2caddr> <length>\n\r", fname);
	fprintf(stderr, "       %s status [-h]\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Mode aliases:\n\r");
	fprintf(stderr, "    wb:     write bytes\n\r");
	fprintf(stderr, "    r:      read\n\r");
	fprintf(stderr, "    (s|st): status\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Writes the bytes pointed to by buf to the specified I2C address.
 */
static int i2c_write_bytes(const char *fname, unsigned int addr, unsigned char *data, unsigned int len)
{
	int ret;

	ret = I2C_write(addr, data, len);
	if (ret != 0) {
		fprintf(stderr, "%s write bytes: I2C_write returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "Wrote %u bytes to address 0x%02X.\n\r", len, addr);
	return 0;
}

/**
 * Reads len bytes from the specified I2C address.
 */
static int i2c_read(const char *fname, unsigned int addr, unsigned int len, unsigned char *buf)
{
	int ret;
	unsigned int i;

	ret = I2C_read(addr, buf, len);
	if (ret != 0) {
		fprintf(stderr, "%s read: I2C_read returned %d\n\r", fname, ret);
		return 1;
	}

	fprintf(stderr, "Read %u bytes from address 0x%02X:", len, addr);
	for (i = 0; i < len; i++) {
		if ((i % 8) == 0)
			fprintf(stderr, "\n\r");
		else
			fprintf(stderr, " ");
		fprintf(stderr, "0x%02X", buf[i]);
	}
	fprintf(stderr, "\n\r");

	return 0;
}

/**
 * Prints current I2C driver state and transfer status
 */
static int i2c_status(const char *fname)
{
	(void) fname;

	I2CdriverState drv_st = I2C_getDriverState();
	I2CtransferStatus tx_st = I2C_getCurrentTransferStatus();

	fprintf(stderr, "I2C Driver State: ");
	switch (drv_st) {
	case idle_i2cState:
		fprintf(stderr, "idle");
		break;
	case uninitialized_i2cState:
		fprintf(stderr, "uninitialized");
		break;
	case write_i2cState:
		fprintf(stderr, "write");
		break;
	case read_i2cState:
		fprintf(stderr, "read");
		break;
	case error_i2cState:
		fprintf(stderr, "error");
		break;
	default:
		fprintf(stderr, "unknown: (0x%02X)", drv_st);
		break;
	}
	fprintf(stderr, "\n\r");

	fprintf(stderr, "I2C Current Transfer Status: ");
	switch (tx_st) {
	case done_i2c:
		fprintf(stderr, "done");
		break;
	case pending_i2c:
		fprintf(stderr, "pending");
		break;
	case writeDone_i2c:
		fprintf(stderr, "write done");
		break;
	case writeDoneReadStarted_i2c:
		fprintf(stderr, "write done, read started");
		break;
	case writeError_i2c:
		fprintf(stderr, "write error");
		break;
	case readError_i2c:
		fprintf(stderr, "read error");
		break;
	case timeoutError_i2c:
		fprintf(stderr, "timeout error");
		break;
	case error_i2c:
		fprintf(stderr, "error");
		break;
	default:
		fprintf(stderr, "unknown (%d)", drv_st);
		break;
	}
	fprintf(stderr, "\n\r");

	return 0;
}
