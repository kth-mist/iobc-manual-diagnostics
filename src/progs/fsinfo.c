/**
 * @file   fsinfo.c
 * @author John Wikman
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>

#include <mist-tools/arghandler.h>

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);
static void fsinfo_print(void);

int fsinfo_main(int argc, char **argv)
{
	int choice;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	while ((choice = getopt(argc, argv, "-h")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
			return 1;
		case 'h':
			print_help(argv[0]);
			return 0;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	fsinfo_print();

	return 0;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Print filesystem information.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s [-h]\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this message.\n\r");
	fprintf(stderr, "\n\r");
}

static void fsinfo_print(void)
{
	int ret;
	F_SPACE fspace;
	char oem_buf[64];
	int drive;

	ret = f_getfreespace(0, &fspace);
	if (ret != F_NO_ERROR) {
		fprintf(stderr, "error: f_getfreespace returned %d\n\r", ret);
		return;
	}

	ret = f_get_oem(0, oem_buf, sizeof(oem_buf));
	if (ret != F_NO_ERROR) {
		fprintf(stderr, "error: f_get_oem returned %d\n\r", ret);
		return;
	}
	oem_buf[sizeof(oem_buf) - 1] = 0;

	drive = f_getdrive();

	fprintf(stderr, "Filesystem Information\n\r");
	fprintf(stderr, "    OEM name: %s\n\r", oem_buf);
	fprintf(stderr, "    drive number: %d\n\r", drive);
	fprintf(stderr, "    space:\n\r");
	fprintf(stderr, "        total: %lu bytes\n\r", fspace.total);
	fprintf(stderr, "        free: %lu bytes\n\r", fspace.free);
	fprintf(stderr, "        used: %lu bytes\n\r", fspace.used);
	fprintf(stderr, "        bad: %lu bytes\n\r", fspace.bad);
}
