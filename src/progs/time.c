/**
 * @file   time.c
 * @author John Wikman
 *
 * Check and set time.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hal/Timing/Time.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int print_current_time(const char *fname, int fmt);
static int time_set_value(const char *fname, const char *value, int mode);

#define TIME_MODE_NONE       (-1)
#define TIME_MODE_GET        (0x10)
#define TIME_MODE_SET_YEAR   (0x20)
#define TIME_MODE_SET_MONTH  (0x21)
#define TIME_MODE_SET_DAY    (0x22)
#define TIME_MODE_SET_HOUR   (0x23)
#define TIME_MODE_SET_MINUTE (0x24)
#define TIME_MODE_SET_SECOND (0x25)
#define TIME_MODE_SET_EPOCH  (0x26)

#define TIME_DISPLAY_FMT_ISO       (1)
#define TIME_DISPLAY_FMT_UNIXEPOCH (2)

int time_main(int argc, char **argv)
{
	int ret;
	int choice;
	int mode = TIME_MODE_NONE;
	int fmt = TIME_DISPLAY_FMT_ISO;
	const char *timevalarg = NULL;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	if (strcmp(argv[1], "get") == 0) { /* GET MODE */
		mode = TIME_MODE_GET;
		optind = 2;
	} else if (strcmp(argv[1], "set") == 0) { /* SET MODE */
		if (argc < 3) {
			fprintf(stderr, "%s set: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "year") == 0) {
			mode = TIME_MODE_SET_YEAR;
			optind = 3;
		} else if (strcmp(argv[2], "month") == 0) {
			mode = TIME_MODE_SET_MONTH;
			optind = 3;
		} else if (strcmp(argv[2], "day") == 0) {
			mode = TIME_MODE_SET_DAY;
			optind = 3;
		} else if (strcmp(argv[2], "hour") == 0) {
			mode = TIME_MODE_SET_HOUR;
			optind = 3;
		} else if (strcmp(argv[2], "min") == 0) {
			mode = TIME_MODE_SET_MINUTE;
			optind = 3;
		} else if (strcmp(argv[2], "sec") == 0) {
			mode = TIME_MODE_SET_SECOND;
			optind = 3;
		} else if (strcmp(argv[2], "epoch") == 0) {
			mode = TIME_MODE_SET_EPOCH;
			optind = 3;
		} else {
			fprintf(stderr, "%s set: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "syr") == 0) { /* SET ALIASES */
		mode = TIME_MODE_SET_YEAR;
		optind = 2;
	} else if (strcmp(argv[1], "smon") == 0) {
		mode = TIME_MODE_SET_MONTH;
		optind = 2;
	} else if (strcmp(argv[1], "sday") == 0) {
		mode = TIME_MODE_SET_DAY;
		optind = 2;
	} else if (strcmp(argv[1], "shr") == 0) {
		mode = TIME_MODE_SET_HOUR;
		optind = 2;
	} else if (strcmp(argv[1], "smin") == 0) {
		mode = TIME_MODE_SET_MINUTE;
		optind = 2;
	} else if (strcmp(argv[1], "ssec") == 0) {
		mode = TIME_MODE_SET_SECOND;
		optind = 2;
	} else if (strcmp(argv[1], "se") == 0) {
		mode = TIME_MODE_SET_EPOCH;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	while ((choice = getopt(argc, argv, "-hf:")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			switch (mode) {
			case TIME_MODE_SET_YEAR:
			case TIME_MODE_SET_MONTH:
			case TIME_MODE_SET_DAY:
			case TIME_MODE_SET_HOUR:
			case TIME_MODE_SET_MINUTE:
			case TIME_MODE_SET_SECOND:
			case TIME_MODE_SET_EPOCH:
				if (timevalarg == NULL) {
					timevalarg = optarg;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			default:
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'f':
			if (strcmp(optarg, "iso") == 0) {
				fmt = TIME_DISPLAY_FMT_ISO;
			} else if (strcmp(optarg, "epoch") == 0) {
				fmt = TIME_DISPLAY_FMT_UNIXEPOCH;
			} else {
				fprintf(stderr, "%s: invalid display format \'%s\'\n", argv[0], optarg);
				return 1;
			}
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check that necessary values have been set
	switch (mode) {
	case TIME_MODE_SET_YEAR:
	case TIME_MODE_SET_MONTH:
	case TIME_MODE_SET_DAY:
	case TIME_MODE_SET_HOUR:
	case TIME_MODE_SET_MINUTE:
	case TIME_MODE_SET_SECOND:
	case TIME_MODE_SET_EPOCH:
		if (timevalarg == NULL) {
			fprintf(stderr, "%s: must specify a value to set\n\r", argv[0]);
			return 1;
		}
		break;
	default:
		break;
	}

	ret = 1;
	switch (mode) {
	case TIME_MODE_SET_YEAR:
	case TIME_MODE_SET_MONTH:
	case TIME_MODE_SET_DAY:
	case TIME_MODE_SET_HOUR:
	case TIME_MODE_SET_MINUTE:
	case TIME_MODE_SET_SECOND:
	case TIME_MODE_SET_EPOCH:
		ret = time_set_value(argv[0], timevalarg, mode);
		break;
	case TIME_MODE_GET:
		ret = print_current_time(argv[0], fmt);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Time functionality.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s get [-h] [-f format]\n\r", fname);
	fprintf(stderr, "       %s set (year|month|day|hour|min|sec|epoch) [-h] <value>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -f: Specify display format: [epoch,iso] (default: iso)\n\r");
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Mode aliases:\n\r");
	fprintf(stderr, "    s(yr|mon|day|hr|min|sec|e): set (year|month|day|hour|min|sec|epoch)\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Prints the current time.
 */
static int print_current_time(const char *fname, int fmt)
{
	int ret;
	unsigned int epochtime;
	Time t;

	switch (fmt) {
	case TIME_DISPLAY_FMT_ISO:
		ret = Time_get(&t);
		if (ret != 0) {
			fprintf(stderr, "%s: Time_get returned %d\n\r", fname, ret);
			return 1;
		}
		fprintf(stderr, "%u-%02d-%02d %02d:%02d:%02d\n\r", 2000 + t.year, t.month, t.date, t.hours, t.minutes, t.seconds);
		break;
	case TIME_DISPLAY_FMT_UNIXEPOCH:
		ret = Time_getUnixEpoch(&epochtime);
		if (ret != 0) {
			fprintf(stderr, "%s: Time_getUnixEpoch returned %d\n\r", fname, ret);
			return 1;
		}
		fprintf(stderr, "%u\n\r", epochtime);
		break;
	default:
		break;
	}

	return 0;
}

/**
 * Sets the value for the entered mode.
 */
static int time_set_value(const char *fname, const char *timeval, int mode)
{
	int ret;
	int conv;
	unsigned int epochtime;
	Time t;

	ret = Time_get(&t);
	if (ret != 0) {
		fprintf(stderr, "%s: Time_get returned %d\n\r", fname, ret);
		return 1;
	}

	ret = arghandler_to_int(timeval, &conv);
	if (ret != 0) {
		fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", fname, timeval);
		return 1;
	}

	switch (mode) {
	case TIME_MODE_SET_YEAR:
		if ((conv < 2000) || (conv > 2255)) {
			fprintf(stderr, "%s: year must be a value between 2000 and 2255\n\r", fname);
			return 1;
		}
		t.year = (unsigned char) (conv - 2000);
		break;
	case TIME_MODE_SET_MONTH:
		if ((conv < 1) || (conv > 12)) {
			fprintf(stderr, "%s: month must be a value between 1 and 12\n\r", fname);
			return 1;
		}
		t.month = (unsigned char) conv;
		break;
	case TIME_MODE_SET_DAY:
		if ((conv < 1) || (conv > 31)) {
			fprintf(stderr, "%s: day must be a value between 1 and 31\n\r", fname);
			return 1;
		}
		t.date = (unsigned char) conv;
		break;
	case TIME_MODE_SET_HOUR:
		if ((conv < 0) || (conv > 23)) {
			fprintf(stderr, "%s: hour must be a value between 0 and 23\n\r", fname);
			return 1;
		}
		t.hours = (unsigned char) conv;
		break;
	case TIME_MODE_SET_MINUTE:
		if ((conv < 0) || (conv > 59)) {
			fprintf(stderr, "%s: minute must be a value between 0 and 59\n\r", fname);
			return 1;
		}
		t.minutes = (unsigned char) conv;
		break;
	case TIME_MODE_SET_SECOND:
		if ((conv < 0) || (conv > 59)) {
			fprintf(stderr, "%s: second must be a value between 0 and 59\n\r", fname);
			return 1;
		}
		t.seconds = (unsigned char) conv;
		break;
	case TIME_MODE_SET_EPOCH:
		if (conv < UNIX_TIME_AT_Y2K) {
			fprintf(stderr, "%s: epoch time must be larger than %u\n\r", fname, UNIX_TIME_AT_Y2K);
			return 1;
		}
		epochtime = (unsigned int) conv;
		break;
	default:
		break;
	}

	switch (mode) {
	case TIME_MODE_SET_YEAR:
	case TIME_MODE_SET_MONTH:
	case TIME_MODE_SET_DAY:
	case TIME_MODE_SET_HOUR:
	case TIME_MODE_SET_MINUTE:
	case TIME_MODE_SET_SECOND:
		ret = Time_set(&t);
		if (ret != 0) {
			fprintf(stderr, "%s: Time_set returned %d\n\r", fname, ret);
			return 1;
		}
		break;
	case TIME_MODE_SET_EPOCH:
		ret = Time_setUnixEpoch(epochtime);
		if (ret != 0) {
			fprintf(stderr, "%s: Time_setUnixEpoch returned %d\n\r", fname, ret);
			return 1;
		}
		break;
	default:
		break;
	}

	return 0;
}
