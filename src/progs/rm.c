/**
 * @file   rm.c
 * @author John Wikman
 *
 * Remove a file from the filesystem.
 */

#include <ctype.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/serial_in.h>

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

int rm_main(int argc, char **argv)
{
	int ret;
	int choice;
	int ignore_prompt;
	F_FIND find;
	const char *filename = NULL;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	ignore_prompt = 0;
	filename = NULL;

	while ((choice = getopt(argc, argv, "-hf")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			if (filename == NULL) {
				filename = optarg;
			} else {
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'f':
			ignore_prompt = 1;
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	if (filename == NULL) {
		fprintf(stderr, "%s: missing filename\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	ret = f_findfirst(filename, &find);
	if (ret == F_ERR_NOTFOUND) {
		fprintf(stderr, "%s: file \'%s\' not found\n\r", argv[0], filename);
		return 1;
	} else if (ret != F_NO_ERROR) {
		fprintf(stderr, "%s: f_findfirst returned %d\n\r", argv[0], ret);
		return 1;
	}

	if (find.attr & F_ATTR_DIR) {
		fprintf(stderr, "%s: \'%s\' is a directory\n\r", argv[0], filename);
		return 1;
	}

	if (!ignore_prompt) {
		int len;
		char yes_no_buf[2];

		fprintf(stderr, "Are you sure that you want to remove the file \'%s\'? [y/N]\n\r", filename);
		fprintf(stderr, "(This could break features used in other projects)\n\r");
		len = serial_readline(yes_no_buf, sizeof(yes_no_buf));
		if (len != 1 || tolower((int) yes_no_buf[0]) != 'y') {
			// Did not receive an explicit yes, exiting...
			fprintf(stderr, "%s: \'%s\' was not removed\n\r", argv[0], filename);
			return 0;
		}
	}

	ret = f_delete(filename);
	if (ret == F_ERR_INVALIDNAME) {
		fprintf(stderr, "%s: illegal filename \'%s\'\n\r", argv[0], filename);
		return 1;
	} else if (ret != F_NO_ERROR) {
		fprintf(stderr, "%s: f_delete returned %d\n\r", argv[0], ret);
		return 1;
	}

	return 0;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Remove a file from the filesystem.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s [-hf] <file>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -f: Ignore confirmation prompt.\n\r");
	fprintf(stderr, "\n\r");
}
