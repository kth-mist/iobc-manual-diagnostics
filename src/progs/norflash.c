/**
 * @file   norflash.c
 * @author William Stackenäs
 *
 * Program for accessing the NorFlash.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <at91/boards/ISIS_OBC_G20/board.h>
#include <at91/memories/norflash/NorFlashApi.h>
#include <at91/memories/norflash/NorFlashCFI.h>

#include <obs-api/mist_storage_errors.h>
#include <obs-api/mist_norflash.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>

#define NORFLASH_MODE_NONE        (-1)
#define NORFLASH_MODE_READ_BYTES  (0x13)
#define NORFLASH_MODE_WRITE_BYTES (0x23)
#define NORFLASH_MODE_INFO        (0x30)

#define NORFLASH_FMT_HEXADECIMAL (2)
#define NORFLASH_FMT_HEXDUMP     (3)

#define NORFLASH_WRITE_BYTES_MAXLEN (256)

struct hexdump_info {
	unsigned int bytes_per_row;
	unsigned int startaddr;
	unsigned int addr;
};

// Global NorFlash device info
static struct mist_norflash _flash_dev;
static struct mist_norflash *_flash = NULL;

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static int norflash_read_bytes(const char *fname, int fmt, const char *addrarg, const char *lenarg);
static void output_hexdump_byte(unsigned char val, struct hexdump_info *info);
static int norflash_write_bytes(const char *fname, const char *addrarg, unsigned char *bytes, int len, int confirmed);
static int norflash_info(const char *fname);
static char *cfi_commandset_vendor(unsigned short code);


int norflash_main(int argc, char **argv)
{
	int ret;
	int val;
	int choice;
	int mode = NORFLASH_MODE_NONE;
	int fmt = NORFLASH_FMT_HEXADECIMAL;
	const char *addrarg = NULL;
	const char *lenarg = NULL;
	const char *valarg = NULL;
	int writebyteslen = 0;
	unsigned char writebytesarg[NORFLASH_WRITE_BYTES_MAXLEN];
	int confirmed = 0;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	if (argc < 2) {
		fprintf(stderr, "%s: missing mode\n\r", argv[0]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan and set the mode variable
	if (strcmp(argv[1], "read") == 0) { /* READ MODE */
		if (argc < 3) {
			fprintf(stderr, "%s read: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "bytes") == 0) {
			mode = NORFLASH_MODE_READ_BYTES;
			optind = 3;
		} else {
			fprintf(stderr, "%s read: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "rb") == 0) { /* READ ALIASES */
		mode = NORFLASH_MODE_READ_BYTES;
		optind = 2;
	} else if (strcmp(argv[1], "write") == 0) { /* WRITE MODE */
		if (argc < 3) {
			fprintf(stderr, "%s write: missing submode\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (strcmp(argv[2], "bytes") == 0) {
			mode = NORFLASH_MODE_WRITE_BYTES;
			optind = 3;
		} else {
			fprintf(stderr, "%s write: invalid submode \'%s\'\n\r", argv[0], argv[2]);
			print_usage(argv[0]);
			return 1;
		}
	} else if (strcmp(argv[1], "wb") == 0) {
		mode = NORFLASH_MODE_WRITE_BYTES;
		optind = 2;
	} else if (strcmp(argv[1], "info") == 0) { /* STATUS/INFO MODE */
		mode = NORFLASH_MODE_INFO;
		optind = 2;
	} else if (strcmp(argv[1], "-h") == 0) {
		print_help(argv[0]);
		return 0;
	} else {
		fprintf(stderr, "%s: unrecognized mode \'%s\'\n\r", argv[0], argv[1]);
		print_usage(argv[0]);
		return 1;
	}

	// Scan choices
	while ((choice = getopt(argc, argv, "-hXy")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			switch (mode) {
			case NORFLASH_MODE_READ_BYTES:
				if (addrarg == NULL) {
					addrarg = optarg;
				} else if (lenarg == NULL) {
					lenarg = optarg;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			case NORFLASH_MODE_WRITE_BYTES:
				if (addrarg == NULL) {
					addrarg = optarg;
				} else if (writebyteslen < NORFLASH_WRITE_BYTES_MAXLEN) {
					// scan the bytes to write to NorFlash
					ret = arghandler_to_int(optarg, &val);
					if (ret != 0) {
						fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", argv[0], optarg);
						return 1;
					}
					if ((val < 0) || (val > 255)) {
						fprintf(stderr, "%s: the value \'%s\' can not be contained within a byte\n\r", argv[0], optarg);
						return 1;
					}
					writebytesarg[writebyteslen++] = (unsigned char) val;
				} else {
					fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
					return 1;
				}
				break;
			default:
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'X':
			if (mode != NORFLASH_MODE_READ_BYTES) {
				fprintf(stderr, "%s: option \'-X\' is only valid for norflash read bytes\n\r", argv[0]);
				return 1;
			}
			fmt = NORFLASH_FMT_HEXDUMP;
			break;
		case 'y':
			if (mode != NORFLASH_MODE_WRITE_BYTES) {
				fprintf(stderr, "%s: option \'-y\' is only valid for norflash write bytes\n\r", argv[0]);
				return 1;
			}
			confirmed = 1;
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	// Check that required positionals are set
	switch (mode) {
	case NORFLASH_MODE_READ_BYTES:
		if (addrarg == NULL) {
			fprintf(stderr, "%s: must specify address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (lenarg == NULL) {
			fprintf(stderr, "%s: must specify length\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	case NORFLASH_MODE_WRITE_BYTES:
		if (addrarg == NULL) {
			fprintf(stderr, "%s: must specify address\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		if (writebyteslen < 1) {
			fprintf(stderr, "%s: must specify the values to write\n\r", argv[0]);
			print_usage(argv[0]);
			return 1;
		}
		break;
	default:
		break;
	}

	// Invoke corresponding functions
	ret = 1;
	switch (mode) {
	case NORFLASH_MODE_READ_BYTES:
		ret = norflash_read_bytes(argv[0], fmt, addrarg, lenarg);
		break;
	case NORFLASH_MODE_WRITE_BYTES:
		ret = norflash_write_bytes(argv[0], addrarg, writebytesarg, writebyteslen, confirmed);
		break;
	case NORFLASH_MODE_INFO:
		ret = norflash_info(argv[0]);
		break;
	default:
		DEBUGLN("unknown mode");
		break;
	}

	return ret;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: Access functionality for NorFlash.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s read bytes [-hX] <addr> <len>\n\r", fname);
	fprintf(stderr, "       %s write bytes [-hy] <addr> <values...>\n\r", fname);
	fprintf(stderr, "       %s info [-h]\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this help message.\n\r");
	fprintf(stderr, "    -X: Output bytes in the style of a hexdump.\n\r");
	fprintf(stderr, "    -y: Confirms that you are aware of the risks of writing to NorFlash.\n\r");
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Mode aliases:\n\r");
	fprintf(stderr, "    r(b): read (bytes)\n\r");
	fprintf(stderr, "    w(b): write (bytes)\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Reads bytes from the specified address.
 */
static int norflash_read_bytes(const char *fname, int fmt, const char *addrarg, const char *lenarg)
{
	int ret;
	int conv;
	uint32_t addr;
	uint8_t buf[128];
	uint32_t readlen;
	uint32_t len;
	struct hexdump_info fmtinfo;

	ret = arghandler_to_int(addrarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s read bytes: \'%s\' is not a valid NORFLASH address\n\r", fname, addrarg);
		return 1;
	}

	addr = (uint32_t) conv;

	ret = arghandler_to_int(lenarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s read bytes: \'%s\' is not a valid length\n\r", fname, lenarg);
		return 1;
	}

	len = (uint32_t) conv;
	if ((addr + len) > BOARD_NORFLASH_SIZE - 1) {
		fprintf(stderr, "%s read bytes: address and length is out-of-bounds\n\r", fname);
		return 1;
	}

	fmtinfo.bytes_per_row = 16;
	fmtinfo.startaddr = addr;
	fmtinfo.addr = addr;

	if (_flash == NULL) {
		ret = mist_norflash_start(&_flash_dev);
		if (ret != 0) {
			fprintf(stderr, "%s read bytes: mist_norflash_start returned %d\n\r", fname, ret);
			return 1;
		}
		_flash = &_flash_dev;
	}

	if (fmt != NORFLASH_FMT_HEXDUMP) {
		fprintf(stderr, "bytes@0x%X:", (unsigned int) addr);
	}

	readlen = 0;
	while (readlen < len) {
		unsigned int i;
		uint32_t bytes_to_read = len - readlen;
		if (bytes_to_read > sizeof(buf))
			bytes_to_read = sizeof(buf);

		ret = mist_norflash_read(_flash, addr + readlen, buf, bytes_to_read);
		if (ret != 0) {
			fprintf(stderr, "%s read bytes: mist_norflash_read returned %d\n\r", fname, ret);
			return 1;
		}

		for (i = 0; i < bytes_to_read; i++) {
			if (fmt == NORFLASH_FMT_HEXDUMP)
				output_hexdump_byte(buf[i], &fmtinfo);
			else if (fmt == NORFLASH_FMT_HEXADECIMAL)
				fprintf(stderr, " 0x%02X", buf[i]);
			else
				fprintf(stderr, " %u", buf[i]);
		}

		readlen += bytes_to_read;
	}

	fprintf(stderr, "\n\r");
	return 0;
}

/**
 * Outputs a byte according to hexadecimal format.
 */
static void output_hexdump_byte(unsigned char val, struct hexdump_info *info)
{
	// Make sure that bytes per row is not 0
	if (info->bytes_per_row == 0)
		info->bytes_per_row = 16;

	int rowpos = (info->addr - info->startaddr) % info->bytes_per_row;


	if (rowpos == 0) {
		if (info->addr != info->startaddr)
			fprintf(stderr, "\n\r");
		fprintf(stderr, "%07x", info->addr);
	}

	if ((rowpos & 1) == 0)
		fprintf(stderr, " ");

	fprintf(stderr, "%02x", val);

	(info->addr)++;
}

/**
 * Writes bytes to the specified address in NorFlash.
 */
static int norflash_write_bytes(const char *fname, const char *addrarg, unsigned char *bytes, int len, int confirmed)
{
	int ret;
	int conv;
	uint32_t addr;

	if (!confirmed) {
		fprintf(stderr, "%s write bytes: Are you sure? Note that when you write to NorFlash at an address\n\r", fname);
		fprintf(stderr, "%s write bytes: the entire sector around that address must be erased, which can lead to data loss.\n\r", fname);
		fprintf(stderr, "%s write bytes: If you know what you are doing, provide the '-y' option.\n\r", fname);
		return 1;
	}

	ret = arghandler_to_int(addrarg, &conv);
	if ((ret != 0) || (conv < 0)) {
		fprintf(stderr, "%s write bytes: \'%s\' is not a valid NorFlash address\n\r", fname, addrarg);
		return 1;
	}

	addr = (uint32_t) conv;

	if (_flash == NULL) {
		ret = mist_norflash_start(&_flash_dev);
		if (ret != 0) {
			fprintf(stderr, "%s write bytes: mist_norflash_start returned %d\n\r", fname, ret);
			return 1;
		}
		_flash = &_flash_dev;
	}

	ret = mist_norflash_write_and_verify(_flash, addr, (const uint8_t*) bytes, (uint32_t) len);
	if (ret == MIST_STORAGE_ERR_PARAM) {
		fprintf(stderr, "%s write bytes: address \'%s\' and length %d is out-of-bounds\n\r", fname, addrarg, len);
		return 1;
	} else if (ret != 0) {
		fprintf(stderr, "%s write bytes: mist_norflash_write_and_verify returned %d\n\r", fname, ret);
		return 1;
	}

	return 0;
}

/**
 * Display information about NorFlash.
 */
static int norflash_info(const char *fname)
{
	int ret;
	int i;
	struct NorFlashCfiQueryInfo *info;
	struct NorFlashCfiDeviceGeometry *geo;
	unsigned int device_size;
	unsigned int calculated_size = 0;
	char *command_set;

	if (_flash == NULL) {
		ret = mist_norflash_start(&_flash_dev);
		if (ret != 0) {
			fprintf(stderr, "%s info: mist_norflash_start returned %d\n\r", fname, ret);
			return 1;
		}
		_flash = &_flash_dev;
	}
	info = &_flash->cfi.norFlashInfo.cfiDescription.norFlashCfiQueryInfo;
	geo = &_flash->cfi.norFlashInfo.cfiDescription.norFlashCfiDeviceGeometry;
	device_size = NorFlash_GetDeviceSizeInBytes(&_flash->cfi.norFlashInfo);

	fprintf(stderr, "NorFlash Info\n\r");
	fprintf(stderr, " - Manufacturer ID:\t\t0x%08X\n\r",
	        NORFLASH_ReadManufactoryID(&_flash->cfi));
	fprintf(stderr, " - Device ID:\t\t\t0x%08X\n\r",
	        NORFLASH_ReadDeviceID(&_flash->cfi));
	fprintf(stderr, "Common Flash Interface (CFI):\n\r");
	fprintf(stderr, " - Base Address:\t\t\t0x%08X\n\r",
	        _flash->cfi.norFlashInfo.baseAddress);
	fprintf(stderr, " - Device Chip Width (bits):\t\t%d\n\r",
	        NorFlash_GetDataBusWidth(&_flash->cfi.norFlashInfo));
	fprintf(stderr, " - CFI compatible:\t\t\t%s\n\r",
	        _flash->cfi.norFlashInfo.cfiCompatible ? "Yes" : "No");

	fprintf(stderr, "CFI Query Info:\n\r");
	fprintf(stderr, " - Query Unique String:\t\t\t'%c%c%c'\n\r",
	        info->queryUniqueString[0],
			info->queryUniqueString[1],
			info->queryUniqueString[2]);
	fprintf(stderr, " - Primary Vendor Command Set:\t\t0x%04X (%s)\n\r",
	        info->primaryCode,
	        cfi_commandset_vendor(info->primaryCode));
	fprintf(stderr, " - Primary Query Table Address:\t\t0x%04X\n\r", info->primaryAddr);
	fprintf(stderr, " - Alternate Vendor Command Set:\t0x%04X (%s)\n\r",
	        info->alternateCode,
	        cfi_commandset_vendor(info->alternateCode));
	fprintf(stderr, " - Alternate Query Table Address:\t0x%04X\n\r", info->alternateAddr);
	fprintf(stderr, " - Minimum Write/Erase Vcc voltage (V):\t%d\n\r", info->minVcc);
	fprintf(stderr, " - Maximum Write/Erase Vcc voltage (V):\t%d\n\r", info->maxVcc);
	fprintf(stderr, " - Minimum Write/Erase Vpp voltage (V):\t%d\n\r", info->minVpp);
	fprintf(stderr, " - Maximum Write/Erase Vpp voltage (V):\t%d\n\r", info->maxVpp);
	fprintf(stderr, " - Minimum Single Write Timeout (us):\t%d\n\r", 1 << info->minTimeOutWrite);
	fprintf(stderr, " - Minimum Buffer Write Timeout (us):\t%d\n\r", 1 << info->minTimeOutBuffer);
	fprintf(stderr, " - Minimum Block Erase Timeout (us):\t%d\n\r", 1 << info->minTimeOutBlockErase);
	fprintf(stderr, " - Minimum Chip Erase Timeout (us):\t%d\n\r", 1 << info->minTimeOutChipErase);
	fprintf(stderr, " - Maximum Single Write Timeout (us):\t%d\n\r", 1 << info->maxTimeOutWrite);
	fprintf(stderr, " - Maximum Buffer Write Timeout (us):\t%d\n\r", 1 << info->maxTimeOutBuffer);
	fprintf(stderr, " - Maximum Block Erase Timeout (us):\t%d\n\r", 1 << info->maxTimeOutBlockErase);
	fprintf(stderr, " - Maximum Chip Erase Timeout (us):\t%d\n\r", 1 << info->maxTimeOutChipErase);

	fprintf(stderr, "CFI Device Geometry:\n\r");
	fprintf(stderr, " - Device Size (bytes):\t\t\t0x%08X\n\r", device_size);
	fprintf(stderr, " - Flash Device Interface Desicrption:\t0x%04X\n\r", geo->deviceInterface);
	fprintf(stderr, " - Maximum Multi-byte Write:\t\t%d\n\r", geo->numMultiWrite);
	fprintf(stderr, " - Number Of Erase Regions:\t\t%d\n\r", geo->numEraseRegion);
	for (i = 0; i < geo->numEraseRegion; i++) {
		fprintf(stderr, "\t - Region %d Block Count:\t%d\n\r", i, geo->eraseRegionInfo[i].Y + 1);
		fprintf(stderr, "\t - Region %d Block Size (bytes):\t0x%04X\n\r", i, geo->eraseRegionInfo[i].Z << 8);

		calculated_size += (geo->eraseRegionInfo[i].Y + 1) * (geo->eraseRegionInfo[i].Z << 8);
	}

	if (calculated_size != device_size) {
		fprintf(stderr, "Warning: Total size of erase region(s) (0x%08X) do not equal reported device size (0x%08X)\r\n",
		        calculated_size,
		        device_size);
	}

	return 0;
}

static char *cfi_commandset_vendor(unsigned short code)
{
	switch (code) {
	case CMD_SET_NULL:
		return "None";
	case CMD_SET_INTEL_EXT:
		return "Intel Ext";
	case CMD_SET_AMD:
		return "AMD";
	case CMD_SET_INTEL:
		return "Intel";
	case CMD_SET_AMD_EXT:
		return "AMD Ext";
	case CMD_SET_MISUBISHI:
		return "Mitsubishi";
	case CMD_SET_MISUBISHI_EXT:
		return "Mitsubishi Ext";
	case CMD_SET_SST:
		return "SST";
	}

	return "Unknown";
}

