/**
 * @file   shell.c
 * @author John Wikman
 * @author William Stackenäs
 */

#include <stdio.h>
#include <string.h>
#include <getopt.h>

#include "progs.h"

struct iobc_program {
	const char *name;
	int (*main_func)(int, char **);
};

static void reset_getopt(void);

int help_main(int argc, char **argv);
int exit_main(int argc, char **argv);

static int _exit = 0;

/**
 * The list of available programs and their main functions. Must always end
 * with a {NULL, NULL} entry.
 */
static struct iobc_program prognames[] = {
	// "exit" and "help" are local special-case programs
	{"exit",    exit_main},
	{"help",    help_main},
	// actual programs: (make sure this is sorted alphabetically)
	{"cd",      cd_main},
	{"eps",     eps_main},
	{"norflash",norflash_main},
	{"fram",    fram_main},
	{"fsinfo",  fsinfo_main},
	{"hexdump", hexdump_main},
	{"i2c",     i2c_main},
	{"led",     led_main},
	{"ls",      ls_main},
	{"mkdir",   mkdir_main},
	{"more",    more_main},
	{"msp",     msp_prog_main},
	{"rm",      rm_main},
	{"rmdir",   rmdir_main},
	{"time",    time_main},
	{"tmq",     tmq_main},
	{"tmtc",    tmtc_prog_main},
	{"trxvu",   trxvu_main},
	{NULL,      NULL}
};

int iobc_manual_diagnostics_exec(int argc, char **argv, int *exit)
{
	int ret = 127;
	struct iobc_program *prog;

	_exit = 0;

	/* Reset option index for argument parsing in programs */
	reset_getopt();

	// Find the appropriate program to launch
	for (prog = &prognames[0]; prog->name != NULL; prog++) {
		if (strcmp(argv[0], prog->name) == 0) {
			ret = prog->main_func(argc, argv);
			break;
		}
	}

	if (prog->name == NULL) {
		fprintf(stderr, "Unrecognized program: \"%s\"\n\r", argv[0]);
		fprintf(stderr, "Type \"help\" for a list of available programs.\n\r");
	}
	*exit = _exit;

	return ret;
}

/**
 * Resets the getopt functionality (as the current)
 */
static void reset_getopt(void)
{
	int argc = 2;
	char * const argv[] = {"main", "h"};

	optind = 1;

	int choice;
	while ((choice = getopt(argc, argv, "-h")) != -1);

	optind = 1;
}

/**
 * Prints out the names of the available programs.
 */
int help_main(int argc, char **argv)
{
	int i;
	int j;
	int rows;
	const int cols = 4;
	int max_namelen = 0;
	int numprogs = 0;
	struct iobc_program *prog = &prognames[0];

	if (argc >= 2) {
		if (strcmp(argv[1], "-h") == 0) {
			fprintf(stderr, "%s: Prints a list the available programs.\n\r", argv[0]);
			fprintf(stderr, "\n\r");
			fprintf(stderr, "usage: %s [-h]\n\r", argv[0]);
			return 0;
		}
	}

	for (prog = &prognames[0]; prog->name != NULL; prog++) {
		int len = (int) strlen(prog->name);
		if (len > max_namelen)
			max_namelen = len;

		numprogs++;
	}

	rows = (numprogs + cols - 1) / cols;

	fprintf(stderr, "Available programs:\n\r");
	for (i = 0; i < rows; i++) {
		fprintf(stderr, "   ");
		for (j = 0; j < cols && ((j * rows) + i) < numprogs; j++) {
			const char *name = prognames[(j * rows) + i].name;
			int len = strlen(name);

			fprintf(stderr, " %s", name);
			if ((j + 1) != cols)
				fprintf(stderr, "%*s", max_namelen - len + 1, "");
		}

		fprintf(stderr, "\n\r");
	}

	fprintf(stderr, "\n\r");
	fprintf(stderr, "Run \"<program name> -h\" for more information about that program.\n\r");

	return 0;
}

/**
 * Program that signals to the caller that the shell was gracefully exited.
 */
int exit_main(int argc, char **argv)
{
	if (argc >= 2) {
		if (strcmp(argv[1], "-h") == 0) {
			fprintf(stderr, "%s: Gracefully exits the shell.\n\r", argv[0]);
			fprintf(stderr, "\n\r");
			fprintf(stderr, "usage: %s [-h]\n\r", argv[0]);
			return 0;
		}
	}

	_exit = 1;

	return 0;
}
