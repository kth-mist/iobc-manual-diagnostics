/**
 * @file   ls.c
 * @author John Wikman
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>

// Define all static functions here
static void print_help(const char *fname);
static void print_usage(const char *fname);

static void print_item_name(F_FIND *find);
static void print_list_item(F_FIND *find);

static const char *default_ls_string = "*.*";

int ls_main(int argc, char **argv)
{
	int ret;
	int choice;
	int include_hidden;
	int show_as_list;
	const char *lsstr = NULL;
	int is_first_item;
	F_FIND find;

	if (argc < 1 || argv == NULL) {
		fprintf(stderr, "Invalid argc or argv parameter.\n\r");
		return 1;
	}

	include_hidden = 0;
	show_as_list = 0;
	lsstr = NULL;

	while ((choice = getopt(argc, argv, "-hal")) != -1) {
		switch (choice) {
		case 1: /* Positional */
			if (lsstr == NULL) {
				lsstr = optarg;
			} else {
				fprintf(stderr, "%s: unexpected positional argument \'%s\'\n\r", argv[0], optarg);
				return 1;
			}
			break;
		case 'h':
			print_help(argv[0]);
			return 0;
		case 'a':
			include_hidden = 1;
			break;
		case 'l':
			show_as_list = 1;
			break;
		default: /* '?' */
			print_usage(argv[0]);
			return 1;
		}
	}

	if (lsstr == NULL) {
		lsstr = default_ls_string;
	}

	ret = f_findfirst(lsstr, &find);
	if (ret != F_NO_ERROR) {
		fprintf(stderr, "%s: f_findfirst returned %d\n\r", argv[0], ret);
		return 1;
	}

	if (find.attr & F_ATTR_DIR) {
		// Check if we listed a directory without a wildcard
		const char *checkname = strrchr(lsstr, '/');
		if (checkname != NULL) {
			checkname++;
		} else {
			checkname = lsstr;
		}

		if (strcasecmp(checkname, find.filename) == 0) {
			// wrote an exact name for a directory, list its contents instead

			// ... but first save our return point
			char retdir[F_MAXPATHNAME + 1];
			ret = f_getcwd(retdir, sizeof(retdir));
			if (ret != F_NO_ERROR) {
				DEBUGLN_ARGS("f_getcwd returned %d", ret);
				return 1;
			}
			retdir[sizeof(retdir) - 1] = '\0';

			ret = f_chdir(lsstr);
			if (ret != F_NO_ERROR) {
				DEBUGLN_ARGS("f_chdir returned %d", ret);
				return 1;
			}

			ret = f_findfirst("*.*", &find);
			if (ret != F_NO_ERROR) {
				DEBUGLN_ARGS("f_findfirst returned %d", ret);
				return 1;
			}

			ret = f_chdir(retdir);
			if (ret != F_NO_ERROR) {
				DEBUGLN_ARGS("f_chdir returned %d", ret);
				return 1;
			}
		}
	}

	is_first_item = 1;

	do {
		if (include_hidden || (find.filename[0] != '.')) {
			if (is_first_item) {
				is_first_item = 0;
			} else {
				if (show_as_list)
					fprintf(stderr, "\n\r");
				else
					fprintf(stderr, "\t");
			}

			if (show_as_list)
				print_list_item(&find);
			else
				print_item_name(&find);
		}

		ret = f_findnext(&find);
	} while (ret == F_NO_ERROR);

	// If we have printed at least one item we must also print a newline that
	// ends it.
	if (!is_first_item)
		fprintf(stderr, "\n\r");

	return 0;
}

static void print_help(const char *fname)
{
	fprintf(stderr, "%s: List the contents of a directory.\n\r", fname);
	print_usage(fname);
}

static void print_usage(const char *fname)
{
	fprintf(stderr, "\n\r");
	fprintf(stderr, "usage: %s [-hal] <dir>\n\r", fname);
	fprintf(stderr, "\n\r");
	fprintf(stderr, "Available options:\n\r");
	fprintf(stderr, "    -h: Prints this message.\n\r");
	fprintf(stderr, "    -a: Includes hidden files in the listing.\n\r");
	fprintf(stderr, "    -l: Display output as a list.\n\r");
	fprintf(stderr, "\n\r");
}

/**
 * Prints just the filename of the found item.
 */
static void print_item_name(F_FIND *find)
{
	fprintf(stderr, "%s", find->filename);
	if (find->attr & F_ATTR_DIR)
		fprintf(stderr, "/");
}

/**
 * Prints the found item as a listed entry.
 */
static void print_list_item(F_FIND *find)
{
	unsigned short year = 1980 + ((find->cdate & F_CDATE_YEAR_MASK) >> F_CDATE_YEAR_SHIFT);
	unsigned short month = (find->cdate & F_CDATE_MONTH_MASK) >> F_CDATE_MONTH_SHIFT;
	unsigned short day = (find->cdate & F_CDATE_DAY_MASK) >> F_CDATE_DAY_SHIFT;
	unsigned short hour = (find->ctime & F_CTIME_HOUR_MASK) >> F_CTIME_HOUR_SHIFT;
	unsigned short minute = (find->ctime & F_CTIME_MIN_MASK) >> F_CTIME_MIN_SHIFT;

	fprintf(stderr, "%8lu %04u-%02u-%02u %02u:%02u %s", find->filesize, year, month, day, hour, minute, find->filename);
	if (find->attr & F_ATTR_DIR)
		fprintf(stderr, "/");
}
