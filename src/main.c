#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>

#include <hal/Timing/WatchDogTimer.h>
#include <hal/Timing/Time.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>
#include <hcc/api_hcc_mem.h>
#include <hcc/api_mdriver_atmel_mcipdc.h>

#include <at91/utility/trace.h>
#include <at91/peripherals/cp15/cp15.h>
#include <at91/utility/exithandler.h>
#include <at91/commons.h>

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <iobc-manual-diagnostics/shell.h>
#include <mist-tools/arghandler.h>
#include <mist-tools/debug.h>
#include <mist-tools/serial_in.h>

#include "tools/fs_setup.h"
#include "tools/fram_setup.h"
#include "tools/i2c_setup.h"
#include "tools/led_setup.h"
#include "tools/time_setup.h"

#define ENABLE_MAIN_TRACES 1
#if ENABLE_MAIN_TRACES
	#define MAIN_TRACE_INFO			TRACE_INFO
	#define MAIN_TRACE_DEBUG		TRACE_DEBUG
	#define MAIN_TRACE_WARNING		TRACE_WARNING
	#define MAIN_TRACE_ERROR		TRACE_ERROR
	#define MAIN_TRACE_FATAL		TRACE_FATAL
#else
	#define MAIN_TRACE_INFO(...)	{ }
	#define MAIN_TRACE_DEBUG(...)	{ }
	#define MAIN_TRACE_WARNING(...)	{ }
	#define MAIN_TRACE_ERROR		TRACE_ERROR
	#define MAIN_TRACE_FATAL		TRACE_FATAL
#endif

void main_task(void *args);
void main_prompt_loop(void);

int main()
{
	xTaskHandle select_handle;

	TRACE_CONFIGURE_ISP(DBGU_STANDARD, 2000000, BOARD_MCK);
	// Enable the Instruction cache of the ARM9 core. Keep the MMU and Data Cache disabled.
	CP15_Enable_I_Cache();

	fprintf(stderr, "\n\r -- iOBC Manual Diagnostics --\n\r");
#ifdef __OPTIMIZE__
	fprintf(stderr, "\n\r -- Compiled on  %s %s in release mode --\n\r", __DATE__, __TIME__);
#else
	fprintf(stderr, "\n\r -- Compiled on  %s %s in debug mode --\n\r", __DATE__, __TIME__);
#endif

	// The actual watchdog is already started, this only initializes the watchdog-kick interface.
	//WDT_start();

	fprintf(stderr, "\t main: Creating tasks... \n\r");
	xTaskCreate(main_task, (const signed char * const) "main_task", 1024, NULL, configMAX_PRIORITIES-2, &select_handle);

	fprintf(stderr, "\t main: Starting scheduler... \n\r");
	vTaskStartScheduler();

	// This part should never be reached.
	fprintf(stderr, "\t main: THIS SHOULD NEVER BE REACHED. \n\r");
	while(1)
	{
		fprintf(stderr, "main: Still alive\n\r");
	}

	return 0;
}

void main_task(void *args)
{
	int ret;
	char buf[2];

	(void) args;

	for (;;) {
		fprintf(stderr, "Press ENTER to start.\n\r");
		ret = serial_readline(buf, sizeof(buf));
		if (ret != 0)
			continue;

		led_setup_init();

		ret = time_setup_init();
		if (ret != 0)
			continue;

		ret = i2c_setup_init();
		if (ret != 0)
			continue;

		ret = fs_setup_init();
		if (ret != 0) {
			i2c_setup_deinit();
			continue;
		}

		ret = fram_setup_init();
		if (ret != 0) {
			i2c_setup_deinit();
			fs_setup_deinit();
			continue;
		}

		main_prompt_loop();

		fram_setup_deinit();
		fs_setup_deinit();
		i2c_setup_deinit();
	}
}

static int _exit;

/**
 * Main loop that reads commands and launches programs.
 */
void main_prompt_loop(void)
{
	int ret;
	int len;
	int argc;
	int last_err;
	// There are made static due to:
	//   1: save stack space, and
	//   2: they are never going to get deallocated.
	static char buf[F_MAXPATHNAME];
	static char *argv[30];

	fprintf(stderr, "Type \"help\" for a list of available programs.\n\r");

	_exit = 0;
	last_err = 0;
	while (!_exit) {
		// Format and print PS1
		ret = f_getcwd(buf, sizeof(buf));
		if (ret != F_NO_ERROR) {
			DEBUGLN_ARGS("fatal: f_getcwd returned %d", ret);
			return;
		}
		if (last_err) {
			// Print a BANG to indicate error on last func call
			fprintf(stderr, "!");
			last_err = 0;
		}
		buf[sizeof(buf) - 1] = '\0';
		fprintf(stderr, "%s$ ", buf);

		// Read command
		len = serial_buffered_readline(buf, sizeof(buf));
		if (len == 0) {
			// no command from input
			continue;
		}

		argc = arghandler_split_args(buf, argv, sizeof(argv) / sizeof(argv[0]));
		if (argc == 0) {
			// No arguments
			continue;
		}

		last_err = iobc_manual_diagnostics_exec(argc, argv, &_exit);
	}
}
