/**
 * @file   i2c_setup.h
 * @author John Wikman
 */

#ifndef TOOLS_I2C_SETUP_H
#define TOOLS_I2C_SETUP_H

int i2c_setup_init();
void i2c_setup_deinit();

#endif /* TOOLS_I2C_SETUP_H */
