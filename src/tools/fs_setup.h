/**
 * @file   fs_setup.h
 * @author John Wikman
 */

#ifndef TOOLS_FS_SETUP_H
#define TOOLS_FS_SETUP_H

int fs_setup_init();
void fs_setup_deinit();

#endif /* TOOLS_FS_SETUP_H */
