/**
 * @file   time_setup.c
 * @author John Wikman
 */

#include <stdio.h>

#include <hal/Timing/Time.h>

static int is_set_up = 0;

/**
 * Initializes the FRAM.
 */
int time_setup_init()
{
	int ret;
	Time t;

	if (is_set_up)
		return 0;

	fprintf(stderr, "Initializing Time.\n\r");

	t.seconds = 0;
	t.minutes = 0;
	t.hours = 0;
	t.day = 1;
	t.date = 1;
	t.month = 10;
	t.year = 19;
	// Set time to 2019-10-01 00:00 initially

	ret = 2;
	while ((ret == 2) && (t.day <= 7)) {
		// Try and see which week day it will accept
		// NOTE: This does not seem to care whether the specified day is
		//       consistent with reality.
		ret = Time_start(&t, 60); // sync every 60 seconds;
		if (ret == 0) {
			is_set_up = 1;
			return 0;
		}

		t.day += 1;
	}

	fprintf(stderr, "Time_start returned %d\n\r", ret);

	return 1;
}
