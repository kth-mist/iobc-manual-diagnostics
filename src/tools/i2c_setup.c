/**
 * @file   i2c_setup.c
 * @author John Wikman
 */

#include <stdio.h>

#include <hal/Drivers/I2C.h>

static int is_set_up = 0;

/**
 * Initializes I2C.
 */
int i2c_setup_init()
{
	int ret;

	if (is_set_up) {
		fprintf(stderr, "error: I2C already initialized\n\r");
		return 1;
	}

	fprintf(stderr, "Initializing I2C.\n\r");
	ret = I2C_start(400000, 1000); // clock: 400 kHz
	if (ret != 0) {
		fprintf(stderr, "error: I2C_start returned %d\n\r", ret);
		return 1;
	}

	is_set_up = 1;

	return 0;
}

/**
 * De-initializes/stops I2C.
 */
void i2c_setup_deinit()
{
	if (!is_set_up) {
		fprintf(stderr, "error: I2C is not initialized\n\r");
		return;
	}

	fprintf(stderr, "De-initializing I2C.\n\r");

	I2C_stop();

	is_set_up = 0;
}
