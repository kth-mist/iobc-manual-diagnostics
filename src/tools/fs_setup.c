/**
 * @file   fs_setup.c
 * @author John Wikman
 *
 * Functions for initializing and de-initializing the filesystem.
 */

#include <ctype.h>
#include <stdio.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>
#include <hcc/api_hcc_mem.h>
#include <hcc/api_mdriver_atmel_mcipdc.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/serial_in.h>

static int is_set_up = 0;
static int sdcard_id = 0;

/**
 * Initializes the filesystem and formats the SD-card if necessary.
 */
int fs_setup_init()
{
	int ret;
	int len;
	char yes_no_buf[2];

	if (is_set_up) {
		fprintf(stderr, "error: Filesystem already initialized\n\r");
		return 1;
	}

	fprintf(stderr, "Initializing filesystem.\n\r");
	fprintf(stderr, "Choose SD-Card (0 or 1): ");

	ret = serial_readint(&sdcard_id);
	if (ret != 0) {
		fprintf(stderr, "error: Must enter a number.\n\r");
		return 1;
	}

	if (sdcard_id != 0 && sdcard_id != 1) {
		fprintf(stderr, "error: Invalid selection: %d\n\r", sdcard_id);
		return 1;
	}

	/* Initialize HCC memory and file system */
	hcc_mem_init();

	ret = fs_init();
	if (ret != F_NO_ERROR) {
		fprintf(stderr, "error: fs_init returned: %d\n\r", ret);
		hcc_mem_delete();
		return 1;
	}

	ret = f_enterFS();
	if (ret != F_NO_ERROR) {
		fprintf(stderr, "error: f_enterFS returned: %d\n\r", ret);
		fs_delete();
		hcc_mem_delete();
		return 1;
	}

	ret = f_initvolume(0, atmel_mcipdc_initfunc, sdcard_id);
	if (ret == F_ERR_NOTFORMATTED) {
		fprintf(stderr, "Volume on SD-Card %d is not formatted. Would you like to format it? [y/N] ", sdcard_id);
		len = serial_readline(yes_no_buf, sizeof(yes_no_buf));
		if (len != 1 || tolower((int) yes_no_buf[0]) != 'y') {
			/* Exit without formatting volume. */
			fprintf(stderr, "error: Not formatting volume, exiting...\n\r");
			f_releaseFS();
			fs_delete();
			hcc_mem_delete();
			return 1;
		}
		/* Now format the volume */
		ret = f_format(0, F_FAT32_MEDIA);
		if (ret != F_NO_ERROR) {
			fprintf(stderr, "error: f_format returned: %d\n\r", sdcard_id);
			f_releaseFS();
			fs_delete();
			hcc_mem_delete();
			return 1;
		}
	} else if (ret != F_NO_ERROR) {
		fprintf(stderr, "error: f_initvolume returned: %d\n\r", sdcard_id);
		f_releaseFS();
		fs_delete();
		hcc_mem_delete();
		return 1;
	}

	is_set_up = 1;

	return 0;
}

/**
 * De-initializes the filesystem.
 */
void fs_setup_deinit()
{
	if (!is_set_up) {
		fprintf(stderr, "error: Filesystem is not initialized\n\r");
		return;
	}

	fprintf(stderr, "De-initializing filesystem.\n\r");

	/* Delete the volume */
	f_delvolume(sdcard_id);

	/* Release the file system from this task */
	f_releaseFS();

	/* Delete the file system */
	fs_delete();

	/* Free the HCC memory */
	hcc_mem_delete();

	is_set_up = 0;
}
