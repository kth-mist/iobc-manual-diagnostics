/**
 * @file   fram_setup.c
 * @author John Wikman
 */

#include <stdio.h>

#include <hal/Storage/FRAM.h>

static int is_set_up = 0;

/**
 * Initializes the FRAM.
 */
int fram_setup_init()
{
	int ret;

	if (is_set_up) {
		fprintf(stderr, "error: FRAM already initialized\n\r");
		return 1;
	}

	fprintf(stderr, "Initializing FRAM.\n\r");
	ret = FRAM_start();
	if (ret != 0) {
		fprintf(stderr, "error: FRAM_start returned %d\n\r", ret);
		return 1;
	}

	is_set_up = 1;

	return 0;
}

/**
 * De-initializes the FRAM.
 */
void fram_setup_deinit()
{
	if (!is_set_up) {
		fprintf(stderr, "error: FRAM is not initialized\n\r");
		return;
	}

	fprintf(stderr, "De-initializing FRAM.\n\r");

	FRAM_stop();

	is_set_up = 0;
}
