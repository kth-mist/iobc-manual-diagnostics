/**
 * @file   time_setup.h
 * @author John Wikman
 */

#ifndef TOOLS_TIME_SETUP_H
#define TOOLS_TIME_SETUP_H

int time_setup_init();

#endif /* TOOLS_TIME_SETUP_H */
