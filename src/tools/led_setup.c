/**
 * @file   led_setup.c
 * @author John Wikman
 */

#include <stdio.h>

#include <hal/Drivers/LED.h>

static int is_set_up = 0;

/**
 * Initializes the LEDs.
 */
void led_setup_init()
{
	if (is_set_up)
		return;

	fprintf(stderr, "Initializing LEDs.\n\r");
	LED_start();
	is_set_up = 1;
}
