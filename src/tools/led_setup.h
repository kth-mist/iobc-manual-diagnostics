/**
 * @file   led_setup.h
 * @author John Wikman
 */

#ifndef TOOLS_LED_SETUP_H
#define TOOLS_LED_SETUP_H

void led_setup_init();

#endif /* TOOLS_LED_SETUP_H */
