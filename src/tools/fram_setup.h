/**
 * @file   fram_setup.h
 * @author John Wikman
 */

#ifndef TOOLS_FRAM_SETUP_H
#define TOOLS_FRAM_SETUP_H

int fram_setup_init();
void fram_setup_deinit();

#endif /* TOOLS_FRAM_SETUP_H */
