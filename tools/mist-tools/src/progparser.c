/**
 * @file   progparser.c
 * @author John Wikman
 */

#include <ctype.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/progparser.h>

/**
 * Attempt to parse as a byte. Returns 0 on success. Returns 1 on error.
 */
int progparser_byte(const char *fname, const char *arg, uint8_t *dst)
{
	int ret;
	int value;

	if ((fname == NULL) || (arg == NULL)) {
		fprintf(stderr, "internal error: NULL argument\n\r");
		return 1;
	}

	ret = arghandler_to_int(arg, &value);
	if (ret != 0) {
		fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", fname, arg);
		return 1;
	}
	if ((value < 0) || (value > 0xFF)) {
		fprintf(stderr, "%s: \'%s\' does not fit within a single byte\n\r", fname, arg);
		return 1;
	}

	*dst = (uint8_t) value;
	return 0;
}

/**
 * Attempt to parse as an I2C address. Returns 0 on success. Returns 1 on error.
 */
int progparser_i2caddr(const char *fname, const char *arg, uint8_t *dst)
{
	int ret;
	int value;

	if ((fname == NULL) || (arg == NULL)) {
		fprintf(stderr, "internal error: NULL argument\n\r");
		return 1;
	}

	ret = arghandler_to_int(arg, &value);
	if (ret != 0) {
		fprintf(stderr, "%s: \'%s\' is not a recognized numerical value\n\r", fname, arg);
		return 1;
	}

	if ((value < 0x01) || (value > 0x7F)) {
		fprintf(stderr, "%s: \'%s\' is not a valid I2C address\n\r", fname, arg);
		return 1;
	}

	*dst = (uint8_t) value;
	return 0;
}
