/**
 * @file   serial_in.c
 * @author John Wikman
 */

#include <at91/peripherals/dbgu/dbgu.h>

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <mist-tools/arghandler.h>
#include <mist-tools/serial_in.h>

#define NUM_BUFFERS (32)
#define BUFLEN      (127)

struct input_buffer {
	int pos;
	int len;
	char buf[BUFLEN + 1];
};

/**
 * Reads a line from serial into a local set of buffers. This memorizes
 * invocations and allows for the user to navigate the current input space
 * using the arrows.
 *
 * Very much NOT thread-safe.
 */
int serial_buffered_readline(char *dest, int destsize)
{
	int i;
	int is_finished = 0;
	int current;
	int earliest_buffer;
	unsigned char c;

	static struct input_buffer bufs[NUM_BUFFERS];
	static int latest_buffer = NUM_BUFFERS - 1;
	static int available_buffers = 0;

	if (dest == NULL || destsize < 1)
		return 0;

	latest_buffer = (latest_buffer + 1) % NUM_BUFFERS;

	if (available_buffers < NUM_BUFFERS)
		available_buffers++;

	current = latest_buffer;
	earliest_buffer = (NUM_BUFFERS + latest_buffer - (available_buffers - 1)) % NUM_BUFFERS;

	bufs[current].pos = 0;
	bufs[current].len = 0;
	bufs[current].buf[0] = '\0';

	if (earliest_buffer != latest_buffer) {
		i = latest_buffer;
		do {
			i = (i + 1) % NUM_BUFFERS;

			bufs[i].pos = bufs[i].len;
			bufs[i].buf[bufs[i].len] = '\0';
		} while (i != earliest_buffer);
	}

	// ESC + '[' + 'A' == arrow-up
	// ESC + '[' + 'B' == arrow-down
	// ESC + '[' + 'C' == arrow-right
	// ESC + '[' + 'D' == arrow-left

	while (!is_finished) {
		/* wait until we have a char to read */
		while (DBGU_IsRxReady() == 0);

		c = DBGU_GetChar();
		if (c == '\r')
			is_finished = 1;

		if (c == 0x7f || c == 0x08) { /* 0x7f = DEL, 0x08 = backspace */
			if (bufs[current].pos > 0) {
				int pos = bufs[current].pos;
				DBGU_PutChar(0x7f);
				while (pos < bufs[current].len) {
					bufs[current].buf[pos - 1] = bufs[current].buf[pos];
					DBGU_PutChar(bufs[current].buf[pos]);
					pos++;
				}
				DBGU_PutChar(' ');
				bufs[current].pos--;
				bufs[current].len--;

				int backstep_count = pos - bufs[current].pos;
				if (backstep_count > 0) {
					fprintf(stderr, "\033[%dD", backstep_count);
				}
			}
		} else if (c == 0x1B) { /* 0x1B == ESC CHAR (\033) */
			char escbuf[11];
			int esclen = 0;

			// This is causes too much trouble...
			//while (esclen < 10) {
			//	while (DBGU_IsRxReady() == 0);
			//	c = DBGU_GetChar();
			//	escbuf[esclen++] = c;
			//
			//	if ((c >= 'A' && c <= 'Z') ||
			//		(c >= 'a' && c <= 'z')) {
			//		break;
			//	}
			//}

			escbuf[0] = serial_getkeypress();

			if (escbuf[0] == '[') {
				escbuf[1] = serial_getkeypress();
				esclen = 2;
			}

			escbuf[esclen] = '\0';

			if (strcmp(escbuf, "[A") == 0) {
				// Up-Arrow: Change to a previous buffer (one step away from the current buffer)
				if (current != earliest_buffer) {
					// Go to the end of current line and erase it from terminal
					int diff = bufs[current].len - bufs[current].pos;
					if (diff > 0) {
						fprintf(stderr, "\033[%dC", diff);
					}

					for (i = 0; i < bufs[current].len; i++)
						DBGU_PutChar(0x7F);

					current = (NUM_BUFFERS + current - 1) % NUM_BUFFERS;

					bufs[current].pos = bufs[current].len;
					bufs[current].buf[bufs[current].len] = '\0';
					fprintf(stderr, "%s", bufs[current].buf);
				}
			} else if (strcmp(escbuf, "[B") == 0) {
				// Down-Arrow: Change to a more "recent" buffer (one step towards the current buffer)
				if (current != latest_buffer) {
					// Go to the end of current line and erase it from terminal
					int diff = bufs[current].len - bufs[current].pos;
					if (diff > 0) {
						fprintf(stderr, "\033[%dC", diff);
					}

					for (i = 0; i < bufs[current].len; i++)
						DBGU_PutChar(0x7F);

					current = (current + 1) % NUM_BUFFERS;

					bufs[current].pos = bufs[current].len;
					bufs[current].buf[bufs[current].len] = '\0';
					fprintf(stderr, "%s", bufs[current].buf);
				}
			} else if (strcmp(escbuf, "[C") == 0) {
				// Right-Arrow: Move position one step right
				if (bufs[current].pos < bufs[current].len) {
					DBGU_PutChar('\033');
					DBGU_PutChar('[');
					DBGU_PutChar('C');
					bufs[current].pos++;
				}
			} else if (strcmp(escbuf, "[D") == 0) {
				// Left-Arrow: Move position one step to the left
				if (bufs[current].pos > 0) {
					DBGU_PutChar('\033');
					DBGU_PutChar('[');
					DBGU_PutChar('D');
					bufs[current].pos--;
				}
			}
		} else if (bufs[current].pos < BUFLEN && !is_finished && c >= 0x20) {
			int pos = bufs[current].pos;
			if (pos == bufs[current].len) {
				bufs[current].buf[pos] = c;
				bufs[current].pos++;
				bufs[current].len++;
				DBGU_PutChar(c);
			} else {
				int backstep_count = 0;
				// check if we need to move characters to the right
				while (pos < bufs[current].len) {
					char tmp = bufs[current].buf[pos];
					bufs[current].buf[pos] = c;
					DBGU_PutChar(c);
					c = tmp;
					pos++;
					backstep_count++;
				}

				if (pos < BUFLEN) {
					bufs[current].len++;
					bufs[current].buf[pos] = c;
					DBGU_PutChar(c);
				}

				bufs[current].pos++;

				if (backstep_count > 0) {
					fprintf(stderr, "\033[%dD", backstep_count);
				}
			}
		}
	}
	DBGU_PutChar('\n');
	DBGU_PutChar('\r');

	bufs[current].buf[bufs[current].len] = '\0';

	if (current != latest_buffer) {
		bufs[latest_buffer].pos = bufs[current].pos;
		bufs[latest_buffer].len = bufs[current].len;
		snprintf(bufs[latest_buffer].buf, BUFLEN, "%s", bufs[current].buf);
	}

	return snprintf(dest, destsize, "%s", bufs[current].buf);
}

/**
 * Reads a line from Serial. Reads at most bufsize - 1 characters and puts a
 * null byte after the last byte. Returns the number of characters written to
 * buf excluding the null character. No newline character is included in the
 * scanned string.
 */ 
int serial_readline(char *buf, int bufsize)
{
	int len = 0;
	int is_finished = 0;
	unsigned char c;

	if (buf == NULL) {
		return 0;
	}

	if (bufsize < 1) {
		return 0;
	}

	if (bufsize == 1) {
		buf[0] = 0;
		return 0;
	}

	while (!is_finished) {
		/* wait until we have a char to read */
		while (DBGU_IsRxReady() == 0);

		c = DBGU_GetChar();
		if (c == '\r')
			is_finished = 1;

		if (c == 0x7f || c == 0x08) { /* 0x7f = DEL, 0x08 = backspace */
			if (len > 0) {
				len--;
				DBGU_PutChar(c);
			}
		} else if (len < (bufsize - 1) && !is_finished && c >= 0x20) {
			buf[len] = c;
			len++;
			DBGU_PutChar(c);
		}
	}
	DBGU_PutChar('\n');
	DBGU_PutChar('\r');
	buf[len] = 0;

	return len;
}

/**
 * Reads an int from Serial. Returns 0 on success and 1 on failure.
 */
int serial_readint(int *dest)
{
	char buf[3 + (sizeof(int) * 8)];
	int len;

	if (dest == NULL)
		return 1;

	len = serial_readline(buf, sizeof(buf));
	if (len == 0)
		return 1;

	return arghandler_to_int(buf, dest);
}

/**
 * Reads input for a [y/n] confirmation prompt. Returns 0 if N|n (no) was
 * entered. Returns 1 if Y|y (yes) was entered. If nothing was entered, the
 * specified default value is returned.
 */
int serial_promptconfirm(const char *msg, int defval)
{
	int len;
	char yes_no_buf[2];

	if (msg == NULL)
		return defval;

	fprintf(stderr, "%s ", msg);
	if (defval)
		fprintf(stderr, "[Y/n] ");
	else
		fprintf(stderr, "[y/N] ");

	len = serial_readline(yes_no_buf, sizeof(yes_no_buf));
	if (len != 1)
		return defval;

	if (tolower((int) yes_no_buf[0]) == 'y')
		return 1;
	else if (tolower((int) yes_no_buf[0]) == 'n')
		return 0;
	else
		return defval;
}

/**
 * Reads a key press from Serial. Does not show the pressed key back to the user.
 */
char serial_getkeypress(void)
{
	while (DBGU_IsRxReady() == 0);

	return (char) DBGU_GetChar();
}

