/**
 * @file   arghandler.c
 * @author John Wikman
 */

#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <mist-tools/arghandler.h>

/**
 * Splits the entered string into separate arguments. The entered string will
 * be modified (inserting null characters etc.), even on errors. The array
 * dest_argv must have allocated storage for at least argc_limit pointers.
 *
 * Returns -1 on error. Otherwise it returns the number of arguments that were
 * scanned.
 */
int arghandler_split_args(char *argstring, char **dest_argv, int argc_limit)
{
	if (argstring == NULL || dest_argv == NULL)
		return -1;
	if (argc_limit < 0)
		return -1;
	if (argc_limit == 0)
		return 0;

	int argc = 0;
	while (argc < argc_limit) {
		/* remove leading spaces */
		while (isspace((int) *argstring))
			argstring++;

		/* If we encountered a null byte after a space, we're done! */
		if (*argstring == 0)
			break;

		/* Check if this argument is encapsulated in quotes */
		int found_matching_quote = 0;
		char quotechar = *argstring;
		if (quotechar == '\"' || quotechar == '\'') {
			char *tmpargstr = argstring;
			tmpargstr++;

			dest_argv[argc] = tmpargstr;

			while (*tmpargstr != 0) {
				if (*tmpargstr == quotechar) {
					*tmpargstr = 0;
					found_matching_quote = 1;

					argstring = tmpargstr;
					argstring++;
					argc++;
					break;
				}

				tmpargstr++;
			}
		}

		if (!found_matching_quote) {
			/* This argument is not encapsulated in quotes */

			/* Set pointer value of this argument */
			dest_argv[argc] = argstring;
			argc++;

			/* Now skip to the next space (or null byte) */
			while (!isspace((int) *argstring) && *argstring != 0)
				argstring++;

			/* No more arguments if we skipped all the way to the null byte! */
			if (*argstring == 0)
				break;

			/* Now argstring points at the first space after the argument, so we
			 * set this character to 0 to mark the end of this argument. */
			*argstring = 0;

			argstring++; /* Start at the character after the null byte. */
		}
	}

	return argc;
}

/**
 * Converts the entered string into an int and stores it in the location
 * pointed to by dest. Returns 0 on success and 1 on error.
 */
int arghandler_to_int(const char *src, int *dest)
{
	int i;

	if (src == NULL || dest == NULL)
		return 1;

	/* remove leading spaces */
	while (isspace((int) *src))
		src++;

	/* Find the length to the trailing spaces */
	int len = 0;
	while (src[len] != 0)
		len++;

	if (len == 0)
		return 1;

	/* Step back trailing spaces */
	while (isspace((int) src[len-1]) && len > 0)
		len--;

	/* Now check if we have a hexadecimal, binary or decimal format */
	if (len > 2 && src[0] == '0' && tolower((int) src[1]) == 'x') {
		/* Hexadecimal format: 0x######## */
		int max_hex_digits = sizeof(int) * 2;
		if (len > (2 + max_hex_digits))
			return 1; /* too many digits */

		int res = 0;
		for (i = 2; i < len; i++) {
			int digit_value;
			char digit = tolower((int) src[i]);

			if ('0' <= digit && digit <= '9')
				digit_value = digit - '0';
			else if ('a' <= digit && digit <= 'f')
				digit_value = digit - 'a' + 10;
			else
				return 1; /* not a hexdigit */

			res = (res << 4) + digit_value;
		}

		*dest = res;
	} else if (len > 2 && src[0] == '0' && tolower((int) src[1]) == 'b') {
		/* Binary format: 0b######.... */
		int max_bin_digits = sizeof(int) * 8;
		if (len > (2 + max_bin_digits))
			return 1; /* too many digits */

		int res = 0;
		for (i = 2; i < len; i++) {
			if (src[i] == '0')
				res = (res << 1);
			else if (src[i] == '1')
				res = (res << 1) + 1;
			else
				return 1; /* not a binary digit */
		}

		*dest = res;
	} else if (len > 1 && src[0] == '-') {
		/* Negative decimal format: -####### */
		int int_min_div10 = INT_MIN / 10;

		int res = 0;
		for (i = 1; i < len; i++) {
			char digit = src[i];
			if (!isdigit((int) digit))
				return 1; /* Not a digit */

			if (res < int_min_div10)
				return 1; /* an int will not fit the entire number */

			/* shift the digits one place up */
			res *= 10;
			int res_diff = res - INT_MIN;
			int digit_value = digit - '0';
			if (res_diff < digit_value && i != 1) /* res_diff will overflow on first digit */
				return 1; /* will not fit the last digit */

			res -= digit_value;
		}

		*dest = res;
	} else {
		/* Decimal format: ###### */
		int int_max_div10 = INT_MAX / 10;

		if (len == 0)
			return 1; /* no digits */

		int res = 0;
		for (i = 0; i < len; i++) {
			char digit = src[i];
			if (!isdigit((int) digit))
				return 1; /* Not a digit */

			if (res > int_max_div10)
				return 1; /* an int will not fit the entire number */

			/* shift the digits one place up */
			res *= 10;
			int res_diff = INT_MAX - res;
			int digit_value = digit - '0';
			if (res_diff < digit_value)
				return 1; /* will not fit the last digit */

			res += digit_value;
		}

		*dest = res;
	}

	/* Now at least one of the cases must have succeeded, so we return 0. */
	return 0;
}
