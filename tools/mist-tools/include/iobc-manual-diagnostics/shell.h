/**
 * @file   shell.h
 * @author William Stackenäs
 */

#ifndef IOBC_MANUAL_DIAGNOSTICS_SHELL_H
#define IOBC_MANUAL_DIAGNOSTICS_SHELL_H

int iobc_manual_diagnostics_exec(int argc, char **argv, int *exit);

#endif