/**
 * @file   arghandler.h
 * @author John Wikman
 */

#ifndef MIST_TOOLS_ARGHANDLER_H
#define MIST_TOOLS_ARGHANDLER_H

int arghandler_split_args(char *argstring, char **dest_argv, int argc_limit);
int arghandler_to_int(const char *src, int *dest);

#endif /* MIST_TOOLS_ARGHANDLER_H */
