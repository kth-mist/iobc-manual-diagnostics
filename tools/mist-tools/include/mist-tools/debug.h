/**
 * @file   debug.h
 * @author John Wikman
 *
 * Provides convenient debug macros.
 */

#ifndef MIST_TOOLS_DEBUG_H
#define MIST_TOOLS_DEBUG_H

#include <stdio.h>

#define DEBUGLN(msg) fprintf(stderr, "%s:%d: %s: " msg "\n\r", __FILE__, __LINE__, __func__);
#define DEBUGLN_ARGS(msg, ...) fprintf(stderr, "%s:%d: %s: " msg "\n\r", __FILE__, __LINE__, __func__, __VA_ARGS__);

#endif /* MIST_TOOLS_DEBUG_H */
