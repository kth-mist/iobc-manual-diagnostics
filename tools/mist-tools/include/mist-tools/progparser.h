/**
 * @file   progparser.h
 * @author John Wikman
 *
 * Functions that parse program arguments and print any errors that occur.
 */

#ifndef MIST_TOOLS_PROGPARSER_H
#define MIST_TOOLS_PROGPARSER_H

#include <inttypes.h>

int progparser_byte(const char *fname, const char *arg, uint8_t *dst);
int progparser_i2caddr(const char *fname, const char *arg, uint8_t *dst);

#endif /* MIST_TOOLS_PROGPARSER_H */
