/**
 * @file   serial_in.h
 * @author John Wikman
 *
 * Wrapper functions for reading input over the Debug UART.
 */

#ifndef MIST_TOOLS_SERIAL_INPUT_H
#define MIST_TOOLS_SERIAL_INPUT_H

int serial_buffered_readline(char *dest, int destsize);
int serial_readline(char *buf, int bufsize);
int serial_readint(int *dest);
int serial_promptconfirm(const char *msg, int defval);
char serial_getkeypress(void);

#endif /* MIST_TOOLS_SERIAL_INPUT_H */
