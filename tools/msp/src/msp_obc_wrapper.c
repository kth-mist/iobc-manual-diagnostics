/**
 * @file      msp_obc_wrapper.c
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Wrapper/helper functions for common use cases.
 */

#include <stdlib.h>

#include <msp/msp_opcodes.h>

#include <msp/msp_obc_error.h>
#include <msp/msp_obc_link.h>
#include <msp/msp_obc_wrapper.h>

/**
 * @brief Sends data over a link (a full OBC send transaction).
 * @param lnk The link to send data over.
 * @param opcode The opcode of the transaction.
 * @param data Pointer to the data to be sent.
 * @param datalen The length of data to send.
 * @return An MSP response.
 *
 * @note Blocking (multiple send/recv over I2C)
 *
 * Wrapper function that handles an entire OBC send transaction. Sends the data
 * over the specified link in a single transaction, invoking the underlying MSP
 * core functions as necessary.
 *
 * If len is 0 and the opcode is of system command type, then data is allowed
 * to be NULL.
 *
 * @note This function will update the state of the link.
 *
 * The success of this function call is indicated by the status of the returned
 * response being MSP_OBC_RESPONSE_TRANSACTION_SUCCESSFUL.
 */
struct msp_response msp_obc_send(msp_link_t *lnk,
                                 unsigned char opcode,
                                 unsigned char *data,
                                 unsigned long datalen)
{
	struct msp_response r;
	unsigned long len;
	unsigned long off;

	if (lnk == NULL) {
		r.status = MSP_RESPONSE_ERROR;
		r.error_code = MSP_OBC_ERR_NULL_POINTER;
		return r;
	}

	/* Check input arguments depending on opcode. */
	switch (MSP_OP_TYPE(opcode)) {
	case MSP_OP_TYPE_SEND:
		if (data == NULL) {
			r.status = MSP_RESPONSE_ERROR;
			r.error_code = MSP_OBC_ERR_NULL_POINTER;
			return r;
		}
		if (datalen == 0) {
			r.status = MSP_RESPONSE_ERROR;
			r.error_code = MSP_OBC_ERR_INVALID_LENGTH;
			return r;
		}
		break;

	case MSP_OP_TYPE_SYS:
		if (datalen != 0) {
			r.status = MSP_RESPONSE_ERROR;
			r.error_code = MSP_OBC_ERR_INVALID_LENGTH;
			return r;
		}
		break;

	default:
		/* Invalid opcode type. */
		r.status = MSP_RESPONSE_ERROR;
		r.error_code = MSP_OBC_ERR_INVALID_OPCODE;
		return r;
	}

	r = msp_start_transaction(lnk, opcode, datalen);
	if (r.status != MSP_RESPONSE_OK)
		return r;

	while (msp_is_active(lnk)) {
		/* Abort the transaction if we have exceeded the error threshold. */
		if (msp_error_count(lnk) > MSP_OBC_ERROR_THRESHOLD) {
			r.status = MSP_RESPONSE_TRANSACTION_ABORTED;
			r.error_code = MSP_OBC_ERR_EXCEEDED_ERROR_THRESHOLD;
			r.opcode = opcode;

			/* TODO: The transaction-ID should be extracted through an accessor
			 *       function instead. */
			r.transaction_id = lnk->transaction_id;
			r.len = datalen;
			msp_abort_transaction(lnk);
			return r;
		}

		switch (msp_next_action(lnk)) {
		case MSP_LINK_ACTION_TX_HEADER:
			r = msp_send_header_frame(lnk);
			break;
		case MSP_LINK_ACTION_TX_DATA:
			len = msp_next_data_length(lnk);
			off = msp_next_data_offset(lnk);
			r = msp_send_data_frame(lnk, &data[off], len);
			break;
		case MSP_LINK_ACTION_RX_HEADER:
			r = msp_recv_header_frame(lnk);
			break;
		default:
			/* We only expect to exchange headers and send data frames. */
			r.status = MSP_RESPONSE_ERROR;
			r.error_code = MSP_OBC_ERR_INVALID_STATE;
			msp_abort_transaction(lnk);
			return r;
		}
	}

	return r;
}

/**
 * @brief Requests data over a link (a full OBC request transaction).
 * @param lnk The link to request data over.
 * @param opcode The opcode of the transaction.
 * @param buf Pointer to where the received data shall be stored.
 * @param bufsize The capacity of the region pointed to by buf.
 * @return An MSP response.
 *
 * @note Blocking (multiple send/recv over I2C)
 *
 * Wrapper function that handles an entire OBC request transaction. Receives
 * data over specified link in a single transaction, invoking the underlying
 * MSP core functions as needed. The number of bytes that was received is
 * specified in the the returned MSP response.
 *
 * If the other side of the link attempts to respond with more data than the
 * provided buffer can handle, the entire transaction is aborted before
 * receiving any data.
 *
 * @note This function will update the state of the link.
 *
 * The success of this function call is indicated by the status of the returned
 * response being MSP_OBC_RESPONSE_TRANSACTION_SUCCESSFUL.
 */
struct msp_response msp_obc_recv(msp_link_t *lnk,
                                 unsigned char opcode,
                                 unsigned char *buf,
                                 unsigned long bufsize)
{
	struct msp_response r;
	unsigned long len;
	unsigned long off;

	if (lnk == NULL || buf == NULL) {
		r.status = MSP_RESPONSE_ERROR;
		r.error_code = MSP_OBC_ERR_NULL_POINTER;
		return r;
	}

	if (MSP_OP_TYPE(opcode) != MSP_OP_TYPE_REQ) {
		r.status = MSP_RESPONSE_ERROR;
		r.error_code = MSP_OBC_ERR_INVALID_OPCODE;
	}

	r = msp_start_transaction(lnk, opcode, 0);
	if (r.status != MSP_RESPONSE_OK)
		return r;

	while (msp_is_active(lnk)) {
		/* Abort the transaction if we have exceeded the error threshold. */
		if (msp_error_count(lnk) > MSP_OBC_ERROR_THRESHOLD) {
			r.status = MSP_RESPONSE_TRANSACTION_ABORTED;
			r.error_code = MSP_OBC_ERR_EXCEEDED_ERROR_THRESHOLD;
			r.opcode = opcode;

			/* TODO: The transaction-ID should be extracted through an accessor
			 *       function instead. */
			r.transaction_id = lnk->transaction_id;
			/* The offset also specifies the number of bytes that have been
			 * sent. */
			r.len = msp_next_data_offset(lnk);
			msp_abort_transaction(lnk);
			return r;
		}

		/* TODO: The total number of bytes to be received should be extracted
		 *       through an accessor function instead. */
		/* Abort transaction if we are receiving more data than we can handle. */
		if (lnk->total_length > bufsize) {
			r.status = MSP_RESPONSE_TRANSACTION_ABORTED;
			r.error_code = MSP_OBC_ERR_TOO_SMALL_REQUEST_BUFFER;
			r.opcode = opcode;

			/* TODO: The transaction-ID should be extracted through an accessor
			 *       function instead. */
			r.transaction_id = lnk->transaction_id;
			r.len = msp_next_data_offset(lnk);
			msp_abort_transaction(lnk);
			return r;
		}

		switch (msp_next_action(lnk)) {
		case MSP_LINK_ACTION_TX_HEADER:
			r = msp_send_header_frame(lnk);
			break;
		case MSP_LINK_ACTION_RX_HEADER:
			r = msp_recv_header_frame(lnk);
			break;
		case MSP_LINK_ACTION_RX_DATA:
			off = msp_next_data_offset(lnk);
			r = msp_recv_data_frame(lnk, &buf[off], &len);
			break;
		default:
			/* We only expect to exchange headers and receive data frames. */
			r.status = MSP_RESPONSE_ERROR;
			r.error_code = MSP_OBC_ERR_INVALID_STATE;
			msp_abort_transaction(lnk);
			return r;
		}
	}

	return r;
}

/**
 * @brief Same as msp_obc_send() but with support for flag-desync retry.
 * @param lnk The link to send data over.
 * @param opcode The opcode of the transaction.
 * @param data Pointer to the data to be sent.
 * @param datalen The length of data to send.
 * @return An MSP response.
 *
 * @note Blocking (multiple send/recv over I2C)
 *
 * Provides the same functionality as for the msp_obc_send() function, but with
 * the addition that it will try to send the transaction again if we receive a
 * T_ACK before sending all the data. In doing so, it updates the sequence flag
 * for the entered OP code before resending the transaction. If the second
 * attempt also receives a T_ACK before sending all data, then the transaction
 * is considered successful.
 *
 * @note This function is an extension to the MSP specification as the
 *       specification demands that both parties can maintain a state of
 *       syncronized sequence flags.
 *
 * If len is 0 and the opcode is of system command type, then data is allowed
 * to be NULL.
 *
 * @note This function will update the state of the link.
 *
 * The success of this function call is indicated by the status of the returned
 * response being MSP_OBC_RESPONSE_TRANSACTION_SUCCESSFUL.
 */
struct msp_response msp_obc_send_desyncretry(msp_link_t *lnk,
                                             unsigned char opcode,
                                             unsigned char *data,
                                             unsigned long datalen)
{
	struct msp_response r;
	unsigned long len;
	unsigned long off;
	unsigned long sent_data;
	unsigned int attempt;

	if (lnk == NULL) {
		r.status = MSP_RESPONSE_ERROR;
		r.error_code = MSP_OBC_ERR_NULL_POINTER;
		return r;
	}

	/* Check input arguments depending on opcode. */
	switch (MSP_OP_TYPE(opcode)) {
	case MSP_OP_TYPE_SEND:
		if (data == NULL) {
			r.status = MSP_RESPONSE_ERROR;
			r.error_code = MSP_OBC_ERR_NULL_POINTER;
			return r;
		}
		if (datalen == 0) {
			r.status = MSP_RESPONSE_ERROR;
			r.error_code = MSP_OBC_ERR_INVALID_LENGTH;
			return r;
		}
		break;

	case MSP_OP_TYPE_SYS:
		if (datalen != 0) {
			r.status = MSP_RESPONSE_ERROR;
			r.error_code = MSP_OBC_ERR_INVALID_LENGTH;
			return r;
		}
		break;

	default:
		/* Invalid opcode type. */
		r.status = MSP_RESPONSE_ERROR;
		r.error_code = MSP_OBC_ERR_INVALID_OPCODE;
		return r;
	}

	r = msp_start_transaction(lnk, opcode, datalen);
	if (r.status != MSP_RESPONSE_OK)
		return r;

	/* 2 attempts. Retry again if and only if we did not send all data on the
	 * first attempt. */
	for (attempt = 0; attempt < 2; attempt++) {
		/* Record how much data that we actuall sent in the transaction. */
		sent_data = 0;

		while (msp_is_active(lnk)) {
			/* Abort the transaction if we have exceeded the error threshold. */
			if (msp_error_count(lnk) > MSP_OBC_ERROR_THRESHOLD) {
				r.status = MSP_RESPONSE_TRANSACTION_ABORTED;
				r.error_code = MSP_OBC_ERR_EXCEEDED_ERROR_THRESHOLD;
				r.opcode = opcode;

				/* TODO: The transaction-ID should be extracted through an accessor
				 *       function instead. */
				r.transaction_id = lnk->transaction_id;
				r.len = datalen;
				msp_abort_transaction(lnk);
				return r;
			}

			switch (msp_next_action(lnk)) {
			case MSP_LINK_ACTION_TX_HEADER:
				r = msp_send_header_frame(lnk);
				break;
			case MSP_LINK_ACTION_TX_DATA:
				len = msp_next_data_length(lnk);
				off = msp_next_data_offset(lnk);
				sent_data = off + len;
				r = msp_send_data_frame(lnk, &data[off], len);
				break;
			case MSP_LINK_ACTION_RX_HEADER:
				r = msp_recv_header_frame(lnk);
				break;
			default:
				/* We only expect to exchange headers and send data frames. */
				r.status = MSP_RESPONSE_ERROR;
				r.error_code = MSP_OBC_ERR_INVALID_STATE;
				msp_abort_transaction(lnk);
				return r;
			}
		}

		if (attempt == 0) {
			/* If we on the first attempt sent all data, then we are done with the
			 * transaction. */
			if (sent_data == datalen)
				return r;
			
			/* Otherwise we restart the transaction and try again. */
			r = msp_start_transaction(lnk, opcode, datalen);
			if (r.status != MSP_RESPONSE_OK)
				return r;
		}
	}

	return r;
}

/**
 * @brief Same as msp_obc_recv() but with support for flag-desync retry.
 * @param lnk The link to request data over.
 * @param opcode The opcode of the transaction.
 * @param buf Pointer to where the received data shall be stored.
 * @param bufsize The capacity of the region pointed to by buf.
 * @return An MSP response.
 *
 * @note Blocking (multiple send/recv over I2C)
 *
 * Provides the same functionality as for the msp_obc_recv() function, but with
 * the addition that it will try to request the same kind of data again if we
 * receive the same transaction-ID as for the previous transaction. If the
 * second request also results in a duplicate transaction, then the request is
 * aborted.
 *
 * @note This function is an extension to the MSP specification as the
 *       specification demands that both parties can maintain a state of
 *       syncronized sequence flags.
 *
 * If the other side of the link attempts to respond with more data than the
 * provided buffer can handle, the entire transaction is aborted before
 * receiving any data.
 *
 * @note This function will update the state of the link.
 *
 * The success of this function call is indicated by the status of the returned
 * response being MSP_OBC_RESPONSE_TRANSACTION_SUCCESSFUL.
 */
struct msp_response msp_obc_recv_desyncretry(msp_link_t *lnk,
                                             unsigned char opcode,
                                             unsigned char *buf,
                                             unsigned long bufsize)
{
	struct msp_response r;

	r = msp_obc_recv(lnk, opcode, buf, bufsize);
	if (r.status == MSP_RESPONSE_TRANSACTION_SUCCESSFUL &&
		r.error_code == MSP_OBC_ERR_DUPLICATE_TRANSACTION) {
		/* Received a duplicate transaction. Try again. */
		r = msp_obc_recv(lnk, opcode, buf, bufsize);
	}

	return r;
}
