/**
 * @file      msp_obc_wrapper.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Wrapper/helper functions for common use cases.
 *
 * @details
 * Declares wrapper/helper functions that perform operations that are commonly
 * performed in MSP. These functions are not yet part of MSP core functionality
 * and rely on underlying MSP functions.
 */

#ifndef MSP_OBC_WRAPPER_H
#define MSP_OBC_WRAPPER_H

#include "msp_obc_link.h"

#define MSP_OBC_ERROR_THRESHOLD (5)

struct msp_response msp_obc_send(msp_link_t *lnk,
                                 unsigned char opcode,
                                 unsigned char *data,
                                 unsigned long datalen);

struct msp_response msp_obc_recv(msp_link_t *lnk,
                                 unsigned char opcode,
                                 unsigned char *buf,
                                 unsigned long bufsize);

struct msp_response msp_obc_send_desyncretry(msp_link_t *lnk,
                                             unsigned char opcode,
                                             unsigned char *data,
                                             unsigned long datalen);

struct msp_response msp_obc_recv_desyncretry(msp_link_t *lnk,
                                             unsigned char opcode,
                                             unsigned char *buf,
                                             unsigned long bufsize);

#endif
