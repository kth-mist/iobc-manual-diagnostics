# iOBC Manual Diagnostics
A tool for performing manual interactive diagnostics on the iOBC.

NOTE: To run this tool, you need to replace the `isis-sdk` submodule. To clarify, it must have the folder name
isis-sdk and if it contains the folders adcs, hal, mission-suppport, etc., then you can be certain that you have
added it correctly.
